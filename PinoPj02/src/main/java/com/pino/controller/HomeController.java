package com.pino.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pino.controller.HomeController;
import com.pino.dto.InsaDto;
import com.pino.dto.Insa_ComDto;
import com.pino.service.MemberService;

@Controller
public class HomeController {

	@Inject 
	//서비스를 호출하기 위해서 의존성을 주입
	MemberService memberservice;

	@Autowired
	private MemberService mServ;

	private ModelAndView mv;

	private static final Logger logger =  LoggerFactory.getLogger(HomeController.class);

	/////////////페이지 이동관련 매핑//////////////
	
	//수정

	//메인페이지
	@GetMapping("/")
	public String main() {
		logger.info("main()");

		return "main";
	}
	
	//메인페이지
	@GetMapping("/kakaoMap")
	public String kakaoMap() {
		logger.info("kakaoMap()");

		return "kakaoMap";
	}
	
	//메인페이지
	@GetMapping("/kakao2")
	public String kakao2() {
		logger.info("kakao2()");

		return "kakao2";
	}
	//메인페이지
	@GetMapping("/overlay")
	public ModelAndView overlay() {
		logger.info("overlay()");
		mv = new ModelAndView();
		String view = "overlay";
		
		mv.setViewName(view);
		return mv;
	}
	
	//메인페이지
	@GetMapping("/otherMap")
	public ModelAndView otherMap() {
		logger.info("otherMap()");
		mv = new ModelAndView();
		String view = "otherMap";
		
		mv.setViewName(view);
		return mv;
	}
	
	//메인페이지
	@GetMapping("/nextMap")
	public ModelAndView nextMap() {
		logger.info("nextMap()");
		mv = new ModelAndView();
		String view = "nextMap";
		
		mv.setViewName(view);
		return mv;
	}
}
