<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>오버레이</title>
<!-- 부트스트랩 CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<!-- 부트스트랩 JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<!-- 네이버지도 JS -->
<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?ncpClientId=0spszca8w6"></script>
<!-- 암차트 JS -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/plugins/wordCloud.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!-- 차트 & 공통함수 JS -->
<script src="resources/js/common.js"></script>
<script src="resources/js/modules.js"></script>

<!-- 전용CSS -->
<link href="resources/css/header.css" rel="stylesheet">
<link href="resources/css/main.css" rel="stylesheet">
<link href="resources/css/footer.css" rel="stylesheet">

</head>
<header>
	
</header>
<body>
<div class="col-xs-12">
				<h2>지역별 주요 이슈 분석</h2>
				<div class="col-xs-5 area-map">
					<div class="ibox-content">
						<h3>지역본부</h3>
						<div>지도를 클릭하여 지역별 이슈를 분석하세요</div>
						<div class="map-korea text-center">
							<img src="img/img-map.jpg" class="korea-han" alt="" usemap="#Map"  />
							<map name="Map" id="Map">
								<area href="#" class="btn-position1" alt="서울지역본부" shape="poly" coords="87,78,91,73,93,75,96,70,102,75,101,79,95,81,86,79" />
								<area href="#" class="btn-position2" alt="남서울지역본부" shape="poly" coords="86,79,80,78,79,80,82,84,83,87,86,89,88,92,93,90,97,92,103,89,105,82,100,79,95,83,88,81"/>
								<area href="#" class="btn-position3" alt="인천지역본부" shape="poly" coords="74,71,74,63,66,63,66,73,69,76,70,86,74,94,77,88,80,78,75,72" />
								<area href="#" class="btn-position3" alt="인천지역본부" shape="poly" coords="64,61,64,74,60,75,56,73,56,67,54,64,57,56,63,59" />
								<area href="#" class="btn-position3" alt="인천지역본부" shape="poly" coords="67,85,59,89,55,84,64,81,67,84" />
								<area href="#" class="btn-position4" alt="경기북부지역본부" shape="poly" coords="106,23,99,25,98,27,89,27,83,32,83,38,90,39,89,43,85,45,85,54,81,48,75,50,79,52,76,57,77,64,74,72,83,78,90,73,96,70,102,77,102,81,107,83,112,88,117,90,117,93,127,96,133,97,140,102,144,95,150,86,131,79,130,63,135,54,127,45,126,39,119,39,117,34,114,35,107,25"  />
								<area href="#" class="btn-position5" alt="경기지역본부" shape="poly" coords="141,103,133,97,117,95,116,90,113,89,105,83,104,87,97,93,94,89,89,92,83,87,80,82,77,84,77,89,74,96,76,101,63,100,63,106,70,113,72,115,79,113,76,122,84,134,92,133,99,130,107,136,123,125,127,126,134,123,134,117,138,118,141,112" />
								<area href="#" class="btn-position6" alt="강원지역본부" shape="poly" coords="191,2,195,12,198,22,201,28,205,37,211,55,218,64,220,70,231,82,230,89,234,93,235,99,247,116,247,127,240,130,237,136,231,129,229,133,219,129,215,133,210,128,206,133,199,131,193,128,191,125,188,126,182,121,177,122,178,117,173,115,168,115,162,118,160,112,155,112,153,116,153,118,144,119,140,113,141,101,146,93,144,91,149,87,142,83,137,80,133,80,130,65,135,58,136,54,131,48,126,43,125,39,118,39,117,33,114,34,110,30,106,24,110,21,116,20,122,21,125,20,131,25,138,22,145,24,149,22,154,26,158,23,166,27,181,19,186,10,187,3" />
								<area href="#" class="btn-position7" alt="충북지역본부" shape="poly" coords="194,132,199,132,193,128,191,125,188,126,181,121,178,122,175,120,176,118,171,115,166,116,164,117,159,114,155,112,153,117,153,118,144,120,141,114,138,118,135,119,135,122,131,125,128,126,124,125,108,135,112,143,115,147,116,151,112,149,105,157,108,163,110,168,111,172,115,173,119,174,122,178,120,182,119,190,123,193,125,206,128,209,135,211,143,211,147,208,150,202,149,198,153,197,153,194,151,193,148,193,145,189,141,191,141,187,145,183,145,179,145,171,142,165,144,162,147,161,150,158,149,156,154,154,157,155,158,151,158,147,162,148,165,147,168,147,172,146,177,148,185,147,187,139,194,134" />
								<area href="#" class="btn-position8" alt="대전춘남지역본부" shape="poly" coords="80,137,84,134,94,133,99,130,107,135,114,146,115,150,111,150,107,154,106,160,111,170,114,174,119,174,121,180,120,185,120,191,122,194,125,200,125,208,125,210,121,208,117,214,113,208,109,210,107,201,101,203,88,203,85,197,79,197,75,201,71,204,66,206,59,201,56,195,51,195,55,187,53,180,56,176,53,174,52,153,48,156,45,154,50,163,48,171,43,165,42,153,40,147,35,147,35,143,31,143,36,134,42,128,46,134,51,131,48,130,51,123,57,125,57,121,67,123,74,126,77,132" />
								<area href="#" class="btn-position9" alt="전북지역본부" shape="poly" coords="66,207,52,208,56,211,58,216,71,216,62,219,68,225,61,224,56,229,45,236,47,240,56,241,48,245,42,252,44,257,48,262,58,259,62,251,68,250,76,257,81,251,82,255,83,265,85,267,95,266,100,268,108,263,117,268,123,261,122,251,124,236,137,223,143,217,141,211,127,207,122,209,116,213,108,207,106,202,88,202,81,197" />
								<area href="#" class="btn-position10" alt="광주전남지역본부" shape="poly" coords="40,252,40,258,32,268,39,280,39,284,34,283,28,290,33,293,36,293,33,299,34,308,28,308,25,313,26,320,32,325,33,325,37,331,38,336,37,341,41,343,47,338,55,336,59,325,60,335,65,336,72,333,75,323,84,319,90,315,95,314,96,317,93,318,83,324,81,331,92,336,98,337,104,328,108,326,102,317,105,309,109,307,112,309,113,318,115,323,119,318,127,318,126,308,120,308,115,302,122,301,128,295,124,284,118,271,108,263,99,268,91,264,86,266,81,256,75,257,69,251,59,259,48,263" />
								<area href="#" class="btn-position10" alt="광주전남지역본부" shape="poly" coords="25,321,23,324,14,330,15,340,28,335,32,329,27,322">
								<area href="#" class="btn-position11" alt="대구지역본부" shape="poly" coords="252,166,248,173,251,179,248,189,245,195,244,207,246,212,244,218,247,222,254,217,257,219,253,231,247,251,240,248,233,249,231,243,226,243,221,246,218,249,210,249,202,252,192,250,185,251,183,245,172,247,167,245,160,243,162,239,158,232,154,228,149,228,141,221,144,216,141,211,149,204,150,198,153,197,153,193,148,194,146,190,141,191,142,186,144,184,145,172,143,166,146,162,149,166,154,169,160,166,162,165,170,170,170,176,173,180,175,182,163,191,165,197,169,200,169,206,175,210,186,207,195,215,200,210,202,210,208,208,212,204,218,206,222,201,226,199,233,197,234,191,233,184,231,180,236,179,236,171,237,166" />
								<area href="#" class="btn-position11" alt="대구지역본부" shape="poly" coords="272,100,266,104,270,108,275,106">
								<area href="#" class="btn-position12" alt="경북지역본부" shape="poly" coords="248,127,249,132,251,134,251,140,250,150,255,158,252,166,241,165,237,165,235,170,236,175,234,179,230,178,231,181,234,185,234,188,236,192,234,194,230,200,224,196,223,200,221,200,217,206,212,204,208,207,200,210,197,214,192,214,191,210,189,210,186,207,177,210,172,209,169,206,169,199,162,193,165,188,170,185,172,186,175,182,174,178,170,178,170,170,162,163,158,167,158,169,152,170,148,164,148,159,152,155,158,155,158,149,166,148,172,146,176,149,180,148,185,147,187,141,192,136,197,131,204,132,211,128,215,132,219,130,227,133,232,130,236,133,245,128" />
								<area href="#" class="btn-position13" alt="부산울산지역본부" shape="poly" coords="246,250,246,259,244,266,241,264,238,269,239,273,231,280,231,286,223,293,219,295,207,297,209,293,202,296,201,289,194,282,194,272,202,268,208,264,212,261,214,256,218,250,219,248,226,243,231,243,234,247,242,248" />
								<area href="#" class="btn-position14" alt="경남지역본부" shape="poly" coords="141,221,136,224,131,225,124,235,120,247,122,253,123,259,120,262,117,271,120,276,123,282,124,286,129,292,129,295,123,298,124,302,128,305,133,301,135,301,131,307,133,312,132,317,140,314,142,320,146,318,147,311,143,309,137,306,139,299,144,295,147,294,147,303,153,306,157,306,159,305,162,305,164,303,166,304,166,307,169,310,165,314,172,320,175,310,173,304,174,300,178,297,177,293,181,292,185,294,186,284,193,292,200,295,200,289,194,280,194,271,205,266,219,250,207,250,200,252,196,250,185,250,184,245,174,248,167,243,163,243,160,242,162,236,153,228" />
								<area href="#" class="btn-position14" alt="경남지역본부" shape="poly" coords="193,299,194,304,192,308,196,311,188,318,187,322,183,325,181,314,182,311,177,307,187,302">
								<area href="#" class="btn-position15" alt="제주지역본부" shape="poly" coords="48,395,35,397,27,404,21,411,26,421,32,419,45,420,49,422,63,416,73,411,76,402,67,395" />
							</map>
							<div id="char4">
								<div id="position1" class="map-layer-info">
								  <h3>서울</h3>
									<p>분석데이터 수 : <font class="text-danger">11,098</font></p>
								</div>
								<div id="position2" class="map-layer-info">
								  <h3>남서울</h3>
									<p>분석데이터 수 : <font class="text-danger">11,098</font></p>
								</div>
								<div id="position3" class="map-layer-info">
								  <h3>인천</h3>
									<p>분석데이터 수 : <font class="text-danger">11,098</font></p>
								</div>
								<div id="position4" class="map-layer-info">
								  <h3>경기북부</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position5" class="map-layer-info">
								  <h3>경기</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position6" class="map-layer-info">
								  <h3>강원</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position7" class="map-layer-info">
								  <h3>충북</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position8" class="map-layer-info">
								  <h3>대전충남</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position9" class="map-layer-info">
								  <h3>전북</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position10" class="map-layer-info">
								  <h3>광주전남</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position11" class="map-layer-info">
								  <h3>대구</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position12" class="map-layer-info">
								  <h3>경북</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position13" class="map-layer-info">
								  <h3>부산울산</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position14" class="map-layer-info">
								  <h3>경남</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position15" class="map-layer-info">
								  <h3>제주</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
							</div>
						</div>
					</div>
				</div>
	<div class="chartbox">
		<div style="width:30%;height:50px;margin-left: 100px;">
			<button onclick="location.href='./'" type="button" class="btn btn-primary" style="margin-left: 10px;">홈으로</button>
			<button type="button" type="button" class="btn btn-primary" style="margin-left: 10px;">이벤트테스트</button>
		</div>
		<p style="width:30%; margin-left: 100px;">boardChart</p>
		<div class="boardChart" id ="map" style="width:30%;height:600px;margin-left: 100px;">
			
		</div>
		<p style="width:30%; margin-left: 100px;">cloudChart</p>
		<div class="cloudChart" id ="map" style="width:30%;height:600px;margin-left: 100px;">
			
		</div>
		<p style="width:30%; margin-left: 100px;">issueChart</p>
		<div class="issueChart" id ="map" style="width:30%;height:600px;margin-left: 100px;">
			
		</div>
		<p style="width:30%; margin-left: 100px;">treeChart</p>
		<div class="treeChart" id ="map" style="width:30%;height:600px;margin-left: 100px;">
			
		</div>			
	</div>
<script src="resources/js/geoJson.js"></script>
<script>
$(function () {
	testView ={
			init : function (){
				var self = this;
				console.log(self);
				self.handler();
			},
			handler : function () {
	            const self = this;
	            const CLICK_EVENT = "click";
	            
				const $topnCaregoryBtn = $('.btn-primary');
	            $topnCaregoryBtn.unbind(CLICK_EVENT).bind(CLICK_EVENT, function () {
	                console.log("이게되내 ????")
	            });
			}
	}
	testView.init();
})

// $(document).on('click', '.btn-primary', function(obj){
// 	console.log("이게되내 ????")
// })
//----------------------------boardChart--------------------------//
//----------------------------/boardChart--------------------------//

//----------------------------cloudChart--------------------------//
var cloudChartData = [ {
    "tag": "javascript",
    "count": "1765836"
}, {
    "tag": "java",
    "count": "1517355"
}, {
    "tag": "c#",
    "count": "1287629"
}, {
    "tag": "php",
    "count": "1263946"
}, {
    "tag": "android",
    "count": "1174721"
}, {
    "tag": "python",
    "count": "1116769"
}, {
    "tag": "jquery",
    "count": "944983"
}, {
    "tag": "html",
    "count": "805679"
}, {
    "tag": "c++",
    "count": "606051"
}, {
    "tag": "ios",
    "count": "591410"
}, {
    "tag": "css",
    "count": "574684"
}, {
    "tag": "mysql",
    "count": "550916"
}, {
    "tag": "sql",
    "count": "479892"
}, {
    "tag": "asp.net",
    "count": "343092"
}, {
    "tag": "ruby-on-rails",
    "count": "303311"
}, {
    "tag": "c",
    "count": "296963"
}, {
    "tag": "arrays",
    "count": "288445"
}, {
    "tag": "objective-c",
    "count": "286823"
}, {
    "tag": ".net",
    "count": "280079"
}, {
    "tag": "r",
    "count": "277144"
}, {
    "tag": "node.js",
    "count": "263451"
}, {
    "tag": "angularjs",
    "count": "257159"
}, {
    "tag": "json",
    "count": "255661"
}, {
    "tag": "sql-server",
    "count": "253824"
}, {
    "tag": "swift",
    "count": "222387"
}, {
    "tag": "iphone",
    "count": "219827"
}, {
    "tag": "regex",
    "count": "203121"
}, {
    "tag": "ruby",
    "count": "202547"
}, {
    "tag": "ajax",
    "count": "196727"
}, {
    "tag": "django",
    "count": "191174"
}, {
    "tag": "excel",
    "count": "188787"
}, {
    "tag": "xml",
    "count": "180742"
}, {
    "tag": "asp.net-mvc",
    "count": "178291"
}, {
    "tag": "linux",
    "count": "173278"
}, {
    "tag": "angular",
    "count": "154447"
}, {
    "tag": "database",
    "count": "153581"
}, {
    "tag": "wpf",
    "count": "147538"
}, {
    "tag": "spring",
    "count": "147456"
}, {
    "tag": "wordpress",
    "count": "145801"
}, {
    "tag": "python-3.x",
    "count": "145685"
}, {
    "tag": "vba",
    "count": "139940"
}, {
    "tag": "string",
    "count": "136649"
}, {
    "tag": "xcode",
    "count": "130591"
}, {
    "tag": "windows",
    "count": "127680"
}, {
    "tag": "reactjs",
    "count": "125021"
}, {
    "tag": "vb.net",
    "count": "122559"
}, {
    "tag": "html5",
    "count": "118810"
}, {
    "tag": "eclipse",
    "count": "115802"
}, {
    "tag": "multithreading",
    "count": "113719"
}, {
    "tag": "mongodb",
    "count": "110348"
}, {
    "tag": "laravel",
    "count": "109340"
}, {
    "tag": "bash",
    "count": "108797"
}, {
    "tag": "git",
    "count": "108075"
}, {
    "tag": "oracle",
    "count": "106936"
}, {
    "tag": "pandas",
    "count": "96225"
}, {
    "tag": "postgresql",
    "count": "96027"
}, {
    "tag": "twitter-bootstrap",
    "count": "94348"
}, {
    "tag": "forms",
    "count": "92995"
}, {
    "tag": "image",
    "count": "92131"
}, {
    "tag": "macos",
    "count": "90327"
}, {
    "tag": "algorithm",
    "count": "89670"
}, {
    "tag": "python-2.7",
    "count": "88762"
}, {
    "tag": "scala",
    "count": "86971"
}, {
    "tag": "visual-studio",
    "count": "85825"
}, {
    "tag": "list",
    "count": "84392"
}, {
    "tag": "excel-vba",
    "count": "83948"
}, {
    "tag": "winforms",
    "count": "83600"
}, {
    "tag": "apache",
    "count": "83367"
}, {
    "tag": "facebook",
    "count": "83212"
}, {
    "tag": "matlab",
    "count": "82452"
}, {
    "tag": "performance",
    "count": "81443"
}, {
    "tag": "css3",
    "count": "78250"
}, {
    "tag": "entity-framework",
    "count": "78243"
}, {
    "tag": "hibernate",
    "count": "76123"
}, {
    "tag": "typescript",
    "count": "74867"
}, {
    "tag": "linq",
    "count": "73128"
}, {
    "tag": "swing",
    "count": "72333"
}, {
    "tag": "function",
    "count": "72043"
}, {
    "tag": "amazon-web-services",
    "count": "71155"
}, {
    "tag": "qt",
    "count": "69552"
}, {
    "tag": "rest",
    "count": "69138"
}, {
    "tag": "shell",
    "count": "68854"
}, {
    "tag": "azure",
    "count": "67431"
}, {
    "tag": "firebase",
    "count": "66411"
}, {
    "tag": "api",
    "count": "66158"
}, {
    "tag": "maven",
    "count": "66113"
}, {
    "tag": "powershell",
    "count": "65467"
}, {
    "tag": ".htaccess",
    "count": "65014"
}, {
    "tag": "sqlite",
    "count": "64888"
}, {
    "tag": "file",
    "count": "62783"
}, {
    "tag": "codeigniter",
    "count": "62393"
}, {
    "tag": "unit-testing",
    "count": "61909"
}, {
    "tag": "perl",
    "count": "61752"
}, {
    "tag": "loops",
    "count": "61015"
}, {
    "tag": "symfony",
    "count": "60820"
}, {
    "tag": "selenium",
    "count": "59855"
}, {
    "tag": "google-maps",
    "count": "59616"
}, {
    "tag": "csv",
    "count": "59600"
}, {
    "tag": "uitableview",
    "count": "59011"
}, {
    "tag": "web-services",
    "count": "58916"
}, {
    "tag": "cordova",
    "count": "58195"
}, {
    "tag": "class",
    "count": "58055"
}, {
    "tag": "numpy",
    "count": "57132"
}, {
    "tag": "google-chrome",
    "count": "56836"
}, {
    "tag": "ruby-on-rails-3",
    "count": "55962"
}, {
    "tag": "android-studio",
    "count": "55801"
}, {
    "tag": "tsql",
    "count": "55736"
}, {
    "tag": "validation",
    "count": "55531"
} ];

var cloudChartObj = {value:"count", word:"tag"}

fn_amChart_wordCloud('cloudChart', cloudChartData, cloudChartObj);
//----------------------------/cloudChart--------------------------//

//----------------------------issueChart--------------------------//
var issueChartData = {
	category : ['카테고리'],
	list : [
		{
			word : "1등입니다",
			rank : 1,
			weight : 23.5
		},
		{
			word : "2등입니다",
			rank : 2,
			weight : 20.5
		},
		{
			word : "3등입니다",
			rank : 3,
			weight : 18.5
		},
		{
			word : "4등입니다",
			rank : 4,
			weight : 16.5
		}
	]
}


var issueChartObj = {limit:"5"}
fn_setRankChart('issueChart', issueChartData, issueChartObj);
//----------------------------/issueChart--------------------------//

//----------------------------treeChart--------------------------//
var treeChartData = [{
	name: "First",
	children: [
		{
			name: "A1",
			value: 100
		},
		{
			name: "A2",
			value: 60
		},
		{
			name: "A3",
			value: 30
		}
	]
},
{
	name: "Second",
	children: [
		{
			name: "B1",
			value: 135
		},
		{
			name: "B2",
			value: 98
		},
		{
			name: "B3",
			value: 56
		}
	]
},
{
	name: "Third",
	children: [
		{
			name: "C1",
			value: 335
		},
		{
			name: "C2",
			value: 148
		},
		{
			name: "C3",
			value: 126
		},
		{
			name: "C4",
			value: 26
		}
	]
},
{
	name: "Fourth",
	children: [
		{
			name: "D1",
			value: 415
		},
		{
			name: "D2",
			value: 148
		},
		{
			name: "D3",
			value: 89
		},
		{
			name: "D4",
			value: 64
		},
		{
			name: "D5",
			value: 16
		}
	]
},
{
	name: "Fifth",
	children: [
		{
			name: "E1",
			value: 687
		},
		{
			name: "E2",
			value: 148
		}
	]
}];

var treeChartObj = {value:"value", word:"name"}

fn_amChart_treemap('treeChart', treeChartData, treeChartObj);
//----------------------------/treeChart--------------------------//

</script>
</body>
<footer>
	
</footer>
</html>