<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>오버레이</title>
<!-- 부트스트랩 CSS -->
<!-- 부트스트랩 JS -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<!-- 카카오지도 JS -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=4d7d35bade5ba376fb1b947fccaaa67c"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/variwide.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="resources/js/geoJson.js"></script>
<script src="resources/js/geoJson3.js"></script>
<script>

//전국시 좌표
var markerLatLng = [];
//오버레이 좌표
var customOverlayLatLng = [];

var geoLatLng = [	
// 	//오버레이 좌표는 0.48정도 더하면 적당 
// 	{latLng : new naver.maps.LatLng(37.5666805, 126.9784147), name:'서울특별시', ename:'seoul'},
// 	{latLng : new naver.maps.LatLng(37.592471511019085,128.375244140625), name:'강원도', ename:'kangwon'},
// 	{latLng : new naver.maps.LatLng(37.47485808497102,126.61743164062499), name:'인천광역시', ename:'incheon'},
// 	{latLng : new naver.maps.LatLng(36.35052700542763,127.38098144531249), name:'대전광역시', ename:'deajeon'},
// 	{latLng : new naver.maps.LatLng(35.14237113713991,126.84814453125), name:'광주', ename:'gwangju'},
// 	{latLng : new naver.maps.LatLng(33.39705230475205,126.5460205078125), name:'제주', ename:'jeju'},
// 	{latLng : new naver.maps.LatLng(34.84762101276787,126.78497314453125), name:'전라남도', ename:'jeollanamdo'},
// 	{latLng : new naver.maps.LatLng(35.180543276002666,129.06463623046875), name:'부산', ename:'busan'},
// 	{latLng : new naver.maps.LatLng(35.54787066533268,129.254150390625), name:'울산', ename:'ulsan'},
// 	{latLng : new naver.maps.LatLng(35.860117799832544,128.55377197265622), name:'대구', ename:'daegu'},
// 	{latLng : new naver.maps.LatLng(35.106428057364255,128.08135986328125), name:'경상남도', ename:'jyeongsangnamdo'},
// 	{latLng : new naver.maps.LatLng(36.301845303684324,128.9794921875), name:'경상북도', ename:'jyeongsangbukdo'},
// 	{latLng : new naver.maps.LatLng(37.32430451813815,127.49359130859374), name:'경기도', ename:'Gyeonggido'},
// 	{latLng : new naver.maps.LatLng(36.35052700542763,127.38098144531249), name:'대전', ename:'deajeon'},
// 	{latLng : new naver.maps.LatLng(36.50301312197295,127.2930908203125), name:'세종', ename:'sejong'},
// 	{latLng : new naver.maps.LatLng(36.461054075054314,126.749267578125), name:'충청남도', ename:'chungcheongnamdo'},
// 	{latLng : new naver.maps.LatLng(36.6640126988417,127.7215576171875), name:'충청북도', ename:'chungcheongbukdo'},
// 	{latLng : new naver.maps.LatLng(35.67068501330236,127.2161865234375), name:'전라북도', ename:'jeollabukdo'}
	
	//오버레이 좌표는 0.48정도 더하면 적당 
	{latLngY : 37.5666805, latLngX : 126.8984147, name:'서울특별시', ename:'seoul'},
	{latLngY : 37.592471511019085, latLngX : 128.375244140625, name:'강원도', ename:'kangwon'},
	{latLngY : 37.47485808497102, latLngX : 126.61743164062499, name:'인천광역시', ename:'incheon'},
	{latLngY : 35.14237113713991, latLngX : 126.75814453125, name:'광주', ename:'gwangju'},
	{latLngY : 33.39705230475205, latLngX : 126.5460205078125, name:'제주', ename:'jeju'},
	{latLngY : 34.84762101276787, latLngX : 126.78497314453125, name:'전라남도', ename:'jeollanamdo'},
	{latLngY : 35.180543276002666, latLngX : 129.06463623046875, name:'부산', ename:'busan'},
	{latLngY : 35.54787066533268, latLngX : 129.254150390625, name:'울산', ename:'ulsan'},
	{latLngY : 35.860117799832544, latLngX : 128.55377197265622, name:'대구', ename:'daegu'},
	{latLngY : 35.106428057364255, latLngX : 128.08135986328125, name:'경상남도', ename:'jyeongsangnamdo'},
	{latLngY : 36.301845303684324, latLngX : 128.9794921875, name:'경상북도', ename:'jyeongsangbukdo'},
	{latLngY : 37.32430451813815, latLngX : 127.40359130859374, name:'경기도', ename:'Gyeonggido'},
	{latLngY : 36.31052700542763, latLngX : 127.31098144531249, name:'대전', ename:'deajeon'}, 
	{latLngY : 36.58301312197295, latLngX : 127.2130908203125, name:'세종', ename:'sejong'},  
	{latLngY : 36.461054075054314, latLngX : 126.749267578125, name:'충청남도', ename:'chungcheongnamdo'},
	{latLngY : 36.7240126988417, latLngX : 127.7215576171875, name:'충청북도', ename:'chungcheongbukdo'},
	{latLngY : 35.67068501330236, latLngX : 127.2161865234375, name:'전라북도', ename:'jeollabukdo'},
	{latLngY : 34.32302363048832, latLngX : 130.92544555664062, name:'차트영역', ename:'summary'}
];

for(var i = 0; i < geoLatLng.length; i++){
	markerLatLng.push({latLng : new kakao.maps.LatLng(geoLatLng[i].latLngY,geoLatLng[i].latLngX), name:geoLatLng[i].name, ename:geoLatLng[i].ename})
}

for(var i = 0; i < geoLatLng.length; i++){
	customOverlayLatLng.push({latLng : new kakao.maps.LatLng(geoLatLng[i].latLngY+0.35,geoLatLng[i].latLngX-0.35), name:geoLatLng[i].name, ename:geoLatLng[i].ename})
}


			(function(){
				var map ="";
					
				var kkoMap = {				
					initKko : function(data){
						areaId = data.mapId;
						option = data.option;
						
						mapContainer = document.getElementById(areaId); // 지도를 표시할 div 
						mapOption = $.extend({
							center: new kakao.maps.LatLng(36.0546781, 128.6806144)						
							,level : 13
						},option);

						map = new kakao.maps.Map(mapContainer, mapOption),
						customOverlay = new kakao.maps.CustomOverlay({}),
						infowindow = new kakao.maps.InfoWindow({removable: true});
						
						$.getJSON("resources/js/all.json",function(jData){
						$jData = $(jData.features);
							$jData.each(function(){
								kkoMap.getPolycode($(this)[0],)
								;
							});
						});
						

							
						for(var i = 0; i<markerLatLng.length; i++){
							if(markerLatLng[i].ename != 'summary'){
								var content = '<div style="width:30px; height: 40px; z-index: 1;">'
									content += '<div class="'+markerLatLng[i].ename+'" id="'+markerLatLng[i].ename+'" style="width:30px; height: 20px; z-index: 1;"></div>';
									content += '<div style="width:30px; height: 20px; z-index: 1; font-size:10px; color:white">'+markerLatLng[i].name+'</div>';
									content += '</div>';
									// 커스텀 오버레이가 표시될 위치입니다 
									var position = markerLatLng[i].latLng;  

									// 커스텀 오버레이를 생성합니다
									var customOverlay = new kakao.maps.CustomOverlay({
									    position: position,
									    content: content   
									});

									// 커스텀 오버레이를 지도에 표시합니다
									customOverlay.setMap(map);
							}else{
								var content = '<div style="width:180px; height: 120px; z-index: 1;">'
									content += '<div class="'+markerLatLng[i].ename+'" id="'+markerLatLng[i].ename+'" style="width:180px; height: 120px; z-index: 1;"></div>';
									content += '<div style="width:180px; height: 120px; z-index: 1; font-size:15px; color:black; text-align:center;">'+markerLatLng[i].name+'</div>';
									content += '</div>';
									// 커스텀 오버레이가 표시될 위치입니다 
									var position = markerLatLng[i].latLng;  

									// 커스텀 오버레이를 생성합니다
									var customOverlay = new kakao.maps.CustomOverlay({
									    position: position,
									    content: content   
									});

									// 커스텀 오버레이를 지도에 표시합니다
									customOverlay.setMap(map);
							}
						}

					}
					,getPolycode : function(Feature){
						var geometry = Feature.geometry
						var polygonBox = [];						
						var polygon=[];
						var MultiPolygon=[];
						
						
						if("Polygon" == geometry.type){
							var coordinate = geometry.coordinates[0];
							polygonArr = {"name":Feature.properties.loc_nm, "path":[]}
							
							for(var c in coordinate){						
								polygonArr.path.push(new kakao.maps.LatLng(coordinate[c][1], coordinate[c][0]));
							}
							
							kkoMap.setPolygon(polygonArr)
						}else if("MultiPolygon" == geometry.type){
							arrP = []
							for(var c in geometry.coordinates){
								var multiCoordinates = geometry.coordinates[c];
								polygonArr = {"name":Feature.properties.loc_nm, "path":[]}
								
								for(var z in multiCoordinates[0]){
									polygonArr.path.push(new kakao.maps.LatLng(multiCoordinates[0][z][1], multiCoordinates[0][z][0]));
									
								}
								kkoMap.setPolygon(polygonArr)
							}
							
						}
					
					}
					,setPolygon : function(data,option){
						console.log('data',data.name,'option',option)			
						var name = ["부산광역시", "서울특별시","대구광역시","인천광역시","대전광역시","울산광역시","세종특별자치시","경기도", "강원도","충청북도","충청남도","전라북도","전라남도","경상북도","경상남도","제주특별자치도","광주광역시"];
						if(data.name == "부산광역시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: '#e49f9f',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: '#e49f9f',
							fillOpacity: 1 
							});
						}else if(data.name == "서울특별시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'lime',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'lime',
							fillOpacity: 1 
							});
						}else if(data.name == "대구광역시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'teal',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'teal',
							fillOpacity: 1 
							});
						}else if(data.name == "인천광역시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'yellow',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'yellow',
							fillOpacity: 1 
							});
						}else if(data.name == "대전광역시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'white',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'white',
							fillOpacity: 1 
							});
						}else if(data.name == "울산광역시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'silver',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'silver',
							fillOpacity: 1 
							});
						}else if(data.name == "세종특별자치시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'purple',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'purple',
							fillOpacity: 1 
							});
						}else if(data.name == "경기도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'orange',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'orange',
							fillOpacity: 1 
							});
						}else if(data.name == "강원도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'olive',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'olive',
							fillOpacity: 1 
							});
						}else if(data.name == "충청북도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'navy',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'navy',
							fillOpacity: 1 
							});
						}else if(data.name == "충청남도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'maroon',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'maroon',
							fillOpacity: 1 
							});
						}else if(data.name == "전라북도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'green',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'green',
							fillOpacity: 1 
							});
						}else if(data.name == "전라남도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'gray',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'gray',
							fillOpacity: 1 
							});
						}else if(data.name == "경상북도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'fuchsia',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'fuchsia',
							fillOpacity: 1 
							});
						}else if(data.name == "경상남도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'aqua',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'aqua',
							fillOpacity: 1 
							});
						}else if(data.name == "제주특별자치도"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'red',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'red',
							fillOpacity: 1 
							});
						}else if(data.name == "광주광역시"){
							polygonOption = $.extend({
								strokeWeight: 2,
								strokeColor: 'black',
								strokeOpacity: 1,
								fillColor: 'blue',
								fillOpacity: 1
							},option);
							
							var polygon = new kakao.maps.Polygon({
								name: data.name
								,path : data.path,
								strokeWeight: 2,
							strokeColor: 'black',
							strokeOpacity: 0.5,
							fillColor: 'blue',
							fillOpacity: 1 
							});
						}
						
						function setCenter() {            
						    // 이동할 위도 경도 위치를 생성합니다 
							map.setLevel(9, {anchor: new kakao.maps.LatLng(37.56668059, 126.8984147)});
						}
						
						kakao.maps.event.addListener(polygon, 'mouseover', function(mouseEvent) { 
// 							polygon.setOptions({fillColor: '#09f'});
// 							customOverlay.setPosition(mouseEvent.latLng); 
// 							customOverlay.setMap(map);
						});
						
			
						
						kakao.maps.event.addListener(polygon, 'mousemove', function(mouseEvent) {
							customOverlay.setPosition(mouseEvent.latLng); 
						});
						
						kakao.maps.event.addListener(polygon, 'mouseout', function() {
// 							polygon.setOptions({fillColor: '#e49f9f'});
// 							customOverlay.setMap(null);
						}); 
						
						kakao.maps.event.addListener(polygon, 'click', function() {
							
							//확대이벤트
							fn_zoomEvent(data.name);
							
							polygon.setMap(null);
							setCenter();
							alert(data.name)
							console.log($(this))
						}); 
						
						polygon.setMap(map);
					}
				}
				
				window.kkoMap = kkoMap;
				
				
			})();
			
			
			
			$(function(){
				kkoMap.initKko({
					mapId :"map"
					,option :""
				});
				
				
				
				fn_setChart();
			});
			
			function fn_zoomEvent(name){
				console.log(name)
				
				fn_policyPolygon('서울특별시');
				
// 				console.log('data', GeoJson.features)
// 				var PolygonData = geoJson3.features
// 				console.log('dddd', PolygonData[0].geometry.coordinates[0])
// 				var $pathsData = new Array();

// 				for(var i = 0; i < PolygonData.length ; i++){
// // 					if(PolygonData[i].properties.CTP_KOR_NM == '서울특별시'){
// 						for(var k = 0; k < PolygonData[i].geometry.coordinates[0].length ; k++){
// 							$pathsData.push(new kakao.maps.LatLng(PolygonData[i].geometry.coordinates[0][k][1], PolygonData[i].geometry.coordinates[0][k][0]));
// 						}
// // 					}
// 				}
				
// 				console.log('$pathsData', $pathsData); 
// 				// 지도에 표시할 선을 생성합니다
// 				var polygon = new kakao.maps.Polygon({
// 				    path: $pathsData, // 그려질 다각형의 좌표 배열입니다
// 				    strokeWeight: 3, // 선의 두께입니다
// 				    strokeColor: '#39DE2A', // 선의 색깔입니다
// 				    strokeOpacity: 0.8, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
// 				    fillColor: '#A2FF99', // 채우기 색깔입니다
// 				    fillOpacity: 0.7 // 채우기 불투명도 입니다
// 				});

// 				// 지도에 다각형을 표시합니다
// 				polygon.setMap(map);
				if(name == '서울특별시') fn_jsonAjax( {url : '/resources/js/seoul_geo.json', name:name, success : function(data){  }} ); //서울특별시
				if(name == '대구광역시') fn_jsonAjax( {url : '/resources/js/testgeo.json', name:name, success : function(data){  }} ); //대구광역시
				if(name == '인천광역시') fn_jsonAjax( {url : '/resources/js/inc_geo.json', name:name, success : function(data){  }} ); //인천광역시
			}
			
			function fn_policyPolygon(obj){

			}
			
			function fn_jsonAjax(obj){
				$.ajax({
					async : true,
					url : obj.url,
					type : 'GET',
					dataType : 'json', 
					success : function(data){
						console.log('성공',data); 
						
						var centerLatLng = new kakao.maps.LatLng(37.57669160058646, 126.98753356933594);
						
						if(obj.name == '대구광역시') centerLatLng = new kakao.maps.LatLng(35.85343961959182, 128.56201171875);
						if(obj.name == '인천광역시') centerLatLng = new kakao.maps.LatLng(37.47485808497102, 126.6119384765625);
						
						var container = document.getElementById('map'),
					    options = {
					         center: centerLatLng,
					         level: 9
					    };
					 
						var map = new kakao.maps.Map(container, options);
						
						
						var PolygonData = data.features;
						
						for(var i = 0; i < PolygonData.length ; i++){
//		 					if(PolygonData[i].properties.CTP_KOR_NM == '서울특별시'){
								for(var j = 0; j < PolygonData[i].geometry.coordinates.length ; j++){
									var $pathsData = new Array();
									for(var k = 0; k < PolygonData[i].geometry.coordinates[j].length ; k++){
										$pathsData.push(new kakao.maps.LatLng(PolygonData[i].geometry.coordinates[j][k][1], PolygonData[i].geometry.coordinates[j][k][0]));
									}
									console.log('$pathsData', $pathsData); 
									// 지도에 표시할 선을 생성합니다
									var polygon = new kakao.maps.Polygon({
									    path: $pathsData, // 그려질 다각형의 좌표 배열입니다
									    strokeWeight: 3, // 선의 두께입니다
									    strokeColor: '#39DE2A', // 선의 색깔입니다
									    strokeOpacity: 0.8, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
									    fillColor: '#A2FF99', // 채우기 색깔입니다
									    fillOpacity: 0.7 // 채우기 불투명도 입니다
									});
	
									// 지도에 다각형을 표시합니다
									polygon.setMap(map);
								}

								
//		 					}


						}
						
						
					}
				}).fail(function(request, status, error) {	
					console.log(status + " error > " + error);
				});
			}
			
			function fn_setChart(){
				//var summaryChart = 130.92544555664062,34.32302363048832
				
				
				var chartLocation = ['inchoenChart'];
	
			 	for(var i = 0; i<markerLatLng.length; i++){
			 		if(markerLatLng[i].ename != 'summary'){
				 		Highcharts.chart(markerLatLng[i].ename, {

				 			  chart: {
				 			   type: 'variwide'
				 			   ,backgroundColor: 'transparent'
				 			   ,spacing: [0,0,0,0]
				 			  },

				 			  title: {
				 			    text: ''
				 			  },
				 			  credits:{
				 				enabled: false
				 			  },
				 			  exporting: { 
				 				enabled: false 
				 			  },
				 			  plotOptions:{
								series:{
					 			  	colors : [ '#2f7ed8', '#0d233a', '#FF0000'],
					 			  	//stroke-width : 0으로 주는 함수
									borderWidth : 0
								},
								bar : {
									borderWidth: 0
								}
							  },
				 			  symbol : { 
				 			    enabled: false
				 			  },
				 			  accessibility: {
				 			    announceNewData: {
				 			      enabled: false
				 			    }
				 			  },
				 			  subtitle: {
				 			    text: ''
				 			  },
				 			  tooltip: {
				 			      enabled: false
				 			    },
				 			  xAxis: {
				 			    type: 'category',
				 			    crosshair: false,
				 			   minPadding: 0,
				 			  maxPadding: 0,
				 			  tickWidth: 0,
				 			  lineWidth: 0,
				 			    visible:false,
				 			   maxPadding:0,
				 			     labels:{
				 			      enabled: false
				 			    }
				 			  },
				 			  
				 			  yAxis: {
				 				crosshair: false,
				 			    padding : 0,
				 			    margin : 0,
				 			   visible:false,
				 			    pointPadding: 0,
				 			    borderWidth: 0,
				 			   maxPadding:0,
				 			    title: {
				 			      text: ''
				 			    },
				 			    labels:{
				 			      enabled: false
				 			    }
				 			  },

				 			  caption: {
				 			    text: ''
				 			  },

				 			  legend: {
				 			    enabled: false
				 			  },

				 			  series: [{
				 			    name: 'Labor Costs',
				 			    data: [
				 			      ['Norway', 50.2, 1],
				 			      ['Denmark', 42, 1],
				 			      ['Belgium', 39.2, 1]
				 			    ],
				 			    dataLabels: {
				 			      enabled: false
				 			    },
				 			    tooltip: {
				 			      enabled: false
				 			    },
				 			    colorByPoint: true
				 			  }]

				 			});
			 		}else{
				 		Highcharts.chart(markerLatLng[i].ename, {

				 			  chart: {
				 			    type: 'variwide',
				 			   //차트뒤에 div를 감춤
				 			   backgroundColor: 'transparent'
				 			   //div안에 차트 padding 먹이는것
				 			   ,spacing: [0,0,0,0]
				 			  },
							  //타이틀 없애기
				 			  title: {
				 			    text: ''
				 			  },
				 			  //크래딧 없애기
				 			  credits:{
				 				    enabled: false
				 			  },
				 			  
				 			  exporting: { 
				 				  enabled: false 
				 			  },
				 			  plotOptions:{
									series:{
										//막대기 컬러 지정
						 			  	colors : [ '#2f7ed8', '#0d233a', '#FF0000'],
						 			  	//stroke-width : 0으로 주는 함수
										borderWidth : 0
									}
							  },
				 			  symbol : { 
				 			    enabled: false
				 			  },
				 			  accessibility: {
				 			    announceNewData: {
				 			      enabled: false
				 			    }
				 			  },
				 			  subtitle: {
				 			    text: ''
				 			  },
				 			  tooltip: {
				 			      enabled: false
				 			    },
				 			  xAxis: {
				 			    type: 'category',
				 			    crosshair: false,
				 			   minPadding: 0,
				 			  maxPadding: 0,
				 			  tickWidth: 0,
				 			  lineWidth: 0,
				 			    visible:false,
				 			   maxPadding:0,
				 			     labels:{
				 			      enabled: false
				 			    }
				 			  },
				 			  
				 			  yAxis: {
				 				crosshair: false,
				 			    padding : 0,
				 			    margin : 0,
				 			   visible:false,
				 			    pointPadding: 0,
				 			    borderWidth: 0,
				 			   maxPadding:0,
				 			    title: {
				 			      text: ''
				 			    },
				 			    labels:{
				 			      enabled: false
				 			    }
				 			  },
				 			  caption: {
				 			    text: ''
				 			  },

				 			  legend: {
				 			    enabled: false
				 			  },

				 			  series: [{
				 			    name: 'Labor Costs',
				 			    data: [
				 			      ['Norway', 50.2, 1],
				 			      ['Denmark', 42, 1],
				 			      ['Belgium', 39.2, 1]
				 			    ],
				 				shapes: [{
				 					strokeWidth: 0
				 				}],
				 			    dataLabels: {
				 			      enabled: false
				 			    },
				 			    tooltip: {
				 			      enabled: false
				 			    },
				 			    colorByPoint: true
				 			  }]

				 			});
			 		}
			 	}
			}
			

		$(document).on('click', '#reset', function(obj){
			kkoMap.initKko({
				mapId :"map"
				,option :""
			});
			
			fn_setChart();
		})
			
		</script>
	</head>
	<body>
		
	<div class="chartbox" style="margin-top:100px;">
		<div style="width:30%;height:50px;margin-left: 100px;">
			<button id="reset" type="button" class="btn btn-primary" style="margin-left: 10px;">초기화</button>
		</div>
		<div class="kakaoMapArea" id ="map" style="width:40%;height:600px;margin-left: 100px;">
			
		</div>
	</div>   
	</body>
</html> 