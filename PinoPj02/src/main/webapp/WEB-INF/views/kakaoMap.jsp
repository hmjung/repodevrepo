<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%-- <meta name="_csrf" th:content="${_csrf.token}"/> --%>
<%-- <meta name="_csrf_header" th:content="${_csrf.headerName}"/> --%>
<title>오버레이</title>
<!-- 부트스트랩 CSS -->
<!-- 부트스트랩 JS -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<!-- 카카오지도 JS -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=4d7d35bade5ba376fb1b947fccaaa67c"></script>

<!-- 하이차트 JS -->
<script src="resources/lib/highcharts/highcharts2021/highcharts.js"></script>
<script src="resources/lib/highcharts/highcharts2021/highcharts-more.js"></script>
<script src="resources/lib/highcharts/highcharts2021/modules/heatmap.js"></script>
<script src="resources/lib/highcharts/highcharts2021/modules/treemap.js"></script>
<script src="resources/lib/highcharts/highcharts2021/modules/variwide.js"></script>
<script src="resources/lib/highcharts/highcharts2021/modules/exporting.js"></script>
<script src="resources/lib/highcharts/highcharts2021/modules/export-data.js"></script>
<script src="resources/lib/highcharts/highcharts2021/modules/accessibility.js"></script>

<!-- 차트 & 공통함수 JS -->
<!-- <script src="resources/js/common.js"></script> -->
<!-- <script src="resources/js/modules.js"></script> -->

<!-- 전용CSS -->
<!-- <link href="resources/css/header.css" rel="stylesheet"> -->
<!-- <link href="resources/css/main.css" rel="stylesheet"> -->
<!-- <link href="resources/css/footer.css" rel="stylesheet"> -->

</head>
<header>
	
</header>
<body>
	<div class="chartbox" style="margin-top:100px;">
		<div style="width:30%;height:50px;margin-left: 100px;">
			<button onclick="location.href='./'" type="button" class="btn btn-primary" style="margin-left: 10px;">홈으로</button>
		</div>
		<div style="width:30%;height:50px;margin-left: 100px;">
			<button id="reset" type="button" class="btn btn-primary" style="margin-left: 10px;">초기화</button>
		</div>
		<div class="kakaoMapArea" id ="map" style="width:506px;height:560px;margin-left: 100px;">
			
		</div>
		<div class="polarChartArea" id ="polarChartArea" style="width:40%;height:600px;margin-left: 100px;">
			
		</div>
		<div class="treemapChartArea" id ="treemapChartArea" style="width:40%;height:600px;margin-left: 100px;">
			
		</div>
	</div>
<!-- <script src="resources/js/geoJson.js"></script> -->
<!-- <script src="resources/js/geoJson2.js"></script> -->
<script src="resources/js/charts.js"></script>
<script>

//전국시 좌표
var markerLatLng = [];

//지역좌표
var geoLatLng = [	
	//오버레이 좌표는 0.48정도 더하면 적당
	//latLngY, latLngX : 지역 센터 좌표 
	//zoomLatLngX, zoomLatLngY : 줌이벤트 이후 좌표 
	//zoomLevel : 줌 실행시 줌 레벨
	{latLngY : 37.5666805, latLngX : 126.8984147, name:'서울특별시', ename:'Seoul', zoomLatLngY : 37.56390197839331, zoomLatLngX : 126.98358535766602, zoomLevel : 10, color:'#DFF2FF'}, //서울특별시 완료!!
	{latLngY : 37.592471511019085, latLngX : 128.375244140625, name:'강원도', ename:'Gangwon-do', zoomLatLngY : 37.67505900514996, zoomLatLngX : 128.3477783203125, zoomLevel : 12, color:'#DFF2FF'}, //강원도 완료!!
	{latLngY : 37.47485808497102, latLngX : 126.61743164062499, name:'인천광역시', ename:'Incheon', zoomLatLngY : 37.47594794878128, zoomLatLngX : 126.33590698242186, zoomLevel : 11, color:'#DFF2FF'}, //인천광역시 완료 !!
	{latLngY : 35.14237113713991, latLngX : 126.75814453125, name:'광주광역시', ename:'Gwangju', zoomLatLngY : 35.160336728130346, zoomLatLngX : 126.81514453125, zoomLevel : 9, color:'#DFF2FF'}, //광주광역시 완료
	{latLngY : 33.39705230475205, latLngX : 126.5460205078125, name:'제주특별자치도', ename:'Jeju-do', zoomLatLngY : 33.390172864722466, zoomLatLngX : 126.55700683593749, zoomLevel : 11, color:'#DFF2FF'}, //제주특별자치도 완료
	{latLngY : 34.84762101276787, latLngX : 126.78497314453125, name:'전라남도', ename:'Jellanam-do', zoomLatLngY : 34.35799531086792, zoomLatLngX : 126.58435058593749, zoomLevel : 12, color:'#DFF2FF'}, //전라남도 완료
	{latLngY : 35.180543276002666, latLngX : 129.06463623046875, name:'부산광역시', ename:'Busan', zoomLatLngY : 35.17128256489428, zoomLatLngX : 129.05845642089844, zoomLevel : 10, color:'#DFF2FF'}, //부산광역시 완료 !!
	{latLngY : 35.54787066533268, latLngX : 129.254150390625, name:'울산광역시', ename:'Ulsan', zoomLatLngY : 35.54619462154643, zoomLatLngX : 129.25483703613278, zoomLevel : 10, color:'#DFF2FF'}, //울산광역시 완료 !!
	{latLngY : 35.860117799832544, latLngX : 128.55377197265622, name:'대구광역시', ename:'Daegu', zoomLatLngY : 35.84773489516852, zoomLatLngX : 128.5587501525879, zoomLevel : 10, color:'#DFF2FF'}, //대구광역시 완료 !!
	{latLngY : 35.106428057364255, latLngX : 128.08135986328125, name:'경상남도', ename:'Gyeongsangnam-do', zoomLatLngY : 35.43381992014202, zoomLatLngX : 128.32031249999997, zoomLevel : 12, color:'#DFF2FF'}, //경상남도 완료!!
	{latLngY : 36.301845303684324, latLngX : 128.9794921875, name:'경상북도', ename:'Gyeongsangbuk-do', zoomLatLngY : 36.379279167407965, zoomLatLngX : 128.76800537109375, zoomLevel : 12, color:'#DFF2FF'}, //경상북도 완료!!
	{latLngY : 37.32430451813815, latLngX : 127.40359130859374, name:'경기도', ename:'Gyeonggi-do', zoomLatLngY : 37.58376576718623, zoomLatLngX : 127.108447265625, zoomLevel : 11, color:'#DFF2FF'}, //경기도!!
	{latLngY : 36.31052700542763, latLngX : 127.31098144531249, name:'대전광역시', ename:'Daejeon', zoomLatLngY : 36.3472087646811, zoomLatLngX : 127.38304138183592, zoomLevel : 9, color:'#DFF2FF'}, //대전광역시 완료 !!
	{latLngY : 36.58301312197295, latLngX : 127.2130908203125, name:'세종특별자치시', ename:'Sejong-si', zoomLatLngY : 36.56984507478879, zoomLatLngX : 127.29583740234375, zoomLevel : 9, color:'#DFF2FF'}, //세종특별자치시 완료!!
	{latLngY : 36.461054075054314, latLngX : 126.749267578125, name:'충청남도', ename:'Chungcheongnam-do', zoomLatLngY : 36.465471886798134, zoomLatLngX : 126.90350097656249, zoomLevel : 11, color:'#DFF2FF'}, //충청남도 완료
	{latLngY : 36.7240126988417, latLngX : 127.7215576171875, name:'충청북도', ename:'Chungcheongbuk-do', zoomLatLngY : 36.671273880045004, zoomLatLngX : 127.95070800781249, zoomLevel : 11, color:'#DFF2FF'}, //충청북도 완료
	{latLngY : 35.67068501330236, latLngX : 127.2161865234375, name:'전라북도', ename:'Jeollabuk-do', zoomLatLngY : 35.71975793933433, zoomLatLngX : 127.04040527343749, zoomLevel : 11, color:'#DFF2FF'}, //전라북도 완료
	{latLngY : 33.32593850874471, latLngX : 129.627685546875, name:'차트영역', ename:'summary', zoomLatLngY : 36.10237644873644, zoomLatLngX : 128.21044921874997, zoomLevel : 13, color:'#DFF2FF'}, //차트영역 완료
	{latLngY : 36.0546781, latLngX : 128.6806144, name:'전국지도', ename:'korea', zoomLatLngY : 35.70237644873644, zoomLatLngX : 128.21044921874997, zoomLevel : 13, color:'#DFF2FF'} //전국지도 완료
];

for(var i = 0; i < geoLatLng.length; i++){
	markerLatLng.push({
		latLng : new kakao.maps.LatLng(geoLatLng[i].latLngY,geoLatLng[i].latLngX), 
		name:geoLatLng[i].name,
		ename:geoLatLng[i].ename,
		zoomLatLng : new kakao.maps.LatLng(geoLatLng[i].zoomLatLngY, geoLatLng[i].zoomLatLngX),
		zoomLevel : geoLatLng[i].zoomLevel,
		color : geoLatLng[i].color
	})
}

var map

$(function () {
	initSetPolygon();
// 	fn_setPolarChart();
// 	fn_setTreemapChart();
});

//전체 지도 폴리곤 ajax
function initSetPolygon(){
	fn_jsonAjax( {url : '/resources/js/geoJson.json', name : '전국지도', success : function(data){ setPolygon(data, '전국지도');	 }} ); //전국지도
};

function fn_jsonAjax(obj){
	var $success = (!fn_isNull(obj.success)) ? obj.success : function(data) {
		if (obj.debug == 1) console.log(data);
	};
	
	$.ajax({
		async : true,
		url : obj.url,
		type : 'GET',
		dataType : 'json',
		success : $success
	}).fail(function(request, status, error) {	
		console.log(status + " error > " + error);
	});
}

//폴리곤 생성
function setPolygon(data, name){
	var centerLatLng = new kakao.maps.LatLng(37.57669160058646, 126.98753356933594);
	var setLevel = 9;
	
	//센터 좌표 및 레벨지정
	for(var i = 0; i < markerLatLng.length; i++){
		if(markerLatLng[i].name == name){
			centerLatLng = markerLatLng[i].zoomLatLng;
			setLevel = markerLatLng[i].zoomLevel;
		}
	}
	
	//맵옵션설정(좌표, 줌레벨, div설정)
	var container = document.getElementById('map'),
    options = {
         center: centerLatLng,
         level: setLevel
    };
 
	//맵생성
	var map = new kakao.maps.Map(container, options);
	
	var PolygonData = data.features;
	console.log('PolygonData', PolygonData)
	//폴리곤 좌표 입력
	for(var i = 0; i < PolygonData.length ; i++){
		for(var j = 0; j < PolygonData[i].geometry.coordinates.length ; j++){
			
			var $pathsData = new Array();
			var $fillColor
			
			for(var k = 0; k < PolygonData[i].geometry.coordinates[j].length ; k++){
				$pathsData.push(new kakao.maps.LatLng(PolygonData[i].geometry.coordinates[j][k][1], PolygonData[i].geometry.coordinates[j][k][0]));
			}
			
			for(var k = 0; k<markerLatLng.length; k++){
				if(PolygonData[i].properties.CTP_KOR_NM == markerLatLng[k].name) $fillColor = markerLatLng[k].color;
			}

			// 지도에 표시할 선을 생성합니다
			var polygon = new kakao.maps.Polygon({
				name:'테스트',
			    path: $pathsData, // 그려질 다각형의 좌표 배열입니다
			    strokeWeight: 1, // 선의 두께입니다
			    strokeColor: 'black', // 선의 색깔입니다
			    strokeOpacity: 0.8, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
			    fillColor: $fillColor, // 채우기 색깔입니다
			    fillOpacity: 0.7 // 채우기 불투명도 입니다
			});
			
			fn_polygonEvent(polygon, PolygonData[i].properties.CTP_KOR_NM);
			
			// 지도에 다각형을 표시합니다
			polygon.setMap(map);
			
		}
	}
	
	//전국지도 표기시 차트용 오버레이 그리는 이벤트
	if(name == '전국지도') setOverlay(map);
	
	//차트들 파라미터 세팅
	$param1 = {divCD : "테스트", category : "테스트", areaName : "테스트", code : "테스트"}
	$param2 = {divCD : "테스트", category : "테스트", areaName : "테스트", code : "테스트"}
	$param3 = {leftCtpvName  : "테스트", rightCtpvName : "테스트", leftCtpvCode  : "테스트", rightCtpvCode  : "테스트"}
	
	//차트 ajax
	if(name == '전국지도') fn_apiAjax( { url : 'http://192.168.0.105:29090/api/search/mapdata', data:fn_setParam($param1), type : 'GET', success : function(data) {  fn_setChart(data);} }); // 지도에 표시할 T-Score 점수 조회
	if(name == '전국지도') fn_apiAjax( { url : 'http://192.168.0.105:29090/api/search/radr', data:fn_setParam($param2), type : 'GET', success : function(data) { fn_setPolarChart(data); } }); // 방사형 그래프와 순위표 조회
	if(name == '전국지도') fn_apiAjax( { url : 'http://192.168.0.105:29090/api/search/comp', data:fn_setParam($param3), type : 'GET', success : function(data) { fn_setTreemapChart(data, 'left'); } }); // 지역별 범주별 T-Score 비교 분석
	if(name == '전국지도') fn_apiAjax( { url : 'http://192.168.0.105:29090/api/search/comp', data:fn_setParam($param3), type : 'GET', success : function(data) { fn_setTreemapChart(data, 'right'); } }); // 지역별 범주별 T-Score 비교 분석
};

//차트넣을 오버레이 띄우기
function setOverlay(map){
	for(var i = 0; i<markerLatLng.length; i++){
		//그려진 차트가 있다면 ID를 삭제 (초기화 버튼용)
		if($('#'+markerLatLng[i].ename).length > 0) $('#'+markerLatLng[i].ename).removeAttr('id');
		
		var content = '';
		
		//전국지도용 오버레이
		if(markerLatLng[i].ename != 'summary' && markerLatLng[i].ename != 'korea'){
			content += '<div class="overlay" rel="'+markerLatLng[i].name+'" style="width:30px; height: 40px; z-index: 1;">'
			content += '<div class="'+markerLatLng[i].ename+'" id="'+markerLatLng[i].ename+'" style="width:30px; height: 20px; z-index: 1;"></div>';
			content += '<div style="width:30px; height: 20px; z-index: 1; font-size:10px; color:white">'+markerLatLng[i].name+'</div>';
			content += '</div>';
		//sumList 차트용 오버레이
		}else if(markerLatLng[i].ename != 'korea'){
			content += '<div class="overlay" rel="'+markerLatLng[i].name+'" style="width:180px; height: 120px; z-index: 1;">'
			content += '<div class="'+markerLatLng[i].ename+'" id="'+markerLatLng[i].ename+'" style="width:180px; height: 120px; z-index: 1;"></div>';
			content += '<div style="width:180px; height: 120px; z-index: 1; font-size:15px; color:black; text-align:center;">'+markerLatLng[i].name+'</div>';
			content += '</div>';
		}
		// 커스텀 오버레이가 표시될 위치입니다 
		var position = markerLatLng[i].latLng;

		// 커스텀 오버레이를 생성합니다
		var customOverlay = new kakao.maps.CustomOverlay({
		    position: position,
		    content: content   
		});
		
		// 커스텀 오버레이를 지도에 표시합니다
		customOverlay.setMap(map);
	}
}

//지역 클릭시 이벤트
$(document).on('click', '.overlay', function(obj){
	fn_zoomEvent($(this).attr('rel'));
})

//폴리곤 클릭 이벤트 핸들러
function fn_polygonEvent(polygon, name){
	kakao.maps.event.addListener(polygon, 'click', function() {
		if(typeof(name) != 'undefined'){
			//확대이벤트
	 		fn_zoomEvent(name);
		}
	}); 
}

//폴리곤 클릭시 확대이벤트
function fn_zoomEvent(name){
	var $url
	
	for(var i = 0; i < markerLatLng.length; i++){
		if(markerLatLng[i].name == name){
			$url = '/resources/js/geoJson/'+markerLatLng[i].ename+'_geo.json';
		}
	}
	
	//geo데이터 불러와서 그리는 ajax
	fn_jsonAjax( {url : $url, name:name, success : function(data){ setPolygon(data, name);  }} ); 
}

//초기화버튼
$(document).on('click', '#reset', function(obj){
	initSetPolygon();
})
</script>
</body>
</html>