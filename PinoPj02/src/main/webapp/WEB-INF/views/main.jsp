<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>지도때스뚜</title>
<!-- 부트스트랩 CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<!-- 부트스트랩 JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<!-- 네이버지도 JS -->
<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?ncpClientId=0spszca8w6"></script>
<!-- 암차트 JS -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!-- 전용CSS -->
<link href="resources/css/header.css" rel="stylesheet">
<link href="resources/css/main.css" rel="stylesheet">
<link href="resources/css/footer.css" rel="stylesheet">
</head>
<body>
<div width="100%" style="display: flex; margin-top:100px;">
	<div class="naverMapArea" id ="map" style="width:40%;height:600px;margin-left: 100px;">
		
	</div>
	<div class="naverMapArea2" id ="map2" style="width:40%;height:600px;margin-left: 100px;">
		
	</div>
	
	<button onclick="location.href='./overlay'" type="button" class="btn btn-primary" style="margin-left: 10px;">다음페이지</button>
	<button onclick="location.href='./nextMap'" type="button" class="btn btn-primary" style="margin-left: 10px;">다른지도</button>
	<button onclick="location.href='./otherMap'" type="button" class="btn btn-primary" style="margin-left: 10px;">다른지도2</button>
	<button onclick="location.href='./kakaoMap'" type="button" class="btn btn-primary" style="margin-left: 10px;">카카오맵</button>
	<button onclick="location.href='./kakao2'" type="button" class="btn btn-primary" style="margin-left: 10px;">카카오맵22</button>
</div>

<div style="margin-top: 100px;">
	
</div>
<script src="resources/js/geoJson.js"></script>
<script>
//--------------------------커스텀 오버레이부분--------------------------//
// var CustomOverlay = function(options) { 
//     this._element = $('<div id = "chartArea" style="position:absolute;left:0;top:10px;  background-color: rgba(47,138,241,0); z-index: 1000;">' +
//     					'<p style="margin:0px; z-index: 1000;">대구</p>'+
//     					'<span class="badge bg-primary rounded-pill">2</span>'+
//                         '</div>')

//     this.setPosition(options.position);
//     this.setMap(options.map || null);
// };

// CustomOverlay.prototype = new naver.maps.OverlayView();
// CustomOverlay.prototype.constructor = CustomOverlay;

// CustomOverlay.prototype.setPosition = function(position) {
//     this._position = position;
//     this.draw();
// };

// CustomOverlay.prototype.getPosition = function() {
//     return this._position;
// };

// CustomOverlay.prototype.onAdd = function() {
//     var overlayLayer = this.getPanes().overlayLayer;

//     this._element.appendTo(overlayLayer);
// };

// CustomOverlay.prototype.draw = function() {
//     if (!this.getMap()) {
//         return;
//     }

//     var projection = this.getProjection(),
//         position = this.getPosition(),
//         pixelPosition = projection.fromCoordToOffset(position);

//     this._element.css('left', pixelPosition.x);
//     this._element.css('top', pixelPosition.y);
// };

// CustomOverlay.prototype.onRemove = function() {
//     var overlayLayer = this.getPanes().overlayLayer;

//     this._element.remove();
//     this._element.off();
// };

//오른쪽지도 오픈
var position2 = new naver.maps.LatLng(37.3849483, 127.1229117);
var map2 = new naver.maps.Map("map2", {
    center: cityhall,
    zoom: 7 
});

//--------------------------/커스텀 오버레이부분--------------------------//


//--------------------------마커 및 정보창(그래프)부분--------------------------//
var GeoJson
var marker
//지정 좌표 저장
var cityhall = new naver.maps.LatLng(35.9157474194997, 127.891845703125)
var kangwondo = new naver.maps.LatLng(219.23449263214843,98.88889561866402)

//전국시 좌표
var markerLatLng = [];
//오버레이 좌표
var customOverlayLatLng = [];

var geoLatLng = [	
// 	//오버레이 좌표는 0.48정도 더하면 적당 
// 	{latLng : new naver.maps.LatLng(37.5666805, 126.9784147), name:'서울특별시', ename:'seoul'},
// 	{latLng : new naver.maps.LatLng(37.592471511019085,128.375244140625), name:'강원도', ename:'kangwon'},
// 	{latLng : new naver.maps.LatLng(37.47485808497102,126.61743164062499), name:'인천광역시', ename:'incheon'},
// 	{latLng : new naver.maps.LatLng(36.35052700542763,127.38098144531249), name:'대전광역시', ename:'deajeon'},
// 	{latLng : new naver.maps.LatLng(35.14237113713991,126.84814453125), name:'광주', ename:'gwangju'},
// 	{latLng : new naver.maps.LatLng(33.39705230475205,126.5460205078125), name:'제주', ename:'jeju'},
// 	{latLng : new naver.maps.LatLng(34.84762101276787,126.78497314453125), name:'전라남도', ename:'jeollanamdo'},
// 	{latLng : new naver.maps.LatLng(35.180543276002666,129.06463623046875), name:'부산', ename:'busan'},
// 	{latLng : new naver.maps.LatLng(35.54787066533268,129.254150390625), name:'울산', ename:'ulsan'},
// 	{latLng : new naver.maps.LatLng(35.860117799832544,128.55377197265622), name:'대구', ename:'daegu'},
// 	{latLng : new naver.maps.LatLng(35.106428057364255,128.08135986328125), name:'경상남도', ename:'jyeongsangnamdo'},
// 	{latLng : new naver.maps.LatLng(36.301845303684324,128.9794921875), name:'경상북도', ename:'jyeongsangbukdo'},
// 	{latLng : new naver.maps.LatLng(37.32430451813815,127.49359130859374), name:'경기도', ename:'Gyeonggido'},
// 	{latLng : new naver.maps.LatLng(36.35052700542763,127.38098144531249), name:'대전', ename:'deajeon'},
// 	{latLng : new naver.maps.LatLng(36.50301312197295,127.2930908203125), name:'세종', ename:'sejong'},
// 	{latLng : new naver.maps.LatLng(36.461054075054314,126.749267578125), name:'충청남도', ename:'chungcheongnamdo'},
// 	{latLng : new naver.maps.LatLng(36.6640126988417,127.7215576171875), name:'충청북도', ename:'chungcheongbukdo'},
// 	{latLng : new naver.maps.LatLng(35.67068501330236,127.2161865234375), name:'전라북도', ename:'jeollabukdo'}
	
	//오버레이 좌표는 0.48정도 더하면 적당 
	{latLngY : 37.5666805, latLngX : 126.8984147, name:'서울특별시', ename:'seoul'},
	{latLngY : 37.592471511019085, latLngX : 128.375244140625, name:'강원도', ename:'kangwon'},
	{latLngY : 37.47485808497102, latLngX : 126.61743164062499, name:'인천광역시', ename:'incheon'},
	{latLngY : 35.14237113713991, latLngX : 126.75814453125, name:'광주', ename:'gwangju'},
	{latLngY : 33.39705230475205, latLngX : 126.5460205078125, name:'제주', ename:'jeju'},
	{latLngY : 34.84762101276787, latLngX : 126.78497314453125, name:'전라남도', ename:'jeollanamdo'},
	{latLngY : 35.180543276002666, latLngX : 129.06463623046875, name:'부산', ename:'busan'},
	{latLngY : 35.54787066533268, latLngX : 129.254150390625, name:'울산', ename:'ulsan'},
	{latLngY : 35.860117799832544, latLngX : 128.55377197265622, name:'대구', ename:'daegu'},
	{latLngY : 35.106428057364255, latLngX : 128.08135986328125, name:'경상남도', ename:'jyeongsangnamdo'},
	{latLngY : 36.301845303684324, latLngX : 128.9794921875, name:'경상북도', ename:'jyeongsangbukdo'},
	{latLngY : 37.32430451813815, latLngX : 127.40359130859374, name:'경기도', ename:'Gyeonggido'},
	{latLngY : 36.31052700542763, latLngX : 127.31098144531249, name:'대전', ename:'deajeon'}, 
	{latLngY : 36.58301312197295, latLngX : 127.2130908203125, name:'세종', ename:'sejong'},  
	{latLngY : 36.461054075054314, latLngX : 126.749267578125, name:'충청남도', ename:'chungcheongnamdo'},
	{latLngY : 36.7240126988417, latLngX : 127.7215576171875, name:'충청북도', ename:'chungcheongbukdo'},
	{latLngY : 35.67068501330236, latLngX : 127.2161865234375, name:'전라북도', ename:'jeollabukdo'}
];

for(var i = 0; i < geoLatLng.length; i++){
	markerLatLng.push({latLng : new naver.maps.LatLng(geoLatLng[i].latLngY,geoLatLng[i].latLngX), name:geoLatLng[i].name, ename:geoLatLng[i].ename})
}

for(var i = 0; i < geoLatLng.length; i++){
	customOverlayLatLng.push({latLng : new naver.maps.LatLng(geoLatLng[i].latLngY+0.35,geoLatLng[i].latLngX-0.35), name:geoLatLng[i].name, ename:geoLatLng[i].ename})
}
//지도의 옵션을 지정한다. 
var mapOptions = {
	//초기 지도 표현시 포커스 좌표
    center: cityhall,
    //초기 지도 줌 레벨
    zoom: 7
};

//맵 인스턴스 생성
var map = new naver.maps.Map('map', mapOptions);

var markers = [];
var infoWindows = [];

//마커&정보창을 지정
for(var i = 0; i < markerLatLng.length; i++){
// 	marker = new naver.maps.Marker({
// 		//마커를 표시할 지도 
// 	    map: map,
// 	    //마커를 표시할 좌표 정보 
// 	    position: markerLatLng[i].latLng, 
// 	    title: markerLatLng[i].ename
// 	});
	
// 	var contentString = [
// 	    '<div style="width:100px;height:100px; background-color: rgba(47,138,241,0);">',
// 	    '<div id="'+markerLatLng[i].ename+'chartdiv" style="width:100px;height:100px; background-color: rgba(47,138,241,0);"></div>',
// 	    '<p style="margin-right:10px; margin-left:10px;">'+markerLatLng[i].name+'</p>',
// 	    '</div>'
// 	].join('');

// 	var infowindow = new naver.maps.InfoWindow({
// 		content: contentString,
// 	    maxWidth: 100,
// 	    maxHeight:100,
// 	    backgroundColor: "#eee",
// 	    borderColor: "#3288ff",
// 	    borderWidth: 1,
// 	    anchorSize: new naver.maps.Size(30, 30),
// 	    anchorSkew: false,
// 	    anchorColor: "#eee",
// 	    disableAnchor:true,
// 	    pixelOffset: new naver.maps.Point(0, -30)
// 	});

//     markers.push(marker);
//     infoWindows.push(infowindow);	

	var CustomOverlay = function(options) { 
	    this._element = $('<div id = "chartArea" style="position:absolute;left:0;top:10px;  background-color: rgba(47,138,241,0); z-index: 1000; text-align:center">' +
	    					'<p style="width:80px; margin:0px; z-index: 1000;">'+customOverlayLatLng[i].name+'</p>'+
	    					'<span class="badge bg-primary rounded-pill" style="font-size:medium; color:white;">2</span>'+
	                        '</div>')
	
	    this.setPosition(options.position);
	    this.setMap(options.map || null);
	};

	CustomOverlay.prototype = new naver.maps.OverlayView();
	CustomOverlay.prototype.constructor = CustomOverlay;
	
	CustomOverlay.prototype.setPosition = function(position) {
	    this._position = position;
	    this.draw();
	};

	CustomOverlay.prototype.getPosition = function() {
	    return this._position;
	};

	CustomOverlay.prototype.onAdd = function() {
	    var overlayLayer = this.getPanes().overlayLayer;

	    this._element.appendTo(overlayLayer);
	};

	CustomOverlay.prototype.draw = function() {
	    if (!this.getMap()) {
	        return;
	    }

	    var projection = this.getProjection(),
	        position = this.getPosition(),
	        pixelPosition = projection.fromCoordToOffset(position);

	    this._element.css('left', pixelPosition.x);
	    this._element.css('top', pixelPosition.y);
	};

	CustomOverlay.prototype.onRemove = function() {
	    var overlayLayer = this.getPanes().overlayLayer;

	    this._element.remove();
	    this._element.off();
	};
	
    //오버레이 넣기
    var overlay = new CustomOverlay({
        map: map,
        position: customOverlayLatLng[i].latLng
    });
    
  //오른쪽지도에 오버레이등록
//     var overlay = new CustomOverlay({
//         map: map2,
//         position: position2
//     });
}


//해당 마커의 인덱스를 seq라는 클로저 변수로 저장하는 이벤트 핸들러를 반환합니다.
function getClickHandler(seq) {
    return function(e) {
        var marker = markers[seq],
            infoWindow = infoWindows[seq];
		console.log('dddddd>>>>>>>',seq)
		console.log('dfdfdfdfdfdf>>>>>>>',e)
		console.log('dfff3333333>>>>>>>',marker.title)
        if (infoWindow.getMap()) {
            infoWindow.close();
        } else {
            infoWindow.open(map, marker);
            setInfoChart(marker.title)
        }
    }
}

for (var i=0, ii=markers.length; i<ii; i++) {
    naver.maps.Event.addListener(markers[i], 'click', getClickHandler(i));
}
//--------------------------/마커 및 정보창(그래프)부분--------------------------//


// naver.maps.Event.addListener(map, 'idle', function() {
// 	console.log('markers',markers)
//     updateMarkers(map, markers);
// });

// function updateMarkers(map, markers) {

//     var mapBounds = map.getBounds();
//     var marker, position;

//     for (var i = 0; i < markers.length; i++) {

//         marker = markers[i]
//         position = marker.getPosition();

//         if (mapBounds.hasLatLng(position)) {
//             showMarker(map, marker);
//         } else {
//             hideMarker(map, marker);
//         }
//     }
// }

// function showMarker(map, marker) {

//     if (marker.setMap()) return;
//     marker.setMap(map);
// }

// function hideMarker(map, marker) {

//     if (!marker.setMap()) return;
//     marker.setMap(null);
// }





// naver.maps.Event.addListener(marker, "click", function(e) {
// 	if (infowindow.getMap()) {
// 	    infowindow.close();
// 	} else {
// 	    infowindow.open(map, marker);
// 	}
// });


//--------------------------폴리곤 그리기--------------------------//


var tooltip = $('<div style="position:absolute;z-index:100;padding:5px 10px;background-color:#fff;border:solid 2px #000;font-size:14px;pointer-events:none;display:none;"></div>');

tooltip.appendTo(map.getPanes().floatPane);

map.data.setStyle(function(feature) {
    var styleOptions = { 
        fillColor: 'white',
        fillOpacity: 1,
        strokeColor: 'blue',
        strokeWeight: 0.5,
        strokeOpacity: 1
    };

    if (feature.getProperty('focus')) {
        styleOptions.fillOpacity = 0.6;
        styleOptions.fillColor = '#0f0';
        styleOptions.strokeColor = '#0f0';
        styleOptions.strokeWeight = 4;
        styleOptions.strokeOpacity = 1;
    }

    return styleOptions;
});

console.log('GeoJson', GeoJson)
for(var i = 0; i < GeoJson.features.length; i++){
	//fn_setPolygon(GeoJson.features[i].geometry.coordinates);
	  map.data.addGeoJson(GeoJson.features[i]);
}

// map.data.addListener('click', function(e) {
//     var feature = e.feature;

//     if (feature.getProperty('focus') !== true) {
//         feature.setProperty('focus', true);
//     } else {
//         feature.setProperty('focus', false);
//     }
// });

// map.data.addListener('mouseover', function(e) {
//     var feature = e.feature,
//         regionName = feature.getProperty('CTP_KOR_NM');

//     tooltip.css({
//         display: '',
//         left: e.offset.x,
//         top: e.offset.y
//     }).text(regionName);

//     map.data.overrideStyle(feature, {
//         fillOpacity: 0.6,
//         strokeWeight: 4,
//         strokeOpacity: 1
//     });
// });

map.data.addListener('mouseout', function(e) {
    tooltip.hide().empty();
    map.data.revertStyle();
});

// function fn_setPolygon(data){
// 	console.log('data',data)
// 	var $pathsData = new Array();
	
// 	for(var i = 0; i < data.length ; i++){
// 		for(var k = 0; k < data[i].length ; k++){
// 			$pathsData.push(new naver.maps.LatLng(data[i][k][1], data[i][k][0]));
// 		}
// 	}
	
// 	console.log('$pathsData', $pathsData); 
	
// 	var polygon = new naver.maps.Polygon({
// 	    map: map,
// 	    paths: [
// 	    	$pathsData
// 	    ],
// 	    fillColor: 'white',
// 	    fillOpacity: 1,
// 	    strokeColor: 'black',
// 	    strokeOpacity: 1,
// 	    strokeWeight: 3 
// 	});
// }

//--------------------------/폴리곤 그리기--------------------------//


//------------------------------------------암차트부분----------------------------------------//
/**
 * ---------------------------------------
 * This demo was created using amCharts 4.
 * 
 * For more information visit:
 * https://www.amcharts.com/
 * 
 * Documentation is available at:
 * https://www.amcharts.com/docs/v4/
 * ---------------------------------------
 */
/*
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Chart
var chart = am4core.create( "chartdiv", am4charts.XYChart );
chart.hiddenState.properties.opacity = 54; // this creates initial fade-in
chart.colors.step = 2;
chart.autoMargins= false;
chart.marginBottom= 0; 
chart.marginLeft=0;
chart.marginRight= 0;
chart.plotAreaBorderAlpha = 0;

// X axis
var xAxis = chart.xAxes.push( new am4charts.CategoryAxis() );
xAxis.dataFields.category = "x";
xAxis.renderer.grid.template.location = 0;
xAxis.renderer.minGridDistance = 10;
xAxis.renderer.labels.template.disabled = true;
xAxis.data = [{ x: "1" }, { x: "2" }, { x: "3" }];

// Y axis
var yAxis = chart.yAxes.push( new am4charts.CategoryAxis() );
yAxis.renderer.labels.template.disabled = true;
yAxis.renderer.grid.template.location = 100;
yAxis.renderer.minGridDistance = 1000;
yAxis.dataFields.category = "y";
yAxis.data = [{ y: "1" }, { y: "2" }, { y: "3" }, { y: "4" }, { y: "5" }, { y: "6" }, { y: "7" }, { y: "8" }, { y: "9" }, { y: "10" }];

// Legend


// Create series
function createSeries(name) {
  var series = chart.series.push( new am4charts.ColumnSeries() );
  series.dataFields.categoryX = "x";
  series.dataFields.categoryY = "y";
  series.sequencedInterpolation = false;
  series.defaultState.transitionDuration = 10000;
  series.name = name;

  // Set up column appearance
  var column = series.columns.template;
  column.strokeWidth = 0;
  column.strokeOpacity = 0;
  column.stroke = am4core.color( "#ffffff" );
  column.width = am4core.percent( 100 );
  column.height = am4core.percent( 100 );
  //column.column.cornerRadius(6, 6, 6, 6);
  
  return series;
}

var series1 = createSeries("Democratic");
series1.data = [
  { x: "1", y: "1" },
  { x: "1", y: "2" },
  { x: "1", y: "3" },
  { x: "1", y: "4" },
  { x: "1", y: "5" },
  { x: "1", y: "6" },
  { x: "1", y: "7" },
  { x: "1", y: "8" },
  { x: "1", y: "9" },
  { x: "1", y: "10" }
];

var series2 = createSeries("Republican");
series2.data = [
  { x: "3", y: "1" },
  { x: "3", y: "2" },
  { x: "3", y: "3" },
  { x: "3", y: "4" },
  { x: "3", y: "5" },
  { x: "3", y: "6" },
  { x: "3", y: "7" },
  { x: "3", y: "8" },
  { x: "3", y: "9" },
  { x: "3", y: "10" }
];

var series3 = createSeries("Libertarian");
series3.data = [
  { x: "2", y: "1" },
  { x: "2", y: "2" },
  { x: "2", y: "3" },
  { x: "2", y: "4" },
  { x: "2", y: "5" },
  { x: "2", y: "6" },
  { x: "2", y: "7" },
  { x: "2", y: "8" },
  { x: "2", y: "9" },
  { x: "2", y: "10" }
];*/
/**
 * ---------------------------------------
 * This demo was created using amCharts 4.
 * 
 * For more information visit:
 * https://www.amcharts.com/
 * 
 * Documentation is available at:
 * https://www.amcharts.com/docs/v4/
 * ---------------------------------------
 */
//  for(var i = 0; i < markerLatLng.length; i++){
	 
	
	function setInfoChart(name){
		// Themes begin
		 am4core.useTheme(am4themes_animated);
		 // Themes end

		 // Create chart instance
		 var chart = am4core.create(name+"chartdiv", am4charts.XYChart);
		
		 // Add data
		 chart.data = [{
		     "name": "John",
		     "points": 35654,
		     "color": chart.colors.next()
		 }, {
		     "name": "Damon",
		     "points": 65456,
		     "color": chart.colors.next()
		 }, {
		     "name": "Patrick",
		     "points": 45724,
		     "color": chart.colors.next()
		 }];

		 // Create axes
		 var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		 categoryAxis.dataFields.category = "name";
		 categoryAxis.renderer.grid.template.disabled = true;
		 categoryAxis.renderer.minGridDistance = 30;
		 categoryAxis.renderer.inside = true;
		 categoryAxis.renderer.labels.template.fill = am4core.color("#fff");
		 categoryAxis.renderer.labels.template.fontSize = 0;

		 var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		 valueAxis.renderer.grid.template.strokeDasharray = "4,4";
		 valueAxis.renderer.labels.template.disabled = true;
		 valueAxis.min = 0;

		 // Do not crop bullets
		 chart.maskBullets = false;

		 // Remove padding
		 chart.paddingBottom = 0;

		 // Create series
		 var series = chart.series.push(new am4charts.ColumnSeries());
		 series.dataFields.valueY = "points";
		 series.dataFields.categoryX = "name";
		 series.columns.template.propertyFields.fill = "color";
		 series.columns.template.propertyFields.stroke = "color";
		 //series.columns.template.column.cornerRadiusTopLeft = 15;
		 //series.columns.template.column.cornerRadiusTopRight = 15;
		 series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/b]";

		 // Add bullets
		 var bullet = series.bullets.push(new am4charts.Bullet());
		 var image = bullet.createChild(am4core.Image);
		 image.horizontalCenter = "middle";
		 image.verticalCenter = "bottom";
		 image.dy = 20;
		 image.y = am4core.percent(100);
		 image.propertyFields.href = "bullet";
		 image.tooltipText = series.columns.template.tooltipText;
		 image.propertyFields.fill = "color";
		 image.filters.push(new am4core.DropShadowFilter());
	}
//  }

//------------------------------------------/암차트부분----------------------------------------//

</script>
</body>
</html>