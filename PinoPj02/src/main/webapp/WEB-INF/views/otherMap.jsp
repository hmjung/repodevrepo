<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="No-Cache">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <title>지역이슈 분석</title>

    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="resources/css/animate.css" rel="stylesheet">
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom.css" rel="stylesheet">
    
    <!-- Mainly scripts -->
	<script src="resources/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="resources/js/jquery-ui.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	
    <script src="resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="resources/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- I check -->
	<script src="resources/js/icheck.min.js"></script>
    <!-- Tags Input -->
    <script src="resources/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<!-- Sweet alert -->
    <script src="resources/js/plugins/sweetalert/sweetalert.min.js"></script>
	 <!-- slick carousel-->
    <script src="resources/js/plugins/slick/slick.min.js"></script>
     <!-- Data picker -->
   <script src="resources/js/plugins/datapicker/bootstrap-datepicker.js"></script>
   <!-- maphilight -->
   <script src="resources/js/jquery.maphilight.min.js"></script>
   
   
    <script type="text/javascript" src="resources/js/kbms/layout/topmenu.js"></script>
	 <!-- Custom and plugin javascript -->
    <script src="resources/js/inspinia.js"></script>
	<script src="resources/js/plugins/pace/pace.min.js"></script>
	
	<!-- 암차트 JS -->
<!-- <script src="https://cdn.amcharts.com/lib/4/core.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/charts.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/plugins/wordCloud.js"></script> -->
<!-- <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script> -->
	
	<!-- 하이차트 JS -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/variwide.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
</head>

<body>

<div id="wrapper">
	<div id="header">

<form name="categoryForm" method="post">
<input type="hidden" name="categoryId">
<input type="hidden" name="categoryNm">
<input type="hidden" name="group_cd">
</form>
       
    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content">
			<div class="row">
				<div class="col-xs-12"  style="margin-top: 100px">
				<h2>지역별 주요 이슈 분석</h2>
				<div class="col-xs-5 area-map">
					<div class="ibox-content">
						<h3>지역본부</h3>
						<div>지도를 클릭하여 지역별 이슈를 분석하세요</div>
						<div class="map-korea text-center">
						<div class="inchoenChart" id="inchoenChart" style="position: absolute; top: 55px; left: 200px; width:30px; height: 20px; z-index: 1;  background-color: rgba(47,138,241,0); padding: 0px; margin: 0px;"></div> 
						<div class="seoulChart" id="seoulChart" style="position: absolute; top: 50px; left: 230px; width:30px; height: 20px; z-index: 1; background-color: rgba(47,138,241,0); padding: 0px; margin: 0px;"></div>
<!-- 						<div class="kangwonChart" style="position: absolute; top: 40px; left: 220px; width:50px; height: 40px; z-index: 1; background-color: rgba(47,138,241,0);"></div> -->
<!-- 						<div class="gwangjuChart" style="position: absolute; top: 40px; left: 220px; width:50px; height: 40px; z-index: 1; background-color: rgba(47,138,241,0);"></div> -->
<!-- 						<div class="jejuChart" style="position: absolute; top: 40px; left: 220px; width:50px; height: 40px; z-index: 1; background-color: rgba(47,138,241,0);"></div> -->
<!-- 						<div class="busanChart" style="position: absolute; top: 240px; left: 345px; width:50px; height: 40px; z-index: 1; background-color: rgba(47,138,241,0);"></div> -->
<!-- 							<img src="resources/img/map_korea.gif" class="korea-han" alt="" usemap="#Map"  /> -->
							<img src="resources/img/img-map.jpg" class="korea-han" alt="" usemap="#Map"  />
							
								<map name="Map" id="Map">
									<area href="#" class="btn-position1" alt="서울지역본부" shape="poly" coords="87,78,91,73,93,75,96,70,102,75,101,79,95,81,86,79"/>
									<area href="#" class="btn-position2" alt="남서울지역본부" shape="poly" coords="86,79,80,78,79,80,82,84,83,87,86,89,88,92,93,90,97,92,103,89,105,82,100,79,95,83,88,81"/>
									<area href="#" class="btn-position3" alt="인천지역본부" shape="poly" coords="74,71,74,63,66,63,66,73,69,76,70,86,74,94,77,88,80,78,75,72" />
									<area href="#" class="btn-position3" alt="인천지역본부" shape="poly" coords="64,61,64,74,60,75,56,73,56,67,54,64,57,56,63,59" />
									<area href="#" class="btn-position3" alt="인천지역본부" shape="poly" coords="67,85,59,89,55,84,64,81,67,84" />
									<area href="#" class="btn-position4" alt="경기북부지역본부" shape="poly" coords="106,23,99,25,98,27,89,27,83,32,83,38,90,39,89,43,85,45,85,54,81,48,75,50,79,52,76,57,77,64,74,72,83,78,90,73,96,70,102,77,102,81,107,83,112,88,117,90,117,93,127,96,133,97,140,102,144,95,150,86,131,79,130,63,135,54,127,45,126,39,119,39,117,34,114,35,107,25"  />
									<area href="#" class="btn-position5" alt="경기지역본부" shape="poly" coords="141,103,133,97,117,95,116,90,113,89,105,83,104,87,97,93,94,89,89,92,83,87,80,82,77,84,77,89,74,96,76,101,63,100,63,106,70,113,72,115,79,113,76,122,84,134,92,133,99,130,107,136,123,125,127,126,134,123,134,117,138,118,141,112" />
									<area href="#" class="btn-position6" alt="강원지역본부" shape="poly" coords="191,2,195,12,198,22,201,28,205,37,211,55,218,64,220,70,231,82,230,89,234,93,235,99,247,116,247,127,240,130,237,136,231,129,229,133,219,129,215,133,210,128,206,133,199,131,193,128,191,125,188,126,182,121,177,122,178,117,173,115,168,115,162,118,160,112,155,112,153,116,153,118,144,119,140,113,141,101,146,93,144,91,149,87,142,83,137,80,133,80,130,65,135,58,136,54,131,48,126,43,125,39,118,39,117,33,114,34,110,30,106,24,110,21,116,20,122,21,125,20,131,25,138,22,145,24,149,22,154,26,158,23,166,27,181,19,186,10,187,3" />
									<area href="#" class="btn-position7" alt="충북지역본부" shape="poly" coords="194,132,199,132,193,128,191,125,188,126,181,121,178,122,175,120,176,118,171,115,166,116,164,117,159,114,155,112,153,117,153,118,144,120,141,114,138,118,135,119,135,122,131,125,128,126,124,125,108,135,112,143,115,147,116,151,112,149,105,157,108,163,110,168,111,172,115,173,119,174,122,178,120,182,119,190,123,193,125,206,128,209,135,211,143,211,147,208,150,202,149,198,153,197,153,194,151,193,148,193,145,189,141,191,141,187,145,183,145,179,145,171,142,165,144,162,147,161,150,158,149,156,154,154,157,155,158,151,158,147,162,148,165,147,168,147,172,146,177,148,185,147,187,139,194,134" />
									<area href="#" class="btn-position8" alt="대전춘남지역본부" shape="poly" coords="80,137,84,134,94,133,99,130,107,135,114,146,115,150,111,150,107,154,106,160,111,170,114,174,119,174,121,180,120,185,120,191,122,194,125,200,125,208,125,210,121,208,117,214,113,208,109,210,107,201,101,203,88,203,85,197,79,197,75,201,71,204,66,206,59,201,56,195,51,195,55,187,53,180,56,176,53,174,52,153,48,156,45,154,50,163,48,171,43,165,42,153,40,147,35,147,35,143,31,143,36,134,42,128,46,134,51,131,48,130,51,123,57,125,57,121,67,123,74,126,77,132" />
									<area href="#" class="btn-position9" alt="전북지역본부" shape="poly" coords="66,207,52,208,56,211,58,216,71,216,62,219,68,225,61,224,56,229,45,236,47,240,56,241,48,245,42,252,44,257,48,262,58,259,62,251,68,250,76,257,81,251,82,255,83,265,85,267,95,266,100,268,108,263,117,268,123,261,122,251,124,236,137,223,143,217,141,211,127,207,122,209,116,213,108,207,106,202,88,202,81,197" />
									<area href="#" class="btn-position10" alt="광주전남지역본부" shape="poly" coords="40,252,40,258,32,268,39,280,39,284,34,283,28,290,33,293,36,293,33,299,34,308,28,308,25,313,26,320,32,325,33,325,37,331,38,336,37,341,41,343,47,338,55,336,59,325,60,335,65,336,72,333,75,323,84,319,90,315,95,314,96,317,93,318,83,324,81,331,92,336,98,337,104,328,108,326,102,317,105,309,109,307,112,309,113,318,115,323,119,318,127,318,126,308,120,308,115,302,122,301,128,295,124,284,118,271,108,263,99,268,91,264,86,266,81,256,75,257,69,251,59,259,48,263" />
									<area href="#" class="btn-position10" alt="광주전남지역본부" shape="poly" coords="25,321,23,324,14,330,15,340,28,335,32,329,27,322">
									<area href="#" class="btn-position11" alt="대구지역본부" shape="poly" coords="252,166,248,173,251,179,248,189,245,195,244,207,246,212,244,218,247,222,254,217,257,219,253,231,247,251,240,248,233,249,231,243,226,243,221,246,218,249,210,249,202,252,192,250,185,251,183,245,172,247,167,245,160,243,162,239,158,232,154,228,149,228,141,221,144,216,141,211,149,204,150,198,153,197,153,193,148,194,146,190,141,191,142,186,144,184,145,172,143,166,146,162,149,166,154,169,160,166,162,165,170,170,170,176,173,180,175,182,163,191,165,197,169,200,169,206,175,210,186,207,195,215,200,210,202,210,208,208,212,204,218,206,222,201,226,199,233,197,234,191,233,184,231,180,236,179,236,171,237,166" />
									<area href="#" class="btn-position11" alt="대구지역본부" shape="poly" coords="272,100,266,104,270,108,275,106">
									<area href="#" class="btn-position12" alt="경북지역본부" shape="poly" coords="248,127,249,132,251,134,251,140,250,150,255,158,252,166,241,165,237,165,235,170,236,175,234,179,230,178,231,181,234,185,234,188,236,192,234,194,230,200,224,196,223,200,221,200,217,206,212,204,208,207,200,210,197,214,192,214,191,210,189,210,186,207,177,210,172,209,169,206,169,199,162,193,165,188,170,185,172,186,175,182,174,178,170,178,170,170,162,163,158,167,158,169,152,170,148,164,148,159,152,155,158,155,158,149,166,148,172,146,176,149,180,148,185,147,187,141,192,136,197,131,204,132,211,128,215,132,219,130,227,133,232,130,236,133,245,128" />
									<area href="#" class="btn-position13" alt="부산울산지역본부" shape="poly" coords="246,250,246,259,244,266,241,264,238,269,239,273,231,280,231,286,223,293,219,295,207,297,209,293,202,296,201,289,194,282,194,272,202,268,208,264,212,261,214,256,218,250,219,248,226,243,231,243,234,247,242,248" />
									<area href="#" class="btn-position14" alt="경남지역본부" shape="poly" coords="141,221,136,224,131,225,124,235,120,247,122,253,123,259,120,262,117,271,120,276,123,282,124,286,129,292,129,295,123,298,124,302,128,305,133,301,135,301,131,307,133,312,132,317,140,314,142,320,146,318,147,311,143,309,137,306,139,299,144,295,147,294,147,303,153,306,157,306,159,305,162,305,164,303,166,304,166,307,169,310,165,314,172,320,175,310,173,304,174,300,178,297,177,293,181,292,185,294,186,284,193,292,200,295,200,289,194,280,194,271,205,266,219,250,207,250,200,252,196,250,185,250,184,245,174,248,167,243,163,243,160,242,162,236,153,228" />
									<area href="#" class="btn-position14" alt="경남지역본부" shape="poly" coords="193,299,194,304,192,308,196,311,188,318,187,322,183,325,181,314,182,311,177,307,187,302">
									<area href="#" class="btn-position15" alt="제주지역본부" shape="poly" coords="48,395,35,397,27,404,21,411,26,421,32,419,45,420,49,422,63,416,73,411,76,402,67,395" />
								</map>
							<div id="char4">
								<div id="position1" class="map-layer-info">
								  <h3>서울</h3>
									<p>분석데이터 수 : <font class="text-danger">11,098</font></p>
								</div>
								<div id="position2" class="map-layer-info">
								  <h3>남서울</h3>
									<p>분석데이터 수 : <font class="text-danger">11,098</font></p>
								</div>
								<div id="position3" class="map-layer-info">
								  <h3>인천</h3>
									<p>분석데이터 수 : <font class="text-danger">11,098</font></p>
								</div>
								<div id="position4" class="map-layer-info">
								  <h3>경기북부</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position5" class="map-layer-info">
								  <h3>경기</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position6" class="map-layer-info">
								  <h3>강원</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position7" class="map-layer-info">
								  <h3>충북</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position8" class="map-layer-info">
								  <h3>대전충남</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position9" class="map-layer-info">
								  <h3>전북</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position10" class="map-layer-info">
								  <h3>광주전남</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position11" class="map-layer-info">
								  <h3>대구</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position12" class="map-layer-info">
								  <h3>경북</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position13" class="map-layer-info">
								  <h3>부산울산</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position14" class="map-layer-info">
								  <h3>경남</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
								<div id="position15" class="map-layer-info">
								  <h3>제주</h3>
									<p>분석데이터 수 : <font class="text-danger">13,098</font></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-7 area-map-right">
					<div class="chart-title m-b-none clear">
						<h5><span class="text-primary">서울</span>지역본부 주요 이슈</h5>
					</div>
					<div>
						<div class="radio radio-primary radio-inline">
						<input type="radio" id="viewcate1" value="일반보기" name="viewecate" checked="">
						<label for="datecate1"> 일반보기 </label>
						</div>
						<div class="radio radio-primary radio- radio-inline">
							<input type="radio" id="viewcate2" value="긍/부정 나눠보기" name="viewcate">
							<label for="datecate2"> 긍/부정 나눠보기 </label>
						</div>
					</div>
					<div class="clear m-t-sm m-b-md">
						<div class="pull-left"><span class="badge badge-gradient-default">크기</span> 언급량</div>
						<div class="chart-button">
						   <button class="btn btn-outline btn-info btn-xs m-l-xs"><i class="fa fa-bar-chart-o"></i> 시각화 변경</button> 
						   <button class="btn btn-outline btn-primary btn-xs m-l-xs"><i class="fa fa-list-alt"></i> 데이터 보기</button> 
						   <button class="btn btn-gradient-secondary btn-xs m-l-xs"><i class="fa fa-dashboard"></i> 위젯 저장</button> 
						   <div class="select-hit">
								<div class="input-group">
									<select class="input-xs form-control inline">
										<option value="0">10건씩 조회</option>
										<option value="1">Option 2</option>
										<option value="2">Option 3</option>
										<option value="3">Option 4</option>
									</select>
									<span class="input-group-btn">
									<button type="button" class="btn btn-xs btn-default"> 적용</button> </span>
								</div>
							</div>
						</div>
					</div>					
					<div class="chart-box">
						<div id="bar-chart"></div>
					</div>
				</div>
				</div>
			</div>
        </div> <!-- end  wrapper content -->
        <div class="footer">
            <div class="pull-right">
                ☎ 포탈 관련 문의 : 061-1585 <a class="btn btn-sm btn-top" href="#">TOP <i class="fa fa-arrow-up"></i> </a>
            </div>
            <div class="small">
            	<img src="resources/img/kepcoLogo.png" alt="KEPCO" /> <span>copyright 2017 kepco bigdata portal. all rights reserved.</span>
            </div>
        </div>

    </div><!-- end  page-wrapper -->
</div> <!-- end wrapper -->
<script>
$(document).ready(function(e) { 
	
	// Data Category slideToggle 
	$("#btn_slideUp, #btn_DatamapCategory").on("click", function() { 
		toggleCategoryBtn();
	})
	
var chartLocation = ['inchoenChart','seoulChart'];
	
// 	for(var i = 0; i<chartLocation.length; i++){
// 		// Themes begin
// 		 am4core.useTheme(am4themes_animated);
// 		 // Themes end

// 		 // Create chart instance
// 		 var chart = am4core.create(chartLocation[i], am4charts.XYChart);
		
// 		 // Add data
// 		 chart.data = [{
// 		     "name": "John",
// 		     "points": 35654,
// 		     "color": chart.colors.next()
// 		 }, {
// 		     "name": "Damon",
// 		     "points": 65456,
// 		     "color": chart.colors.next()
// 		 }, {
// 		     "name": "Patrick",
// 		     "points": 45724,
// 		     "color": chart.colors.next()
// 		 }];

// 		 // Create axes
// 		 var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
// 		 categoryAxis.dataFields.category = "name";
// 		 categoryAxis.renderer.grid.template.disabled = true;
// 		 categoryAxis.renderer.minGridDistance = 30;
// 		 categoryAxis.renderer.inside = true;
// 		 categoryAxis.renderer.labels.template.fill = am4core.color("#fff");
// 		 categoryAxis.renderer.labels.template.fontSize = 0;

// 		 var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
// 		 valueAxis.renderer.grid.template.strokeDasharray = "4,4";
// 		 valueAxis.renderer.labels.template.disabled = true;
// 		 valueAxis.min = 0;

// 		 // Do not crop bullets
// 		 chart.maskBullets = false;

// 		 // Remove padding
// 		 chart.paddingBottom = 0;

// 		 // Create series
// 		 var series = chart.series.push(new am4charts.ColumnSeries());
// 		 series.dataFields.valueY = "points";
// 		 series.dataFields.categoryX = "name";
// 		 series.columns.template.propertyFields.fill = "color";
// 		 series.columns.template.propertyFields.stroke = "color";
// 		 //series.columns.template.column.cornerRadiusTopLeft = 15;
// 		 //series.columns.template.column.cornerRadiusTopRight = 15;
// 		 series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/b]";

// 		 // Add bullets
// 		 var bullet = series.bullets.push(new am4charts.Bullet());
// 		 var image = bullet.createChild(am4core.Image);
// 		 image.horizontalCenter = "middle";
// 		 image.verticalCenter = "bottom";
// 		 image.dy = 20;
// 		 image.y = am4core.percent(100);
// 		 image.propertyFields.href = "bullet";
// 		 image.tooltipText = series.columns.template.tooltipText;
// 		 image.propertyFields.fill = "color";
// 		 image.filters.push(new am4core.DropShadowFilter());
// 	}

 	for(var i = 0; i<chartLocation.length; i++){
 		Highcharts.chart(chartLocation[i], {

 			  chart: {
 			    type: 'variwide',
 			   backgroundColor: 'transparent',
 			   colors : [
 				   "black","black","black"
 			   ],
 			  	padding : 0,
 			   spacing: [0,0,0,0]
 			  },

 			  title: {
 			    text: ''
 			  },
 			  credits:{
 				    enabled: false
 			  },
 			  exporting: { 
 				  enabled: false 
 			  },
 			  plotOptions:{
					series:{
		 			  	colors : [ '#2f7ed8', '#0d233a', '#FF0000']
					}
			  },
 			  symbol : { 
 			    enabled: false
 			  },
 			  accessibility: {
 			    announceNewData: {
 			      enabled: false
 			    }
 			  },
 			  subtitle: {
 			    text: ''
 			  },
 			  tooltip: {
 			      enabled: false
 			    },
 			  xAxis: {
 			    type: 'category',
 			    crosshair: false,
 			   minPadding: 0,
 			  maxPadding: 0,
 			  tickWidth: 0,
 			  lineWidth: 0,
 			    visible:false,
 			   maxPadding:0,
 			     labels:{
 			      enabled: false
 			    }
 			  },
 			  
 			  yAxis: {
 				crosshair: false,
 			    padding : 0,
 			    margin : 0,
 			   visible:false,
 			    pointPadding: 0,
 			    borderWidth: 0,
 			   maxPadding:0,
 			    title: {
 			      text: ''
 			    },
 			    labels:{
 			      enabled: false
 			    },
 			    gridLineColor: '#ffffff'
 			  },

 			  caption: {
 			    text: ''
 			  },

 			  legend: {
 			    enabled: false
 			  },

 			  series: [{
 			    name: 'Labor Costs',
 			    data: [
 			      ['Norway', 50.2, 1],
 			      ['Denmark', 42, 1],
 			      ['Belgium', 39.2, 1]
 			    ],
 			    dataLabels: {
 			      enabled: false
 			    },
 			    tooltip: {
 			      enabled: false
 			    },
 			    colorByPoint: true
 			  }]

 			});
 	}

	
	
});

//Data Category slideToggle
function toggleCategoryBtn() { 
	if($("#category_line").css("display") == "none") {
		$("#btn_DatamapCategory").attr("src", "resources/img/btn_DataMapCategoryUp.png");	
		$("#logo_lineType").fadeOut("fast");
		$("#clogo").fadeIn();
	}else {
		$("#btn_DatamapCategory").attr("src", "resources/img/btn_DataMapCategoryDown.png");
		$("#clogo").fadeOut("fast");
		$("#logo_lineType").fadeIn();
	}
	$("#category_line").slideToggle();
}

// submenu behavior
function submenu(sid) { 
	$("#submenu_wrap ul").css("display", "none");
	$("#submenu_wrap ul").stop().clearQueue();
	$("#submenu_"+sid).fadeIn();
}

// Data Category In/Out Toggle
function iconDataCategoryToggle(dataType) {
	$("#iconDataCategory").attr("src", "resources/img/icon_dataCategory"+dataType+".jpg");
	if(dataType == "In") {
		$("#categoryListIn").css("display", "block");
		$("#categoryListOut").css("display", "none");
	}else {
		$("#categoryListIn").css("display", "none");
		$("#categoryListOut").css("display", "block");
	}
	
}

$(function() {
$('.korea-han').maphilight(
		{
			
			fillColor:'038eff',
			strokeColor:'038eff',
			groupBy:'alt'
		}
		);
})
</script>
</body>

</html>