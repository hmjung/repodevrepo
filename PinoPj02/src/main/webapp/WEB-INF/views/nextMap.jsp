<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>오버레이</title>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<!-- 네이버지도 JS -->
	<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?ncpClientId=0spszca8w6"></script>	
	
</head>

<body>
	<div class="chartbox" style="margin-top:100px;">
		<div style="width:30%;height:50px;margin-left: 100px;">
			<button onclick="location.href='./'" type="button" class="btn btn-primary" style="margin-left: 10px;">홈으로</button>
		</div>
		<p style="width:30%; margin-left: 100px;">지도</p>
		<div class="map" id ="map" style="width:40%;height:600px;margin-left: 100px; background-image: url('/resources/img/aa.jpg'); background-repeat: no-repeat;" >
			<div class="chartArea" id="chartArea"></div>
		</div>
	</div>

<script src="resources/js/geoJson/nextMap.js"></script>
<script>

$(function () {
	setPolygon(geodata, '전국지도');
});

function setPolygon(data, name){
	var centerLatLng = new naver.maps.LatLng(37.57669160058646, 126.98753356933594);
	var setLevel = 9;
	
	//센터 좌표 및 레벨지정
	for(var i = 0; i < markerLatLng.length; i++){
		if(markerLatLng[i].name == name){
			centerLatLng = markerLatLng[i].zoomLatLng;
			setLevel = markerLatLng[i].zoomLevel;
		}
	}
	
	//맵옵션설정(좌표, 줌레벨, div설정)
	var container = document.getElementById('map'),
    options = {
       center: centerLatLng,
       //level: setLevel 카카오
       zoom : setLevel,
       scrollWheel : false,
       pinchZoom : false
    };

	//맵생성
	//var map = new kakao.maps.Map(container, options);
	map = new naver.maps.Map(container, options);
	//map.setZoomable(false);    
	var PolygonData = data.features;
	//폴리곤 좌표 입력
	for(var i = 0; i < PolygonData.length ; i++){
		for(var j = 0; j < PolygonData[i].geometry.coordinates.length ; j++){
			
			var $pathsData = new Array();
			var $fillColor
			var $polygonOption
			var $zIndex
			//폴리곤 좌표 넣기
			for(var k = 0; k < PolygonData[i].geometry.coordinates[j].length ; k++){
				$pathsData.push(new naver.maps.LatLng(PolygonData[i].geometry.coordinates[j][k][1], PolygonData[i].geometry.coordinates[j][k][0]));
			}
			
			//전국 17개 시/도 색가져오기
			for(var k = 0; k<markerLatLng.length; k++){
 				if(name == '전국지도' && PolygonData[i].properties.CTP_KOR_NM == markerLatLng[k].name) $fillColor = markerLatLng[k].color;
				if(name != '전국지도' && name == markerLatLng[k].name) $fillColor = markerLatLng[k].color;
			}
			
			//z-index
			for(var k = 0; k<markerLatLng.length; k++){
 				if(name == '전국지도' && PolygonData[i].properties.CTP_KOR_NM == '전라남도') $zIndex = 0;
 				if(name == '전국지도' && PolygonData[i].properties.CTP_KOR_NM == '광주광역시') {
 					$zIndex = 5;
 					console.log('광주광역시 폴리곤그리기')
 				}
			}
			
			if(name=='전국지도'){
				$polygonOption = {
					map : map,
					name : PolygonData[i].properties.CTP_KOR_NM,
				    paths: $pathsData, // 그려질 다각형의 좌표 배열
				    strokeWeight: 0.5, // 선의 두께
				    strokeColor: 'gray', // 선의 색깔
				    strokeOpacity: 0.8, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명
				    fillColor: $fillColor, // 채우기 색깔
				    fillOpacity: 1, // 채우기 불투명도 
				    clickable : true,
				    zIndex : $zIndex
				};				
			}else{
				$polygonOption = {
					map : map,
					paths: $pathsData, // 그려질 다각형의 좌표 배열
				    strokeWeight: 0.5, // 선의 두께
				    strokeColor: 'gray', // 선의 색깔
				    strokeOpacity: 1, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명
				    fillColor: '#f5ffff', // 채우기 색깔
				    fillOpacity: 1, // 채우기 불투명
				    clickable : true
				};
			}
			
			// 지도에 표시할 선을 생성합니다
			var polygon = new naver.maps.Polygon($polygonOption);
			var polygonType = 'ctpv';
			var polygonName = PolygonData[i].properties.CTP_KOR_NM;
			if($.trim(PolygonData[i].properties.SGG_NM) != '' && PolygonData[i].properties.SGG_NM != null && PolygonData[i].properties.SGG_NM != undefined){
				polygonName = PolygonData[i].properties.SGG_NM;
				polygonType = 'sgg'
			}
			//수정필요
			if(polygonName == undefined){
				polygonName = PolygonData[i].properties.name;
				polygonType = 'sgg'
			}

			//폴리곤 이벤트 핸들러
			fn_polygonEvent(polygon, polygonName, polygonType, $polygonOption);
		}
	}
}//폴리곤생성 끝

//폴리곤 클릭 이벤트 핸들러
function fn_polygonEvent(polygon, name, type, option){
	naver.maps.Event.addListener(polygon, 'click', function(e) {
		if(typeof(name) != 'undefined'){
			fn_zoomEvent(name, type);
		}
	}); 
	
    naver.maps.Event.addListener(polygon, "mouseover", function(e) {
//      map.setCursor("pointer");

    	var zIndex = 0;
    	
		if(polygon.name == '광주광역시') zIndex = 5;
		
        polygon.setOptions({
            fillOpacity: 0.8,
            fillColor : 'gray',
            zIndex:zIndex
        });			
        
        console.log('mouseover', polygon.name)
    });
    
    naver.maps.Event.addListener(polygon, "mouseout", function(e) {
//         map.setCursor("auto");

        polygon.setOptions(option);
    });
    
}

//폴리곤 클릭시 확대이벤트
function fn_zoomEvent(name, type){
	console.log('name, type',name, type)
}

</script>
</body>
</html>