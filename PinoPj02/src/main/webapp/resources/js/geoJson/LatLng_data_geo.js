//전국시 좌표
var markerLatLng = [];

//지역좌표
var geoLatLng = [	
	//오버레이 좌표는 0.48정도 더하면 적당
	//latLngY, latLngX : 지역 센터 좌표 
	//zoomLatLngX, zoomLatLngY : 줌이벤트 이후 좌표 
	//zoomLevel : 줌 실행시 줌 레벨 , 
	//latLngY : 37.47485808497102, latLngX : 126.61743164062499 인천      네이버 높을수록 확대   34.791123, latLngX : 126.924972
	{latLngY : 37.6066805, latLngX : 126.9984147, name:'서울특별시', ename:'Seoul', zoomLatLngY : 37.56390197839331, zoomLatLngX : 126.98358535766602, zoomLevel : 11, color:'#f5ffff'}, //서울특별시
	{latLngY : 37.592471511019085, latLngX : 128.355244140625, name:'강원도', ename:'Gangwon-do', zoomLatLngY : 37.67505900514996, zoomLatLngX : 128.3477783203125, zoomLevel : 8, color:'#f5ffff'}, //강원도
	{latLngY : 37.47485808497102, latLngX : 126.51743164062499, name:'인천광역시', ename:'Incheon', zoomLatLngY : 37.482320, zoomLatLngX : 126.642615, zoomLevel : 11, color:'#f5ffff'}, //인천광역시 
	{latLngY : 35.14237113713991, latLngX : 126.00814453125, name:'광주광역시', ename:'Gwangju', zoomLatLngY : 35.135188, zoomLatLngX : 126.857191, zoomLevel : 11, color:'#f5ffff'}, //광주광역시 
	{latLngY : 33.39705230475205, latLngX : 126.5460205078125, name:'제주특별자치도', ename:'Jeju-do', zoomLatLngY : 33.390172864722466, zoomLatLngX : 126.55700683593749, zoomLevel : 9, color:'#f5ffff'}, //제주특별자치도 
	{latLngY : 34.84762101276787, latLngX : 126.78497314453125, name:'전라남도', ename:'Jeollanam-do', zoomLatLngY : 34.811123, zoomLatLngX : 126.924972, zoomLevel : 9, color:'#f5ffff'}, //전라남도 
	{latLngY : 35.200543276002666, latLngX : 129.16463623046875, name:'부산광역시', ename:'Busan', zoomLatLngY : 35.17128256489428, zoomLatLngX : 129.05845642089844, zoomLevel : 11, color:'#f5ffff'}, //부산광역시 
	{latLngY : 35.56787066533268, latLngX : 129.354150390625, name:'울산광역시', ename:'Ulsan', zoomLatLngY : 35.54619462154643, zoomLatLngX : 129.25483703613278, zoomLevel : 10, color:'#f5ffff'}, //울산광역시 
	{latLngY : 35.860117799832544, latLngX : 128.55377197265622, name:'대구광역시', ename:'Daegu', zoomLatLngY : 35.832368, zoomLatLngX : 128.589046, zoomLevel : 11, color:'#f5ffff'}, //대구광역시 
	{latLngY : 35.306428057364255, latLngX : 128.08135986328125, name:'경상남도', ename:'Gyeongsangnam-do', zoomLatLngY : 35.29081992014202, zoomLatLngX : 128.32031249999997, zoomLevel : 9, color:'#f5ffff'}, //경상남도
	{latLngY : 36.401845303684324, latLngX : 128.5794921875, name:'경상북도', ename:'Gyeongsangbuk-do', zoomLatLngY : 36.379279167407965, zoomLatLngX : 128.76800537109375, zoomLevel : 7, color:'#f5ffff'}, //경상북도
	{latLngY : 37.32430451813815, latLngX : 127.30359130859374, name:'경기도', ename:'Gyeonggi-do', zoomLatLngY : 37.277946, zoomLatLngX : 127.016198, zoomLevel : 10, color:'#f5ffff'}, //경기도
	{latLngY : 36.31052700542763, latLngX : 127.31098144531249, name:'대전광역시', ename:'Daejeon', zoomLatLngY : 36.3472087646811, zoomLatLngX : 127.38304138183592, zoomLevel : 11, color:'#f5ffff'}, //대전광역시 
	{latLngY : 36.58301312197295, latLngX : 127.2130908203125, name:'세종특별자치시', ename:'Sejong-si', zoomLatLngY : 36.56984507478879, zoomLatLngX : 127.29583740234375, zoomLevel : 11, color:'#f5ffff'}, //세종특별자치시
	{latLngY : 36.461054075054314, latLngX : 126.749267578125, name:'충청남도', ename:'Chungcheongnam-do', zoomLatLngY : 36.465471886798134, zoomLatLngX : 126.90350097656249, zoomLevel : 9, color:'#f5ffff'}, //충청남도 
	{latLngY : 36.8440126988417, latLngX : 127.7215576171875, name:'충청북도', ename:'Chungcheongbuk-do', zoomLatLngY : 36.671273880045004, zoomLatLngX : 127.95070800781249, zoomLevel : 9, color:'#f5ffff'}, //충청북도 
	{latLngY : 35.77068501330236, latLngX : 127.1161865234375, name:'전라북도', ename:'Jeollabuk-do', zoomLatLngY : 35.71975793933433, zoomLatLngX : 127.10040527343749, zoomLevel : 9, color:'#f5ffff'}, //전라북도 
	{latLngY : 33.62593850874471, latLngX : 129.027685546875, name:'차트영역', ename:'summary', zoomLatLngY : 36.10237644873644, zoomLatLngX : 128.21044921874997, zoomLevel : 7, color:'#f5ffff'}, //차트영역 
	{latLngY : 36.0546781, latLngX : 128.6806144, name:'전국지도', ename:'korea', zoomLatLngY : 35.50237644873644, zoomLatLngX : 128.21044921874997, zoomLevel : 7, color:'#f5ffff'} //전국지도 
];

var markerSeoul = [];
var latLngSeoul = [
    //서울특별시
    {latLngY : 37.580009, latLngX : 126.983626, name:'종로구', color:'#f5ffff', root:'서울특별시', ename:'Jongno-gu'}
    ,{latLngY : 37.554079, latLngX : 126.995046, name:'중구', color:'#f5ffff', root:'서울특별시', ename:'Jung-gu'}
    ,{latLngY : 37.525502, latLngX : 126.979899, name:'용산구', color:'#f5ffff', root:'서울특별시', ename:'Yongsan-gu'}
    ,{latLngY : 37.550386, latLngX : 127.045047, name:'성동구', color:'#f5ffff', root:'서울특별시', ename:'Seongdong-gu'}
    ,{latLngY : 37.545578, latLngX : 127.088722, name:'광진구', color:'#f5ffff', root:'서울특별시', ename:'Gwangjin-gu'}
    ,{latLngY : 37.574428, latLngX : 127.059712, name:'동대문구', color:'#f5ffff', root:'서울특별시', ename:'Dongdaemun-gu'}
    ,{latLngY : 37.586736, latLngX : 127.092924, name:'중랑구', color:'#f5ffff', root:'서울특별시', ename:'Jungnang-gu'}
    ,{latLngY : 37.599440, latLngX : 127.016817, name:'성북구', color:'#f5ffff', root:'서울특별시', ename:'Seongbuk-gu'}
    ,{latLngY : 37.630702, latLngX : 127.017511, name:'강북구', color:'#f5ffff', root:'서울특별시', ename:'Gangbuk-gu'}
    ,{latLngY : 37.668639, latLngX : 127.037364, name:'도봉구', color:'#f5ffff', root:'서울특별시', ename:'Dobong-gu'}
    ,{latLngY : 37.640485, latLngX : 127.076685, name:'노원구', color:'#f5ffff', root:'서울특별시', ename:'Nowon-gu'}
    ,{latLngY : 37.602766, latLngX : 126.928839, name:'은평구', color:'#f5ffff', root:'서울특별시', ename:'Eunpyeong-gu'}
    ,{latLngY : 37.569213, latLngX : 126.936773, name:'서대문구', color:'#f5ffff', root:'서울특별시', ename:'Seodaemun-gu'}
    ,{latLngY : 37.547780, latLngX : 126.922218, name:'마포구', color:'#f5ffff', root:'서울특별시', ename:'Mapo-gu'}
    ,{latLngY : 37.513674, latLngX : 126.849050, name:'양천구', color:'#f5ffff', root:'서울특별시', ename:'Yangcheon-gu'}
    ,{latLngY : 37.550968, latLngX : 126.849614, name:'강서구', color:'#f5ffff', root:'서울특별시', ename:'Gangseo-gu'}
    ,{latLngY : 37.490650, latLngX : 126.860771, name:'구로구', color:'#f5ffff', root:'서울특별시', ename:'Guro-gu'}
    ,{latLngY : 37.456979, latLngX : 126.900656, name:'금천구', color:'#f5ffff', root:'서울특별시', ename:'Gumcheon-gu'}
    ,{latLngY : 37.516410, latLngX : 126.906252, name:'영등포구', color:'#f5ffff', root:'서울특별시', ename:'Yeongdeungpo-gu'}
    ,{latLngY : 37.500492, latLngX : 126.952317, name:'동작구', color:'#f5ffff', root:'서울특별시', ename:'Dongjak-gu'}
    ,{latLngY : 37.468376, latLngX : 126.942503, name:'관악구', color:'#f5ffff', root:'서울특별시', ename:'Gwanak-gu'}
    ,{latLngY : 37.490102, latLngX : 127.009083, name:'서초구', color:'#f5ffff', root:'서울특별시', ename:'Seocho-gu'}
    ,{latLngY : 37.493236, latLngX : 127.056687, name:'강남구', color:'#f5ffff', root:'서울특별시', ename:'Gangnam-gu'}
    ,{latLngY : 37.500596, latLngX : 127.114379, name:'송파구', color:'#f5ffff', root:'서울특별시', ename:'Songpa-gu'}
    ,{latLngY : 37.540120, latLngX : 127.139858, name:'강동구', color:'#f5ffff', root:'서울특별시', ename:'Gangdong-gu'}

    //부산광역시
    ,{latLngY : 35.102131, latLngX : 129.036793, name:'중구', color:'#f5ffff', root:'부산광역시', ename:'Jung-gu'}
    ,{latLngY : 35.120689, latLngX : 129.010906, name:'서구', color:'#f5ffff', root:'부산광역시', ename:'Seo-gu'}
    ,{latLngY : 35.129298, latLngX : 129.045302, name:'동구', color:'#f5ffff', root:'부산광역시', ename:'Dong-gu'}
    ,{latLngY : 35.079974, latLngX : 129.057595, name:'영도구', color:'#f5ffff', root:'부산광역시', ename:'Yeongdo-gu'}
    ,{latLngY : 35.162943, latLngX : 129.053097, name:'부산진구', color:'#f5ffff', root:'부산광역시', ename:'Busanjin-gu'}
    ,{latLngY : 35.203021, latLngX : 129.081035, name:'동래구', color:'#f5ffff', root:'부산광역시', ename:'Dongnae-gu'}
    ,{latLngY : 35.126318, latLngX : 129.094477, name:'남구', color:'#f5ffff', root:'부산광역시', ename:'Nam-gu'}
    ,{latLngY : 35.220266, latLngX : 129.030201, name:'북구', color:'#f5ffff', root:'부산광역시', ename:'Buk-gu'}
    ,{latLngY : 35.175096, latLngX : 129.153500, name:'해운대구', color:'#f5ffff', root:'부산광역시', ename:'Haeundae-gu'}
    ,{latLngY : 35.092503, latLngX : 128.974786, name:'사하구', color:'#f5ffff', root:'부산광역시', ename:'Saha-gu'}
    ,{latLngY : 35.243036, latLngX : 129.092125, name:'금정구', color:'#f5ffff', root:'부산광역시', ename:'Geumjeong-gu'}
    ,{latLngY : 35.151067, latLngX : 128.916780, name:'강서구', color:'#f5ffff', root:'부산광역시', ename:'Gangseo-gu'}
    ,{latLngY : 35.180273, latLngX : 129.079814, name:'연제구', color:'#f5ffff', root:'부산광역시', ename:'Yeonje-gu'}
    ,{latLngY : 35.155568, latLngX : 129.113132, name:'수영구', color:'#f5ffff', root:'부산광역시', ename:'Suyeong-gu'}
    ,{latLngY : 35.160084, latLngX : 128.990268, name:'사상구', color:'#f5ffff', root:'부산광역시', ename:'Sasang-gu'}
    ,{latLngY : 35.275965, latLngX : 129.196930, name:'기장군', color:'#f5ffff', root:'부산광역시', ename:'Gijang-gun'}

    //대구광역시
    ,{latLngY : 35.862562, latLngX : 128.595649, name:'중구', color:'#f5ffff', root:'대구광역시', ename:'Jung-gu'}
    ,{latLngY : 35.916879, latLngX : 128.679640, name:'동구', color:'#f5ffff', root:'대구광역시', ename:'Dong-gu'}
    ,{latLngY : 35.869814, latLngX : 128.544289, name:'서구', color:'#f5ffff', root:'대구광역시', ename:'Seo-gu'}
    ,{latLngY : 35.832368, latLngX : 128.589046, name:'남구', color:'#f5ffff', root:'대구광역시', ename:'Nam-gu'}
    ,{latLngY : 35.922406, latLngX : 128.584370, name:'북구', color:'#f5ffff', root:'대구광역시', ename:'Buk-gu'}
    ,{latLngY : 35.835533, latLngX : 128.655115, name:'수성구', color:'#f5ffff', root:'대구광역시', ename:'Suseong-gu'}
    ,{latLngY : 35.824571, latLngX : 128.526307, name:'달서구', color:'#f5ffff', root:'대구광역시', ename:'Dalseo-gu'}
    ,{latLngY : 35.746488, latLngX : 128.500007, name:'달성군', color:'#f5ffff', root:'대구광역시', ename:'Dalseong-gun'}
    
    //인천광역시
    ,{latLngY : 37.483869, latLngX : 126.500965, name:'중구', color:'#f5ffff', root:'인천광역시', ename:'Jung-gu'}
    ,{latLngY : 37.482320, latLngX : 126.642615, name:'동구', color:'#f5ffff', root:'인천광역시', ename:'Dong-gu'}
    ,{latLngY : 37.457071, latLngX : 126.669362, name:'미추홀구', color:'#f5ffff', root:'인천광역시', ename:'Michuhol-gu'}
    ,{latLngY : 37.399604, latLngX : 126.650823, name:'연수구', color:'#f5ffff', root:'인천광역시', ename:'Yeonsu-gu'}
    ,{latLngY : 37.432178, latLngX : 126.730168, name:'남동구', color:'#f5ffff', root:'인천광역시', ename:'Namdong-gu'}
    ,{latLngY : 37.494168, latLngX : 126.720032, name:'부평구', color:'#f5ffff', root:'인천광역시', ename:'Bupyeong-gu'}
    ,{latLngY : 37.543621, latLngX : 126.737225, name:'계양구', color:'#f5ffff', root:'인천광역시', ename:'Gyeyang-gu'}
    ,{latLngY : 37.544367, latLngX : 126.653590, name:'서구', color:'#f5ffff', root:'인천광역시', ename:'Seo-gu'}
    ,{latLngY : 37.723407, latLngX : 126.423653, name:'강화군', color:'#f5ffff', root:'인천광역시', ename:'Ganghwa-gu'}
    ,{latLngY : 37.953666, latLngX : 124.694859, name:'옹진군', color:'#f5ffff', root:'인천광역시', ename:'Ongjin-gu'}

    //광주광역시
    ,{latLngY : 35.120455, latLngX : 126.952831, name:'동구', color:'#f5ffff', root:'광주광역시', ename:'Dong-gu'}
    ,{latLngY : 35.135188, latLngX : 126.857191, name:'서구', color:'#f5ffff', root:'광주광역시', ename:'Seo-gu'}
    ,{latLngY : 35.081335, latLngX : 126.834016, name:'남구', color:'#f5ffff', root:'광주광역시', ename:'Nam-gu'}
    ,{latLngY : 35.186824, latLngX : 126.921621, name:'북구', color:'#f5ffff', root:'광주광역시', ename:'Buk-gu'}
    ,{latLngY : 35.152510, latLngX : 126.756121, name:'광산구', color:'#f5ffff', root:'광주광역시', ename:'Gwangsan-gu'}

    //대전광역시
    ,{latLngY : 36.317351, latLngX : 127.467665, name:'동구', color:'#f5ffff', root:'대전광역시', ename:'Dong-gu'}
    ,{latLngY : 36.306364, latLngX : 127.410085, name:'중구', color:'#f5ffff', root:'대전광역시', ename:'Jung-gu'}
    ,{latLngY : 36.265889, latLngX : 127.341638, name:'서구', color:'#f5ffff', root:'대전광역시', ename:'Seo-gu'}
    ,{latLngY : 36.375372, latLngX : 127.333373, name:'유성구', color:'#f5ffff', root:'대전광역시', ename:'Yuseong-gu'}
    ,{latLngY : 36.389276, latLngX : 127.431470, name:'대덕구', color:'#f5ffff', root:'대전광역시', ename:'Daedeok-gu'}		

    //울산광역시
    ,{latLngY : 35.571425, latLngX : 129.295574, name:'중구', color:'#f5ffff', root:'울산광역시', ename:'Jung-gu'}
    ,{latLngY : 35.529590, latLngX : 129.300178, name:'남구', color:'#f5ffff', root:'울산광역시', ename:'Nam-gu'}
    ,{latLngY : 35.554630, latLngX : 129.421348, name:'동구', color:'#f5ffff', root:'울산광역시', ename:'Dong-gu'}
    ,{latLngY : 35.608204, latLngX : 129.372491, name:'북구', color:'#f5ffff', root:'울산광역시', ename:'Buk-gu'}
    ,{latLngY : 35.584771, latLngX : 129.159892, name:'울주군', color:'#f5ffff', root:'울산광역시', ename:'Ulju-gun'}

    //세종특별자치시
    ,{latLngY : 36.537421, latLngX : 127.274099, name:'세종특별자치시', color:'#f5ffff', root:'세종특별자치시', ename:'Sejong-si'}

    //경기도
    ,{latLngY : 37.277946, latLngX : 126.999198, name:'수원시', color:'#f5ffff', root:'경기도', ename:'Suwon-si'}
//    ,{latLngY : 37.311841, latLngX : 126.997139, name:'장안구', color:'#f5ffff', root:'경기도', ename:'Jangan-gu'}
//    ,{latLngY : 37.262111, latLngX : 126.978044, name:'권선구', color:'#f5ffff', root:'경기도', ename:'Gwonseon-gu'}
//    ,{latLngY : 37.277946, latLngX : 127.016198, name:'팔달구', color:'#f5ffff', root:'경기도', ename:'Paldal-gu'}
//    ,{latLngY : 37.270423, latLngX : 127.060340, name:'영통구', color:'#f5ffff', root:'경기도', ename:'Yeongtong-gu'}
    ,{latLngY : 37.401366, latLngX : 127.102705, name:'성남시', color:'#f5ffff', root:'경기도', ename:'Seongnam-si'}
//    ,{latLngY : 37.423114, latLngX : 127.091136, name:'수정구', color:'#f5ffff', root:'경기도', ename:'Sujeong-gu'}
//    ,{latLngY : 37.431366, latLngX : 127.162705, name:'중원구', color:'#f5ffff', root:'경기도', ename:'Jungwon-gu'}
//    ,{latLngY : 37.368891, latLngX : 127.104636, name:'분당구', color:'#f5ffff', root:'경기도', ename:'Bundang-gu'}
    ,{latLngY : 37.746353, latLngX : 127.057571, name:'의정부시', color:'#f5ffff', root:'경기도', ename:'Uijeongbu-si'}
    ,{latLngY : 37.410865, latLngX : 126.923349, name:'안양시', color:'#f5ffff', root:'경기도', ename:'Anyang-si'}
//    ,{latLngY : 37.399221, latLngX : 126.905926, name:'만안구', color:'#f5ffff', root:'경기도', ename:'Manan-gu'}
//    ,{latLngY : 37.399865, latLngX : 126.953349, name:'동안구', color:'#f5ffff', root:'경기도', ename:'Dongan-gu'}
    ,{latLngY : 37.519198, latLngX : 126.780918, name:'부천시', color:'#f5ffff', root:'경기도', ename:'Bucheon-si'}
    ,{latLngY : 37.448626, latLngX : 126.852681, name:'광명시', color:'#f5ffff', root:'경기도', ename:'Gwangmyeong-si'}
    ,{latLngY : 37.001351, latLngX : 126.983877, name:'평택시', color:'#f5ffff', root:'경기도', ename:'Pyeongtaek-si'}
    ,{latLngY : 37.907354, latLngX : 127.065582, name:'동두천시', color:'#f5ffff', root:'경기도', ename:'Dongducheon-si'}
	,{latLngY : 37.329783, latLngX : 126.826831, name:'안산시', color:'#f5ffff', root:'경기도', ename:'Ansan-si'}
//    ,{latLngY : 37.302100, latLngX : 126.870050, name:'상록구', color:'#f5ffff', root:'경기도', ename:'Sangnok-gu'}
//    ,{latLngY : 37.329783, latLngX : 126.796831, name:'단원구', color:'#f5ffff', root:'경기도', ename:'Danwon-gu'}
	,{latLngY : 37.675123, latLngX : 126.810198, name:'고양시', color:'#f5ffff', root:'경기도', ename:'Goyang-si'}
//    ,{latLngY : 37.641123, latLngX : 126.857198, name:'덕양구', color:'#f5ffff', root:'경기도', ename:'Deogyang-gu'}
//    ,{latLngY : 37.678915, latLngX : 126.806103, name:'일산동구', color:'#f5ffff', root:'경기도', ename:'Ilsandong-gu'}
//    ,{latLngY : 37.680141, latLngX : 126.733547, name:'일산서구', color:'#f5ffff', root:'경기도', ename:'Ilsanseo-gu'}
    ,{latLngY : 37.433968, latLngX : 126.986933, name:'과천시', color:'#f5ffff', root:'경기도', ename:'Gwacheon-si'}
    ,{latLngY : 37.611643, latLngX : 127.108113, name:'구리시', color:'#f5ffff', root:'경기도', ename:'Guri-si'}
    ,{latLngY : 37.644026, latLngX : 127.217094, name:'남양주시', color:'#f5ffff', root:'경기도', ename:'Namyangju-si'}
    ,{latLngY : 37.162233, latLngX : 127.040567, name:'오산시', color:'#f5ffff', root:'경기도', ename:'Osan-si'}
    ,{latLngY : 37.395404, latLngX : 126.798226, name:'시흥시', color:'#f5ffff', root:'경기도', ename:'Siheung-si'}
    ,{latLngY : 37.348633, latLngX : 126.910584, name:'군포시', color:'#f5ffff', root:'경기도', ename:'Gunpo-si'}
    ,{latLngY : 37.365427, latLngX : 126.980422, name:'의왕시', color:'#f5ffff', root:'경기도', ename:'Uiwang-si'}
    ,{latLngY : 37.528135, latLngX : 127.194392, name:'하남시', color:'#f5ffff', root:'경기도', ename:'Hanam -si'}
	,{latLngY : 37.295085, latLngX : 127.101379, name:'용인시', color:'#f5ffff', root:'경기도', ename:'Yongin-si'}
//    ,{latLngY : 37.215497, latLngX : 127.247546, name:'처인구', color:'#f5ffff', root:'경기도', ename:'Cheoin-gu'}
//    ,{latLngY : 37.283085, latLngX : 127.131379, name:'기흥구', color:'#f5ffff', root:'경기도', ename:'Giheung-gu'}
//    ,{latLngY : 37.327982, latLngX : 127.074943, name:'수지구', color:'#f5ffff', root:'경기도', ename:'Suji-gu'}
    ,{latLngY : 37.874165, latLngX : 126.780387, name:'파주시', color:'#f5ffff', root:'경기도', ename:'Paju-si'}
    ,{latLngY : 37.234708, latLngX : 127.456404, name:'이천시', color:'#f5ffff', root:'경기도', ename:'Icheon-si'}
    ,{latLngY : 37.024714, latLngX : 127.293594, name:'안성시', color:'#f5ffff', root:'경기도', ename:'Anseong-si'}
    ,{latLngY : 37.695995, latLngX : 126.599599, name:'김포시', color:'#f5ffff', root:'경기도', ename:'Gimpo-si'}
    ,{latLngY : 37.173917, latLngX : 126.909222, name:'화성시', color:'#f5ffff', root:'경기도', ename:'Hwaseong-si'}
    ,{latLngY : 37.391719, latLngX : 127.282111, name:'광주시', color:'#f5ffff', root:'경기도', ename:'Gwangju-si'}
    ,{latLngY : 37.808907, latLngX : 126.999622, name:'양주시', color:'#f5ffff', root:'경기도', ename:'Yangju-si'}
    ,{latLngY : 37.934298, latLngX : 127.209043, name:'포천시', color:'#f5ffff', root:'경기도', ename:'Pocheon-si'}
    ,{latLngY : 37.279184, latLngX : 127.620168, name:'여주시', color:'#f5ffff', root:'경기도', ename:'Yeoju-si'}
    ,{latLngY : 38.100272, latLngX : 127.054677, name:'연천군', color:'#f5ffff', root:'경기도', ename:'Yeoncheon-gun'}
    ,{latLngY : 37.828819, latLngX : 127.417518, name:'가평군', color:'#f5ffff', root:'경기도', ename:'Gapyeong-gun'}
    ,{latLngY : 37.493552, latLngX : 127.544053, name:'양평군', color:'#f5ffff', root:'경기도', ename:'Yangpyeong-gun'}



    //강원도		
    ,{latLngY : 37.900090, latLngX : 127.643618, name:'춘천시', color:'#f5ffff', root:'강원도', ename:'Chuncheon-si'}
    ,{latLngY : 37.351418, latLngX : 127.856404, name:'원주시', color:'#f5ffff', root:'강원도', ename:'Wonju-si'}
    ,{latLngY : 37.786673, latLngX : 128.782214, name:'강릉시', color:'#f5ffff', root:'강원도', ename:'Gangneung-si'}
    ,{latLngY : 37.560573, latLngX : 129.038713, name:'동해시', color:'#f5ffff', root:'강원도', ename:'Donghae-si'}
    ,{latLngY : 37.211271, latLngX : 128.906912, name:'태백시', color:'#f5ffff', root:'강원도', ename:'Taebaek-si'}
    ,{latLngY : 38.220770, latLngX : 128.522809, name:'속초시', color:'#f5ffff', root:'강원도', ename:'Sokcho-si'}
    ,{latLngY : 37.330650, latLngX : 129.072011, name:'삼척시', color:'#f5ffff', root:'강원도', ename:'Samcheok-si'}
    ,{latLngY : 37.794273, latLngX : 128.004011, name:'홍천군', color:'#f5ffff', root:'강원도', ename:'Hongcheon-gun'}
    ,{latLngY : 37.600640, latLngX : 128.045858, name:'횡성군', color:'#f5ffff', root:'강원도', ename:'Hoengseong-gun'}
    ,{latLngY : 37.201996, latLngX : 128.510833, name:'영월군', color:'#f5ffff', root:'강원도', ename:'Yeongwol-gun'}
    ,{latLngY : 37.601670, latLngX : 128.381387, name:'평창군', color:'#f5ffff', root:'강원도', ename:'Pyongchang-gun'}
    ,{latLngY : 37.380410, latLngX : 128.622824, name:'정선군', color:'#f5ffff', root:'강원도', ename:'Jeongseon-gun'}
    ,{latLngY : 38.283118, latLngX : 127.323815, name:'철원군', color:'#f5ffff', root:'강원도', ename:'Cheorwon-gun'}
    ,{latLngY : 38.215486, latLngX : 127.652528, name:'화천군', color:'#f5ffff', root:'강원도', ename:'Hwacheon-gun'}
    ,{latLngY : 38.204212, latLngX : 127.912853, name:'양구군', color:'#f5ffff', root:'강원도', ename:'Yanggu-gun'}
    ,{latLngY : 38.065981, latLngX : 128.208190, name:'인제군', color:'#f5ffff', root:'강원도', ename:'Inje-gun'}
    ,{latLngY : 38.422836, latLngX : 128.321823, name:'고성군', color:'#f5ffff', root:'강원도', ename:'Goseong-gun'}
    ,{latLngY : 38.061603, latLngX : 128.540663, name:'양양군', color:'#f5ffff', root:'강원도', ename:'Yangyang-gun'}

    //충청북도
    ,{latLngY : 36.653509, latLngX : 127.484114, name:'청주시', color:'#f5ffff', root:'충청북도', ename:'Cheongju-si'}
//    ,{latLngY : 36.669991, latLngX : 127.597139, name:'상당구', color:'#f5ffff', root:'충청북도', ename:'Sangdang-gu'}
//    ,{latLngY : 36.563509, latLngX : 127.414114, name:'서원구', color:'#f5ffff', root:'충청북도', ename:'Seowon-gu'}
//    ,{latLngY : 36.670419, latLngX : 127.332136, name:'흥덕구', color:'#f5ffff', root:'충청북도', ename:'Heungdeok-gu'}
//    ,{latLngY : 36.742909, latLngX : 127.477302, name:'청원구', color:'#f5ffff', root:'충청북도', ename:'Cheongwon-gu'}
    ,{latLngY : 37.026264, latLngX : 127.863327, name:'충주시', color:'#f5ffff', root:'충청북도', ename:'Chungju-si'}
    ,{latLngY : 36.998423, latLngX : 128.135509, name:'제천시', color:'#f5ffff', root:'충청북도', ename:'Jecheon-si'}
    ,{latLngY : 36.499159, latLngX : 127.723130, name:'보은군', color:'#f5ffff', root:'충청북도', ename:'Boeun-gun'}
    ,{latLngY : 36.341508, latLngX : 127.600737, name:'옥천군', color:'#f5ffff', root:'충청북도', ename:'Okcheon-gun'}
    ,{latLngY : 36.170499, latLngX : 127.810632, name:'영동군', color:'#f5ffff', root:'충청북도', ename:'Yeongdong-gun'}
    ,{latLngY : 36.806861, latLngX : 127.563930, name:'증평군', color:'#f5ffff', root:'충청북도', ename:'Jeungpyeong -gun'}
    ,{latLngY : 36.847832, latLngX : 127.414079, name:'진천군', color:'#f5ffff', root:'충청북도', ename:'Jincheon-gun'}
    ,{latLngY : 36.777128, latLngX : 127.805132, name:'괴산군', color:'#f5ffff', root:'충청북도', ename:'Goesan-gun'}
    ,{latLngY : 36.941694, latLngX : 127.594401, name:'음성군', color:'#f5ffff', root:'충청북도', ename:'Umseong-gun'}
    ,{latLngY : 36.999927, latLngX : 128.331172, name:'단양군', color:'#f5ffff', root:'충청북도', ename:'Danyang-gun'}


    //충청남도
	,{latLngY : 36.832113, latLngX : 127.178314, name:'천안시', color:'#f5ffff', root:'충청남도', ename:'Cheonan-si'}
//    ,{latLngY : 36.808453, latLngX : 127.232553, name:'동남구', color:'#f5ffff', root:'충청남도', ename:'Dongnam-gu'}
//    ,{latLngY : 36.912113, latLngX : 127.148314, name:'서북구', color:'#f5ffff', root:'충청남도', ename:'Sebuk-gu'}
    ,{latLngY : 36.476512, latLngX : 127.085837, name:'공주시', color:'#f5ffff', root:'충청남도', ename:'Gongju-si'}
    ,{latLngY : 36.342946, latLngX : 126.599147, name:'보령시', color:'#f5ffff', root:'충청남도', ename:'Boryeong-si'}
    ,{latLngY : 36.801760, latLngX : 126.942215, name:'아산시', color:'#f5ffff', root:'충청남도', ename:'Asan-si'}
    ,{latLngY : 36.765514, latLngX : 126.430713, name:'서산시', color:'#f5ffff', root:'충청남도', ename:'Seosan-si'}
    ,{latLngY : 36.182662, latLngX : 127.140313, name:'논산시', color:'#f5ffff', root:'충청남도', ename:'Nonsan-si'}
    ,{latLngY : 36.302862, latLngX : 127.220056, name:'계룡시', color:'#f5ffff', root:'충청남도', ename:'Gyeryong-si'}
    ,{latLngY : 36.913090, latLngX : 126.664369, name:'당진시', color:'#f5ffff', root:'충청남도', ename:'Dangjin-si'}
    ,{latLngY : 36.125856, latLngX : 127.461906, name:'금산군', color:'#f5ffff', root:'충청남도', ename:'Gumsan-gun'}
    ,{latLngY : 36.256247, latLngX : 126.847781, name:'부여군', color:'#f5ffff', root:'충청남도', ename:'Buyeo-gun'}
    ,{latLngY : 36.080084, latLngX : 126.697328, name:'서천군', color:'#f5ffff', root:'충청남도', ename:'Socheon-gun'}
    ,{latLngY : 36.448819, latLngX : 126.820170, name:'청양군', color:'#f5ffff', root:'충청남도', ename:'Cheongyang-gun'}
    ,{latLngY : 36.583042, latLngX : 126.602736, name:'홍성군', color:'#f5ffff', root:'충청남도', ename:'Hongseong-gun'}
    ,{latLngY : 36.663743, latLngX : 126.795025, name:'예산군', color:'#f5ffff', root:'충청남도', ename:'Yesan-gun'}
    ,{latLngY : 36.783517, latLngX : 126.222574, name:'태안군', color:'#f5ffff', root:'충청남도', ename:'Taean-gun'}

    //전라북도
	,{latLngY : 35.847984, latLngX : 127.084031, name:'전주시', color:'#f5ffff', root:'전라북도', ename:'Jeonju-si'}
//    ,{latLngY : 35.813525, latLngX : 127.100598, name:'완산구', color:'#f5ffff', root:'전라북도', ename:'Wansan-gu'}
//    ,{latLngY : 35.897984, latLngX : 127.084031, name:'덕진구', color:'#f5ffff', root:'전라북도', ename:'Deokjin-gu'}
    ,{latLngY : 35.995719, latLngX : 126.750947, name:'군산시', color:'#f5ffff', root:'전라북도', ename:'Gunsan-si'}
    ,{latLngY : 36.021415, latLngX : 126.964369, name:'익산시', color:'#f5ffff', root:'전라북도', ename:'Iksan-si'}
    ,{latLngY : 35.631421, latLngX : 126.871697, name:'정읍시', color:'#f5ffff', root:'전라북도', ename:'Jeongeup-si'}
    ,{latLngY : 35.450535, latLngX : 127.385414, name:'남원시', color:'#f5ffff', root:'전라북도', ename:'Namwon-si'}
    ,{latLngY : 35.818685, latLngX : 126.883212, name:'김제시', color:'#f5ffff', root:'전라북도', ename:'Gimje-si'}
    ,{latLngY : 35.992986, latLngX : 127.205409, name:'완주군', color:'#f5ffff', root:'전라북도', ename:'Wanju-gun'}
    ,{latLngY : 35.876774, latLngX : 127.383895, name:'진안군', color:'#f5ffff', root:'전라북도', ename:'Jinan-gun'}
    ,{latLngY : 35.931515, latLngX : 127.695877, name:'무주군', color:'#f5ffff', root:'전라북도', ename:'Muju-gun'}
    ,{latLngY : 35.683649, latLngX : 127.521492, name:'장수군', color:'#f5ffff', root:'전라북도', ename:'Jangsu-gun'}
    ,{latLngY : 35.612447, latLngX : 127.213689, name:'임실군', color:'#f5ffff', root:'전라북도', ename:'Imsil-gun'}
    ,{latLngY : 35.410442, latLngX : 127.103410, name:'순창군', color:'#f5ffff', root:'전라북도', ename:'Sunchang-gun'}
    ,{latLngY : 35.462089, latLngX : 126.593911, name:'고창군', color:'#f5ffff', root:'전라북도', ename:'Gochang-gun'}
    ,{latLngY : 35.708655, latLngX : 126.634899, name:'부안군', color:'#f5ffff', root:'전라북도', ename:'Buan-gun'}

    //전라남도
    ,{latLngY : 34.805944, latLngX : 126.381788, name:'목포시', color:'#f5ffff', root:'전라남도', ename:'Mokpo-si'}
    ,{latLngY : 34.805775, latLngX : 127.632203, name:'여수시', color:'#f5ffff', root:'전라남도', ename:'Yeosu-si'}
    ,{latLngY : 35.000600, latLngX : 127.362103, name:'순천시', color:'#f5ffff', root:'전라남도', ename:'Suncheon-si'}
    ,{latLngY : 35.008550, latLngX : 126.700589, name:'나주시', color:'#f5ffff', root:'전라남도', ename:'Naju-si'}
    ,{latLngY : 35.038141, latLngX : 127.648487, name:'광양시', color:'#f5ffff', root:'전라남도', ename:'Gwangyang-si'}
    ,{latLngY : 35.300872, latLngX : 126.952598, name:'담양군', color:'#f5ffff', root:'전라남도', ename:'Damyang-gun'}
    ,{latLngY : 35.230040, latLngX : 127.251154, name:'곡성군', color:'#f5ffff', root:'전라남도', ename:'Gokseong-gun'}
    ,{latLngY : 35.252315, latLngX : 127.485934, name:'구례군', color:'#f5ffff', root:'전라남도', ename:'Gurye-gun'}
    ,{latLngY : 34.602279, latLngX : 127.290214, name:'고흥군', color:'#f5ffff', root:'전라남도', ename:'Goheung-gun'}
    ,{latLngY : 34.811022, latLngX : 127.120660, name:'보성군', color:'#f5ffff', root:'전라남도', ename:'Boseong-gun'}
    ,{latLngY : 35.022164, latLngX : 127.000322, name:'화순군', color:'#f5ffff', root:'전라남도', ename:'Hwasun-gun'}
    ,{latLngY : 34.729745, latLngX : 126.894972, name:'장흥군', color:'#f5ffff', root:'전라남도', ename:'Jangheung-gun'}
    ,{latLngY : 34.690743, latLngX : 126.759839, name:'강진군', color:'#f5ffff', root:'전라남도', ename:'Gangjin-gun'}
    ,{latLngY : 34.538036, latLngX : 126.566155, name:'해남군', color:'#f5ffff', root:'전라남도', ename:'Haenam-gun'}
    ,{latLngY : 34.861123, latLngX : 126.694915, name:'영암군', color:'#f5ffff', root:'전라남도', ename:'Yeongam-gun'}
    ,{latLngY : 34.922461, latLngX : 126.425291, name:'무안군', color:'#f5ffff', root:'전라남도', ename:'Muan-gun'}
    ,{latLngY : 35.127916, latLngX : 126.508620, name:'함평군', color:'#f5ffff', root:'전라남도', ename:'Hampyeong-gun'}
    ,{latLngY : 35.264801, latLngX : 126.445709, name:'영광군', color:'#f5ffff', root:'전라남도', ename:'Yeonggwang-gun'}
    ,{latLngY : 35.326538, latLngX : 126.725867, name:'장성군', color:'#f5ffff', root:'전라남도', ename:'Jangseong-gun'}
    ,{latLngY : 34.368584, latLngX : 126.645752, name:'완도군', color:'#f5ffff', root:'전라남도', ename:'Wando-gun'}
    ,{latLngY : 34.470403, latLngX : 126.200049, name:'진도군', color:'#f5ffff', root:'전라남도', ename:'Jindo-gun'}
    ,{latLngY : 35.060483, latLngX : 126.210251, name:'신안군', color:'#f5ffff', root:'전라남도', ename:'Sinan-gun'}

    //경상북도
	,{latLngY : 36.217586, latLngX : 129.202553, name:'포항시', color:'#f5ffff', root:'경상북도', ename:'Pohang-si'}
//    ,{latLngY : 36.025301, latLngX : 129.345429, name:'남구', color:'#f5ffff', root:'경상북도', ename:'Nam-gu'}
//    ,{latLngY : 36.217586, latLngX : 129.202553, name:'북구', color:'#f5ffff', root:'경상북도', ename:'Buk-gu'}
    ,{latLngY : 35.886238, latLngX : 129.158526, name:'경주시', color:'#f5ffff', root:'경상북도', ename:'Gyeongju-si'}
    ,{latLngY : 36.155460, latLngX : 128.025745, name:'김천시', color:'#f5ffff', root:'경상북도', ename:'Gimcheon-si'}
    ,{latLngY : 36.673971, latLngX : 128.735186, name:'안동시', color:'#f5ffff', root:'경상북도', ename:'Andong-si'}
    ,{latLngY : 36.252507, latLngX : 128.302813, name:'구미시', color:'#f5ffff', root:'경상북도', ename:'Gumi-si'}
    ,{latLngY : 36.921989, latLngX : 128.488954, name:'영주시', color:'#f5ffff', root:'경상북도', ename:'Yeongju-si'}
    ,{latLngY : 36.061699, latLngX : 128.889361, name:'영천시', color:'#f5ffff', root:'경상북도', ename:'Yeongcheon-si'}
    ,{latLngY : 36.479605, latLngX : 128.027772, name:'상주시', color:'#f5ffff', root:'경상북도', ename:'Sangju-si'}
    ,{latLngY : 36.760687, latLngX : 128.097574, name:'문경시', color:'#f5ffff', root:'경상북도', ename:'Mungyeong-si'}
    ,{latLngY : 35.863771, latLngX : 128.737191, name:'경산시', color:'#f5ffff', root:'경상북도', ename:'Gyeongsan-si'}
    ,{latLngY : 36.187097, latLngX : 128.622792, name:'군위군', color:'#f5ffff', root:'경상북도', ename:'Gunwi-gun'}
    ,{latLngY : 36.378986, latLngX : 128.600871, name:'의성군', color:'#f5ffff', root:'경상북도', ename:'Uiseong-gun'}
    ,{latLngY : 36.422300, latLngX : 129.018370, name:'청송군', color:'#f5ffff', root:'경상북도', ename:'Cheongsong-gun'}
    ,{latLngY : 36.752512, latLngX : 129.031835, name:'영양군', color:'#f5ffff', root:'경상북도', ename:'Yeongyang-gun'}
    ,{latLngY : 36.537062, latLngX : 129.271419, name:'영덕군', color:'#f5ffff', root:'경상북도', ename:'Yeongdeok-gun'}
    ,{latLngY : 35.706045, latLngX : 128.756272, name:'청도군', color:'#f5ffff', root:'경상북도', ename:'Cheongdo-gun'}
    ,{latLngY : 35.780298, latLngX : 128.227050, name:'고령군', color:'#f5ffff', root:'경상북도', ename:'Goryeong-gun'}
    ,{latLngY : 35.987980, latLngX : 128.180543, name:'성주군', color:'#f5ffff', root:'경상북도', ename:'Seongju-gun'}
    ,{latLngY : 36.057307, latLngX : 128.404051, name:'칠곡군', color:'#f5ffff', root:'경상북도', ename:'Chilgok-gun'}
    ,{latLngY : 36.726665, latLngX : 128.353439, name:'예천군', color:'#f5ffff', root:'경상북도', ename:'Yecheon-gun'}
    ,{latLngY : 36.989986, latLngX : 128.857302, name:'봉화군', color:'#f5ffff', root:'경상북도', ename:'Bonghwa-gun'}
    ,{latLngY : 37.001052, latLngX : 129.256034, name:'울진군', color:'#f5ffff', root:'경상북도', ename:'Uljin-gun'}
    ,{latLngY : 37.595375, latLngX : 130.828288, name:'울릉군', color:'#f5ffff', root:'경상북도', ename:'Ulleung-gun'}

    //경상남도
	,{latLngY : 35.233580, latLngX : 128.520213, name:'창원시', color:'#f5ffff', root:'경상남도', ename:'Changwon-si'}
//    ,{latLngY : 35.346019, latLngX : 128.611073, name:'의창구', color:'#f5ffff', root:'경상남도', ename:'Uichang-gu'}
//    ,{latLngY : 35.213580, latLngX : 128.670213, name:'성산구', color:'#f5ffff', root:'경상남도', ename:'Seongsan-gu'}
//    ,{latLngY : 35.171271, latLngX : 128.442820, name:'마산합포구', color:'#f5ffff', root:'경상남도', ename:'Masanhappo-gu'}
//    ,{latLngY : 35.239058, latLngX : 128.503998, name:'마산회원구', color:'#f5ffff', root:'경상남도', ename:'Masanhoewon-gu'}
//    ,{latLngY : 35.140639, latLngX : 128.741484, name:'진해구', color:'#f5ffff', root:'경상남도', ename:'Jinhae-gu'}
    ,{latLngY : 35.194874, latLngX : 128.140099, name:'진주시', color:'#f5ffff', root:'경상남도', ename:'Jinju-si'}
    ,{latLngY : 34.884855, latLngX : 128.382851, name:'통영시', color:'#f5ffff', root:'경상남도', ename:'Tongyeong-si'}
    ,{latLngY : 35.060633, latLngX : 128.075875, name:'사천시', color:'#f5ffff', root:'경상남도', ename:'Sacheon-si'}
    ,{latLngY : 35.283833, latLngX : 128.803848, name:'김해시', color:'#f5ffff', root:'경상남도', ename:'Gimhae-si'}
    ,{latLngY : 35.492679, latLngX : 128.740609, name:'밀양시', color:'#f5ffff', root:'경상남도', ename:'Miryang-si'}
    ,{latLngY : 34.857083, latLngX : 128.603137, name:'거제시', color:'#f5ffff', root:'경상남도', ename:'Geoje-si'}
    ,{latLngY : 35.402826, latLngX : 129.010605, name:'양산시', color:'#f5ffff', root:'경상남도', ename:'Yangsan-si'}
    ,{latLngY : 35.437664, latLngX : 128.261954, name:'의령군', color:'#f5ffff', root:'경상남도', ename:'Uiryeong-gun'}
    ,{latLngY : 35.288060, latLngX : 128.370375, name:'함안군', color:'#f5ffff', root:'경상남도', ename:'Haman-gun'}
    ,{latLngY : 35.517709, latLngX : 128.460396, name:'창녕군', color:'#f5ffff', root:'경상남도', ename:'Changnyeong-gun'}
    ,{latLngY : 35.053686, latLngX : 128.240722, name:'고성군', color:'#f5ffff', root:'경상남도', ename:'Goseong-gun'}
    ,{latLngY : 34.792890, latLngX : 127.938655, name:'남해군', color:'#f5ffff', root:'경상남도', ename:'Namhae-gun'}
    ,{latLngY : 35.133825, latLngX : 127.793665, name:'하동군', color:'#f5ffff', root:'경상남도', ename:'Hadong-gun'}
    ,{latLngY : 35.382268, latLngX : 127.873139, name:'산청군', color:'#f5ffff', root:'경상남도', ename:'Sancheong-gun'}
    ,{latLngY : 35.578052, latLngX : 127.720053, name:'함양군', color:'#f5ffff', root:'경상남도', ename:'Hamyang-gun'}
    ,{latLngY : 35.709633, latLngX : 127.906908, name:'거창군', color:'#f5ffff', root:'경상남도', ename:'Geochang-gun'}
    ,{latLngY : 35.581595, latLngX : 128.134231, name:'합천군', color:'#f5ffff', root:'경상남도', ename:'Hapcheon-gun'}

    //제주특별자치도
    ,{latLngY : 33.466347, latLngX : 126.524734, name:'제주시', color:'#f5ffff', root:'제주특별자치도', ename:'Jeju-si'}
    ,{latLngY : 33.326446, latLngX : 126.527034, name:'서귀포시', color:'#f5ffff', root:'제주특별자치도', ename:'Seogwipo-si'}
];

fn_geoDataParse(latLngSeoul, markerSeoul);

//시군구 데이터 파싱
function fn_geoDataParse(latLngArr, markerArr){
	for(var i = 0; i < latLngArr.length; i++){
		markerArr.push({
			latLng :  new naver.maps.LatLng(parseFloat(latLngArr[i].latLngY)+0.01, parseFloat(latLngArr[i].latLngX)-0.01), 
			ename : latLngArr[i].ename,
			name : latLngArr[i].name,
			color : latLngArr[i].color,
			root : latLngArr[i].root
		})
	}
}

//좌표데이터 파싱
for(var i = 0; i < geoLatLng.length; i++){
	markerLatLng.push({
		latLng : new naver.maps.LatLng(parseFloat(geoLatLng[i].latLngY)+0.1,parseFloat(geoLatLng[i].latLngX)-0.1), 
		name:geoLatLng[i].name,
		ename:geoLatLng[i].ename,
		zoomLatLng : new naver.maps.LatLng(geoLatLng[i].zoomLatLngY, geoLatLng[i].zoomLatLngX),
		zoomLevel : geoLatLng[i].zoomLevel,
		color : geoLatLng[i].color
	})
}
