// 현재위치
var ImageHover = function ( $ ) {
	var $hover = $ ( '.Hover' );	
};

var Menus = function ( $ ) {
	var $container,
		$doc,
		$opacity,
		$menus,
		menusArr,
		curMenu,
		defMenu,
		$locD1,
		$locD2,
		$locD3,
		$locD4,
		curLoc;

	function init () {
		$doc = $ ( document );
		$container = $ ( '#container' );
		$opacity = $ ( '.opacity' );
		$menus = $ ( '.header .gnb ul li' );
		$footer = $ ( '#footer' );
		$locD1 = $ ( '#location .Cont_depth1' );
		$locD2 = $ ( '#location .Cont_depth2' );
		$locD3 = $ ( '#location .Cont_depth3' );
		$locD4 = $ ( '#location .Cont_depth4' );
		

		menusArr = [];
		for ( var i = 0, len = $menus.length; i < len; i++ ) {
			var menu = $menus.eq ( i ),
				isDefault = menu.hasClass ( 'on' );
			menusArr.push ( menu );
			menu.on ( 'mouseover focusin', { $target: menu }, menuHn );
			if ( isDefault ) {
				curMenu = menu;
				defMenu = menu;
			}
		}

		//$locD1.find ( '> span > a' ).on ( 'click', { $target: $locD1 },locationHn );
		$locD1.find ( '> a' ).on ( 'click', { $target: $locD1 },locationHn );
		$locD2.find ( '> a' ).on ( 'click', { $target: $locD2 },locationHn );
		$locD3.find ( '> a' ).on ( 'click', { $target: $locD3 },locationHn );
		$locD4.find ( '> a' ).on ( 'click', { $target: $locD4 },locationHn );

		if ( $locD1.length ) {
			var locD1Sub = $locD1.find ( 'ul' ).hide ();
			$locD1.data ( { sub: locD1Sub } );
		}

		if ( $locD2.length ) {
			var locD2Sub = $locD2.find ( 'ul' ).hide ();
			$locD2.data ( { sub: $locD2.find ( 'ul' ) } );
		}

		if ( $locD3.length ) {
			var locD3Sub = $locD3.find ( 'ul' ).hide ();
			$locD3.data ( { sub: $locD3.find ( 'ul' ) } );
		}

		if ( $locD4.length ) {
			var locD4Sub = $locD4.find ( 'ul' ).hide ();
			$locD4.data ( { sub: $locD4.find ( 'ul' ) } );
		}
	}

	function locationHn ( e ) {
		e.preventDefault ();
		var target = e.data.$target;

		if ( curLoc ) {
			$doc.off ( 'click', docHn );
			if ( curLoc.data ( 'sub' ) ) {
				curLoc.data ( 'sub' ).hide ();
			}

			if ( curLoc == target ) {
				curLoc = null;
				return;
			}
		}
		curLoc = target;
		if ( curLoc.data ( 'sub' ) ) {
			curLoc.data ( 'sub' ).show ();
			$doc.on ( 'click', docHn );
		}
	}

	function docHn ( e ) {
		if ( curLoc == null ) {
			return;
		}
		var isLocation1 = false,
			isLocation2 = false,
			isLocation3 = false;
			isLocation4 = false;
		
		if ( $locD1.length ) {
			isLocation1 = $.contains ( $locD1.get ( 0 ), e.target );
		}

		if ( $locD2.length ) {
			isLocation2 = $.contains ( $locD2.get ( 0 ), e.target );
		}

		if ( $locD3.length ) {
			isLocation3 = $.contains ( $locD3.get ( 0 ), e.target );
		}

		if ( $locD4.length ) {
			isLocation4 = $.contains ( $locD4.get ( 0 ), e.target );
		}

		if ( !isLocation1 && !isLocation2 && !isLocation3 && !isLocation4 ) {
			if ( curLoc.data ( 'sub' ) ) {
				curLoc.data ( 'sub' ).hide ();
			}
			curLoc = null;
			$doc.off ( 'click', docHn );
		}
	}
	init ();
};

// fixbar
$.fn.setFixbar = function () {
	var s = this,
		windowHeight,
		$window = $ ( window ),
		$footer = $ ( '#footerWr' ),
		limitTop = 0,
		offset = s.offset (),
		contentH = s.outerHeight (),
		limitTimer,
		transition = '',
		isFixedTrigger = false,
		isShowTrigger = false;	
	 
	function init () {
		getTransitionProperty ();
		s.addClass ( 'fixed' );
		isFixedTrigger = true;
		s.css ( { bottom: -contentH } );
		$footer.css ( { marginTop: contentH } );
		$window.on ( 'scroll', update ).resize ( update );
		$window.trigger ( 'resize' );
		update ();
	}

	
	init ();
};


( function ( wnd, $ ) {
	
	var $wnd = $ (wnd),
		$doc = $ (document),
		tabsArr;

	$doc.ready ( init );

	function init () {
		var imgHover = new ImageHover ( $ ),
			menus = new Menus ( $ ),
			fixBar = $ ( '.fixBar' );

		if ( fixBar.length ) {
			fixBar.setFixbar ();
		}

	}

} ) ( window, jQuery );
