
//차트 그리기의 시작부분 
function fn_setChartAttribute(obj, fn, data, opt) {
	var $_obj = $('#' + obj);
	var $_objArr = [ 'chart-fn', 'chart-data', 'chart-opt' ];
	var $_objDataArr = [ fn, data, opt ];
	$_objArr.forEach(function(t, i) {
		//if (!fn_isNull($_objDataArr[i])) $_obj.attr('data-' + t, (typeof $_objDataArr[i] === 'string') ? $_objDataArr[i] : JSON.stringify($_objDataArr[i]));
		if (!fn_isNull($_objDataArr[i])) $_obj.data(t, (typeof $_objDataArr[i] === 'string') ? $_objDataArr[i] : JSON.stringify($_objDataArr[i]));
	});
}

/**
 * set line amchart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt [ categoryX : name, valueY : value, wave : 곡선 여부, bullet : 포인트 여부 ]
 * @returns
 */
//파라미터가 중요함
//obj 차트를 그릴 div아이디를 의미 보통 id를 주고 사용
//스크립트에서 div에 아이디를 부여하고 해당 data를 가져오는 함수를 만들고 데이터를 가공해줌 oject나 array 
//카테고리X X축에있는 데이터를 연결시킬것 
/*
 * 
 * 위에 주석에 표시되있는 데이터가 key값이 됨 
 * $data = [
 * 		{name : '상수도' : , value : 67}
 * ]
 * 
 * */
function fn_amChart_lineChart(obj, data, opt) {
	//Create chart instance
	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.cursor = new am4charts.XYCursor();
	$chart.data = data;

	var $cAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	$cAxis.renderer.grid.template.location = 0;
	$cAxis.dataFields.category = opt.categoryX;
	$cAxis.renderer.minGridDistance = 40;
	$cAxis.fontSize = 11;
	
	//Create value axis
	var $vAxis = $chart.yAxes.push(new am4charts.ValueAxis());
	
	//Create series
	var $series = $chart.series.push(new am4charts.LineSeries());
	$series.tooltipText = "{name}\ : {valueY}[/]";
	$series.dataFields.valueY = opt.valueY;
	$series.dataFields.categoryX = opt.categoryX;
	// wave : true -> 곡선
	if (opt.wave) $series.tensionX = 0.8;

	// bullet : true -> 차트 포인트 생성
	if (opt.bullet) {
		var $bullet = $series.bullets.push(new am4charts.CircleBullet());
		$bullet.circle.strokeWidth = 2;
		$bullet.circle.radius = 4;
		$bullet.circle.fill = am4core.color("#fff");
		$bullet.color = am4core.color("#fff");
		$bullet.tooltipText = "{name} : {valueY}[/]";
	
		var $states = $bullet.states.create("hover");
		$states.properties.scale = 1.4;
	}
	
	opt.step = 1;
	fn_setChartAttribute(obj, 'fn_amChart_lineChart', data, opt);
}

/**
 * set multi line chart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt [ categoryX : name, categoryArr : 데이터 명칭 (Array), categoryViewArr : 화면에 보여줄 데이터 명칭(Array), wave : 곡선 여부, bullet : 포인트 여부, share : 툴팁 공유여부 ]
 * @returns
 */
//멀티라인차트는 라인이 2개임 
function fn_amChart_multiLineChart(obj, data, opt) {
	//Create chart instance
	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.legend = new am4charts.Legend();
	$chart.data = data;
	//$chart.colors.step = 2;
	$chart.legend.itemContainers.template.paddingTop = 3;
	$chart.legend.itemContainers.template.paddingBottom = 3;
	
	// share [ 툴팁 공유 여부 ]
	if (opt.share) $chart.cursor = new am4charts.XYCursor();

	var $cAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	$cAxis.renderer.grid.template.location = 0;
	$cAxis.dataFields.category = opt.categoryX;
	$cAxis.fontSize = 11;

	//Create value axis
	var $vAxis = $chart.yAxes.push(new am4charts.ValueAxis());
	
	function fn_setSeries(field, name) {
		
		//Create series
		var $series = $chart.series.push(new am4charts.LineSeries());
		$series.tooltipText = "{name} : {valueY}[/]";
		$series.dataFields.valueY = field;
		$series.dataFields.categoryX = opt.categoryX;
	  $series.yAxis = $vAxis;
	  $series.name = name;
	  $series.strokeWidth = 2;
	  $series.legendSettings.valueText = "{valueY}";
		// wave : true -> 곡선
		if (opt.wave) $series.tensionX = 0.8;

		// bullet : true -> 차트 포인트 생성
		if (opt.bullet) {
			var $bullet = $series.bullets.push(new am4charts.CircleBullet());
			$bullet.circle.strokeWidth = 2;
			$bullet.circle.radius = 4;
			$bullet.circle.fill = am4core.color("#fff");
			$bullet.color = am4core.color("#fff");
			$bullet.tooltipText = "{name} : {valueY}[/]";
		
			var $states = $bullet.states.create("hover");
			$states.properties.scale = 1.4;
		}
	}

	var $categoryArr = opt.categoryArr, $categoryViewArr = (fn_isNull(opt.categoryViewArr) ? $categoryArr : opt.categoryViewArr);
	$($categoryArr).each(function(i, e) {
		fn_setSeries(e, $categoryViewArr[i]);
	});

	opt.step = 2;
	fn_setChartAttribute(obj, 'fn_amChart_multiLineChart', data, opt)
}

/**
 * set bar [ column ] amchart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt [ categoryX : name, valueY : value ]
 * @returns
 */

function fn_amChart_barChart(obj, data, opt) {

	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
	$chart.data = data;

	var $cAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	$cAxis.renderer.grid.template.location = 0;
	$cAxis.dataFields.category = opt.categoryX;
	$cAxis.renderer.minGridDistance = 40;
	$cAxis.fontSize = 11;

	var $vAxis = $chart.yAxes.push(new am4charts.ValueAxis());
	$vAxis.min = 0;
	$vAxis.strictMinMax = true;
	$vAxis.renderer.minGridDistance = 30;

	var $series = $chart.series.push(new am4charts.ColumnSeries());
	$series.dataFields.categoryX = opt.categoryX;
	$series.dataFields.valueY = opt.valueY;
	$series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
	$series.columns.template.tooltipY = 0;
	$series.columns.template.strokeOpacity = 0;

	// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
	$series.columns.template.adapter.add("fill", function(fill, target) {
	  return $chart.colors.getIndex(target.dataItem.index);
	});

	opt.step = 1;
	fn_setChartAttribute(obj, 'fn_amChart_barChart', data, opt)
}

/**
 * stacked bar amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt [ categoryX : name, categoryArr : 데이터 명칭 (Array), categoryViewArr : 화면에 보여줄 데이터 명칭(Array), colors : 색상 지정(Array), stacked : 여러 데이터를 한 컬럼에 표시할 경우 true, percent : 100% 컬럼 차트 일 경우 true ]
 * @returns
 */
//얘도 데이터가 한개인가 어려개 인가 차이????? 얘를 써야 할것임
function fn_amChart_stackedBarChart(obj, data, opt) {

	//Create chart instance
	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.data = data;
	$chart.legend = new am4charts.Legend();
	//$chart.cursor = new am4charts.XYCursor();
	
	// set color list
	if (!fn_isNull(opt.colors)) {
		var $colorArr = new Array();
		$(opt.colors).each(function(i, e) {
			$colorArr.push(am4core.color(e));
		});
		$chart.colors.list = $colorArr;
	}
	//Create axes
	var $cAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	$cAxis.dataFields.category = opt.categoryX;
	$cAxis.renderer.grid.template.location = 0;
	
	var $vAxis = $chart.yAxes.push(new am4charts.ValueAxis());
	$vAxis.min = 0;
	if (opt.percent) $vAxis.max = 100;
	$vAxis.calculateTotals = true;
	
	//Create series
	function fn_setSeries(field, name) {
	 
	 // Set up series
	 var $series = $chart.series.push(new am4charts.ColumnSeries());
	 $series.name = name;
	 $series.dataFields.valueY = field;
	 $series.dataFields.categoryX = opt.categoryX;
	 if (opt.percent) $series.dataFields.valueYShow = "totalPercent";
	 
	 // Make it stacked
	 if (opt.stacked) $series.stacked = true;
	 
	 // Configure columns
	 $series.columns.template.width = am4core.percent(60);
	 
	 var $seriesToolTipTxt = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
	 if (opt.percent) $seriesToolTipTxt = "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
	 $series.columns.template.tooltipText = $seriesToolTipTxt;
	 
	 // Add label
	 var $bullet = $series.bullets.push(new am4charts.LabelBullet());
	 $bullet.locationY = 0.5;
	 
	 return $series;
	}
	
	var $categoryArr = opt.categoryArr, $categoryViewArr = (fn_isNull(opt.categoryViewArr) ? $categoryArr : opt.categoryViewArr);
	$($categoryArr).each(function(i, e) {
		fn_setSeries(e, $categoryViewArr[i]);
	});

	opt.step = 2;
	fn_setChartAttribute(obj, 'fn_amChart_stackedBarChart', data, opt);
}

/**
 * stacked area amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt [ categoryX : name, categoryArr : 데이터 명칭 (Array) ]
 * @returns
 */
function fn_amChart_stackedAreaChart(obj, data, opt) {

	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.data = data;
	
	$chart.legend = new am4charts.Legend();
	$chart.cursor = new am4charts.XYCursor();

	//Create axes
	var $cAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	$cAxis.dataFields.category = opt.categoryX;
	$cAxis.renderer.grid.template.location = 0;
	$chart.cursor.xAxis = $cAxis;

	var valueAxis = $chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.tooltip.disabled = true;

	function fn_setSeries(field, name) {
		var $series = $chart.series.push(new am4charts.LineSeries());
		$series.dataFields.categoryX = opt.categoryX;
		$series.name = name;
		$series.dataFields.valueY = field;
		$series.tooltipHTML = '<span style="color:#000;">' + name + ' : <b>{valueY.value}</b></span>';
		$series.tooltipText = "[#000]{valueY.value}[/]";
//		$series.tooltip.background.fill = am4core.color("#FFF");
		$series.tooltip.getStrokeFromObject = true;
		$series.tooltip.background.strokeWidth = 3;
		$series.tooltip.getFillFromObject = false;
		$series.fillOpacity = 0.6;
		$series.strokeWidth = 2;
		$series.stacked = true;

		// wave : true -> 곡선
		if (opt.wave) $series.tensionX = 0.8;
		
		return $series;
	}

	var $categoryArr = opt.categoryArr, $categoryViewArr = (fn_isNull(opt.categoryViewArr) ? $categoryArr : opt.categoryViewArr);
	$($categoryArr).each(function(i, e) {
		fn_setSeries(e, $categoryViewArr[i]);
	});

	opt.step = 2;
	fn_setChartAttribute(obj, 'fn_amChart_stackedAreaChart', data, opt);
}

/**
 * set multi chart [ bar & line ]
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt [ categoryX : name, categoryArr : 데이터 명칭 (Array), categoryViewArr : 화면에 보여줄 데이터 명칭(Array), categoryTitleArr : 차트 Y 값 타이틀 ]
 * @returns
 */
function fn_amChart_multiChart(obj, data, opt) {

	//Create chart instance
	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.data = data;
	$chart.legend = new am4charts.Legend();
	$chart.cursor = new am4charts.XYCursor();
	
	//Create axes
	var $cAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	$cAxis.dataFields.category = opt.categoryX;
	
	function fn_setSeries(field, name, title, index) {

		var $vAxis = $chart.yAxes.push(new am4charts.ValueAxis());
		$vAxis.title.text = title;
		
		var $series = $chart.series.push( (index == 0 ? new am4charts.ColumnSeries() : new am4charts.LineSeries()) );
		$series.dataFields.valueY = field;
		$series.dataFields.categoryX = opt.categoryX;
		$series.name = name;
		$series.tooltipText = "{name}: [bold]{valueY}[/]";
		
		if (index == 1) {
			$vAxis.renderer.opposite = true;
			$series.strokeWidth = 3;
			$series.yAxis = $vAxis;
			// wave : true -> 곡선
			if (opt.wave) $series.tensionX = 0.8;
		}
	}

	var $categoryArr = opt.categoryArr, $categoryViewArr = (fn_isNull(opt.categoryViewArr) ? $categoryArr : opt.categoryViewArr);
	var $categoryTitleArr = (fn_isNull(opt.categoryTitleArr) ? $categoryArr : opt.categoryTitleArr);
	$($categoryArr).each(function(i, e) {
		fn_setSeries(e, $categoryViewArr[i], $categoryTitleArr[i], i);
	});

	opt.step = 2;
	fn_setChartAttribute(obj, 'fn_amChart_multiChart', data, opt);
}

/**
 * set pie / donut amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt - [ category : name, value : value, donut : 도넛 차트 (true), radius : 차트 크기별 표기(true) ]
 * @returns
 */
function fn_amChart_PieChart(obj, data, opt) {
	//Create chart instance
	var $chart = am4core.create(obj, am4charts.PieChart);
	var $radius = (fn_isNull(opt.radius) ? false : opt.radius);
	$chart.data = data;

	//Set inner radius
	if (opt.donut) $chart.innerRadius = am4core.percent(50);
	
	//Add and configure Series
	var $series = $chart.series.push(new am4charts.PieSeries());
	$series.dataFields.value = (fn_isNull(opt.value) ? 'value' : opt.value);
	if ($radius) $series.dataFields.radiusValue = (fn_isNull(opt.value) ? 'value' : opt.value);
	$series.dataFields.category = (fn_isNull(opt.name) ? 'name' : opt.name);
	$series.slices.template.stroke = am4core.color("#fff");
	$series.slices.template.strokeWidth = 2;
	$series.slices.template.strokeOpacity = 1;
	
	//This creates initial animation
	$series.hiddenState.properties.opacity = 1;
	$series.hiddenState.properties.endAngle = -90;
	$series.hiddenState.properties.startAngle = -90;

	opt.step = 1;
	fn_setChartAttribute(obj, 'fn_amChart_PieChart', data, opt);
}

/**
 * set gauge amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. 10 ]
 * @param opt - [ colorArr : 색상 array ]
 * @returns
 */
function fn_amChart_gauageChart(obj, data, opt) {

	//create chart
	var $chart = am4core.create(obj, am4charts.GaugeChart);
	$chart.innerRadius = -15;
	
	var $axis = $chart.xAxes.push(new am4charts.ValueAxis());
	$axis.min = 0;
	$axis.max = 100;
	$axis.strictMinMax = true;
//	$axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor("background");
//	$axis.renderer.grid.template.strokeOpacity = 0.3;
	
	var $colorSet = new am4core.ColorSet();
	
	var $colorArr = (fn_isNull(opt.colorArr) ? ['red', 'yellow', 'green'] : opt.colorArr);
	var $gradient = new am4core.LinearGradient();
	$($colorArr).each(function(i, e) {
		$gradient.stops.push({color:am4core.color(e)});
	});

//	var range = $axis.axisRanges.create();
//	range.value = 0;
//	range.endValue = 40;
//	range.axisFill.fillOpacity = 0.8;
//	range.axisFill.fill = '#db096c';
//
//	var range2 = $axis.axisRanges.create();
//	range2.value = 40;
//	range2.endValue = 60;
//	range2.axisFill.fillOpacity = 0.8;
//	range2.axisFill.fill = '#f4c300';
//
//	var range3 = $axis.axisRanges.create();
//	range3.value = 60;
//	range3.endValue = 100;
//	range3.axisFill.fillOpacity = 0.8;
//	range3.axisFill.fill = '#4db500';

	$axis.renderer.line.stroke = $gradient;
	$axis.renderer.line.strokeWidth = 15;
	$axis.renderer.line.strokeOpacity = 1;
	$axis.renderer.grid.template.disabled = true;

	var $hand = $chart.hands.push(new am4charts.ClockHand());
	$hand.radius = am4core.percent(97);
	$hand.showValue(data, am4core.ease.cubicOut);

	var label = $chart.radarContainer.createChild(am4core.Label);
	label.isMeasured = false;
	label.fontSize = 14;
	label.x = am4core.percent(50);
	label.y = am4core.percent(100);
	label.horizontalCenter = "middle";
	label.verticalCenter = "top";
	//label.html = '<div class="center mgt10"><span style="font-size:17px;">' + $.number(opt.count) + '건 / ' + data.toFixed(1) + '점</span></div>';
	label.html = '<p class="emo font_sq font-900">' + $.number(opt.count) + '건 / ' + data.toFixed(1) + '점</p>';
	label.html += '<div class="emoBar">';
	label.html += '<a href="javascript:void(0);" class="denial" style="width:' + (100 - data.toFixed(1)) + '%;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="' + (100 - data.toFixed(1)) + '% 부정"><span style="float:left; padding-left:20px;"> ' + (100 - data.toFixed(1)) + '% 부정</span></a><a href="javascript:void(0);" class="positive" style="width:' + data.toFixed(1) + '%;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="' + data.toFixed(1) + '% 긍정"><span style="float:right; padding-right:20px;">' + data.toFixed(1) + '% 긍정</span></a>';
	label.html += '</div>';
	opt.step = 1;
	fn_setChartAttribute(obj, 'fn_amChart_gauageChart', data, opt);
}

/**
 * set wordcloud amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { word : 'word', value : 100 } ] ]
 * @param opt - [ word : word, value : value ]
 * @returns
 */
//워드클라우드 너무간단 디자인요소가 좀 많이 들어옴 
function fn_amChart_wordCloud(obj, data, opt) {

	var $chart = am4core.create(obj, am4plugins_wordCloud.WordCloud);

	var $series = $chart.series.push(new am4plugins_wordCloud.WordCloudSeries());
	$series.randomness = 0.1;
	$series.rotationThreshold = 0;
	$series.data = data;

	$series.dataFields.word = opt.word;
	$series.dataFields.value = opt.value;
	$series.labels.template.tooltipText = "{word}: {value}";
	$series.configField = 'config';
	$series.labels.template.propertyFields.fill = "color";

	$series.heatRules.push({
	 "target": $series.labels.template,
	 "property": "fill",
	 "min": am4core.color("#0000CC"),
	 "max": am4core.color("#CC00CC"),
	 "dataField": "value"
	});

//	$series.labels.template.url = "https://stackoverflow.com/questions/tagged/{word}";
//	$series.labels.template.urlTarget = "_blank";
	//$series.labels.template.url = "javascript:fn_wtest('{word}{value}')";
	
	$series.labels.template.events.on('hit', function(event) {
		//console.log(event);
	});
	
	$series.minFontSize = 30;
	
	var $hover = $series.labels.template.states.create("hover");
	$hover.properties.fill = am4core.color("#FF0000");

	opt.step = 3;
	fn_setChartAttribute(obj, 'fn_amChart_wordCloud', data, opt);
}

/**
 * set treemap amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { word : 'word', value : 100 } ] ]
 * @param opt - [ word : word, value : value, child : children ]
 * @returns
 */
// + child??추가? 잘안씀 
function fn_amChart_treemap(obj, data, opt) {
	//create chart
	//$nData.push({ name : '-', children : data });
	var $chart = am4core.create(obj, am4charts.TreeMap);
	$chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
	$chart.data = data;
	$chart.colors.step = 2;
	
	//define data fields
	$chart.dataFields.value = opt.value;
	$chart.dataFields.name = opt.word;
	$chart.dataFields.children = (fn_isNull(opt.child) ? 'children' : opt.child);
	$chart.dataFields.color = 'color';
	$chart.zoomable = false;
//$chart.maxLevels = 2;
	
	var $bgColor = new am4core.InterfaceColorSet().getFor('background');
	
	//level 0 series template
	var level0SeriesTemplate = $chart.seriesTemplates.create("0");
	var level0ColumnTemplate = level0SeriesTemplate.columns.template;
	
	level0ColumnTemplate.column.cornerRadius(10, 10, 10, 10);
	level0ColumnTemplate.fillOpacity = 0;
	level0ColumnTemplate.strokeWidth = 4;
	level0ColumnTemplate.strokeOpacity = 0;
	
	//level 1 series template
	var level1SeriesTemplate = $chart.seriesTemplates.create("1");
	var level1ColumnTemplate = level1SeriesTemplate.columns.template;
	
	level1SeriesTemplate.tooltip.animationDuration = 0;
	level1SeriesTemplate.strokeOpacity = 1;
	
	level1ColumnTemplate.column.cornerRadius(10, 10, 10, 10)
	level1ColumnTemplate.fillOpacity = 1;
	level1ColumnTemplate.strokeWidth = 4;
	level1ColumnTemplate.stroke = $bgColor;
	
	var $bullet = level1SeriesTemplate.bullets.push(new am4charts.LabelBullet());
	$bullet.locationY = 0.5;
	$bullet.locationX = 0.5;
	$bullet.label.text = "{name}";
	$bullet.label.fill = $bgColor;

	opt.step = 3;
	fn_setChartAttribute(obj, 'fn_amChart_treemap', data, opt);
}

/**
 * 
 * @param obj
 * @param data
 * @param opt
 * @returns
 */
function fn_amChart_bubbleChart(obj, data, opt) {

	var $chart = am4core.create(obj, am4charts.XYChart);

	var valueAxisX = $chart.xAxes.push(new am4charts.ValueAxis());
	valueAxisX.renderer.ticks.template.disabled = true;
	valueAxisX.renderer.axisFills.template.disabled = true;

	var valueAxisY = $chart.yAxes.push(new am4charts.ValueAxis());
	valueAxisY.renderer.ticks.template.disabled = true;
	valueAxisY.renderer.axisFills.template.disabled = true;

	var series = $chart.series.push(new am4charts.LineSeries());
	series.dataFields.valueX = "x";
	series.dataFields.valueY = "y";
	series.dataFields.value = "value";
	series.strokeOpacity = 0;
	series.sequencedInterpolation = true;
	series.tooltip.pointerOrientation = "vertical";

	var bullet = series.bullets.push(new am4core.Circle());
	bullet.fill = am4core.color("#ff0000");
	bullet.propertyFields.fill = "color";
	bullet.strokeOpacity = 0;
	bullet.strokeWidth = 2;
	bullet.fillOpacity = 0.5;
	bullet.stroke = am4core.color("#ffffff");
	bullet.hiddenState.properties.opacity = 0;
	bullet.tooltipText = "[bold]{title}:[/]\nPopulation: {value.value}\nIncome: {valueX.value}\nLife expectancy:{valueY.value}";

	var outline = $chart.plotContainer.createChild(am4core.Circle);
	outline.fillOpacity = 0;
	outline.strokeOpacity = 0.8;
	outline.stroke = am4core.color("#ff0000");
	outline.strokeWidth = 2;
	outline.hide(0);

	var blurFilter = new am4core.BlurFilter();
	outline.filters.push(blurFilter);

	bullet.events.on("over", function(event) {
	    var target = event.target;
	    $chart.cursor.triggerMove({ x: target.pixelX, y: target.pixelY }, "hard");
	    $chart.cursor.lineX.y = target.pixelY;
	    $chart.cursor.lineY.x = target.pixelX - $chart.plotContainer.pixelWidth;
	    valueAxisX.tooltip.disabled = false;
	    valueAxisY.tooltip.disabled = false;

	    outline.radius = target.pixelRadius + 2;
	    outline.x = target.pixelX;
	    outline.y = target.pixelY;
	    outline.show();
	})

	bullet.events.on("out", function(event) {
		$chart.cursor.triggerMove(event.pointer.point, "none");
		$chart.cursor.lineX.y = 0;
		$chart.cursor.lineY.x = 0;
    valueAxisX.tooltip.disabled = true;
    valueAxisY.tooltip.disabled = true;
    outline.hide();
	})

	var hoverState = bullet.states.create("hover");
	hoverState.properties.fillOpacity = 1;
	hoverState.properties.strokeOpacity = 1;

	series.heatRules.push({ target: bullet, min: 2, max: 60, property: "radius" });

	bullet.adapter.add("tooltipY", function (tooltipY, target) {
	    return -target.radius;
	})

	$chart.cursor = new am4charts.XYCursor();
	$chart.cursor.behavior = "zoomXY";

	$chart.scrollbarX = new am4core.Scrollbar();
	$chart.scrollbarY = new am4core.Scrollbar();

	$chart.data = [
	    {
	        "title": "Afghanistan",
	        "id": "AF",
	        "color": "#eea638",
	        "continent": "asia",
	        "x": 1349.69694102398,
	        "y": 60.524,
	        "value": 33397058
	    },
    {
        "title": "Korea, Dem. Rep.",
        "id": "KP",
        "color": "#eea638",
        "continent": "asia",
        "x": 1540.44018783769,
        "y": 69.701,
        "value": 24553672
    },
	   ];

	opt.step = 3;
	fn_setChartAttribute(obj, 'fn_amChart_bubbleChart', data, opt);
}

/**
 * set bubble heat amchart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { weekday : 'MON', hour : '12:00', value : 100 } ] ]
 * @param opt [ categoryX : 'weekday', categoryY : 'hour', value : 'value', toolTip : 툴팁 정보 ]
 * @returns
 */
function fn_amChart_bubbleHeatChart(obj, data, opt) {

	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.maskBullets = false;
	$chart.data = data;

	var $xAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	var $yAxis = $chart.yAxes.push(new am4charts.CategoryAxis());

	$yAxis.dataFields.category = opt.categoryY;
	$xAxis.renderer.minGridDistance = 40;
	$xAxis.dataFields.category = opt.categoryX;

	$xAxis.renderer.grid.template.disabled = true;
	$yAxis.renderer.grid.template.disabled = true;
	$xAxis.renderer.axisFills.template.disabled = true;
	$yAxis.renderer.axisFills.template.disabled = true;
	$yAxis.renderer.ticks.template.disabled = true;
	$xAxis.renderer.ticks.template.disabled = true;

	$yAxis.renderer.inversed = true;

	var $series = $chart.series.push(new am4charts.ColumnSeries());
	$series.dataFields.categoryY = opt.categoryY;
	$series.dataFields.categoryX = opt.categoryX;
	$series.dataFields.value = opt.value;
	$series.columns.template.disabled = true;
	$series.sequencedInterpolation = true;
	//series.defaultState.transitionDuration = 3000;

	var $bullet = $series.bullets.push(new am4core.Circle());
	$bullet.tooltipText = (opt.toolTip ? opt.toolTip : '{' + opt.categoryX + '}, {' + opt.categoryY + '} : {' + opt.value + '.workingValue.formatNumber(\'#.\')}');
	$bullet.strokeWidth = 3;
	$bullet.stroke = am4core.color("#ffffff");
	$bullet.strokeOpacity = 0;

	$bullet.adapter.add("tooltipY", function(tooltipY, target) {
	  return -target.radius + 1;
	})

	$series.heatRules.push({
	  property: "radius",
	  target: $bullet,
	  min: 2,
	  max: 40
	});

	$bullet.hiddenState.properties.scale = 0.01;
	$bullet.hiddenState.properties.opacity = 1;

	var $hoverState = $bullet.states.create("hover");
	$hoverState.properties.strokeOpacity = 1;

	opt.step = 3;
	fn_setChartAttribute(obj, 'fn_amChart_bubbleHeatChart', data, opt);
}

/**
 * set risk heat bubble amchart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100, linkWith : [ '홍길자' ], children : [ { name : '홍순', value : 40 } ] } ] ]
 * @param opt [ categoryX : 'week', categoryY : 'day', value : 'value', xTitle : x축 제목, yTitle : y축 제목 ]
 * @returns
 */
function fn_amChart_riskBubbleChart(obj, data, opt) {

	var $chart = am4core.create(obj, am4charts.XYChart);
	$chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

	$chart.data = data;
	$chart.maskBullets = false;

	var $xAxis = $chart.xAxes.push( new am4charts.CategoryAxis() );
	$xAxis.dataFields.category = opt.categoryX;
	$xAxis.renderer.grid.template.disabled = true;
	$xAxis.renderer.minGridDistance = 40;
	$xAxis.title.text = opt.xTitle;

	var $yAxis = $chart.yAxes.push( new am4charts.CategoryAxis() );
	$yAxis.dataFields.category = opt.categoryY;
	$yAxis.renderer.grid.template.disabled = true;
	$yAxis.renderer.inversed = true;
	$yAxis.renderer.minGridDistance = 30;
	$yAxis.title.text = opt.yTitle;

	var $series = $chart.series.push( new am4charts.ColumnSeries() );
	$series.dataFields.categoryX = opt.categoryX;
	$series.dataFields.categoryY = opt.categoryY;
	$series.dataFields.value = opt.value;
	$series.sequencedInterpolation = true;
	$series.defaultState.transitionDuration = 3000;

	// Set up column appearance
	var $column = $series.columns.template;
	$column.strokeWidth = 2;
	$column.strokeOpacity = 1;
	$column.stroke = am4core.color('#ffffff');
	$column.tooltipText = (opt.toolTip ? opt.toolTip : '{' + opt.categoryX + '}, {' + opt.categoryY + '} : {' + opt.value + '.workingValue.formatNumber(\'#.\')}');
	$column.width = am4core.percent( 100 );
	$column.height = am4core.percent( 100 );
	$column.column.cornerRadius(6, 6, 6, 6);
	$column.propertyFields.fill = '#fff';

	// Set up bullet appearance
	var $bullet = $series.bullets.push(new am4charts.LabelBullet());
	$bullet.label.text = '{' + opt.value + '}';
	$bullet.label.fill = am4core.color('#000');
	$bullet.zIndex = 1;
	$bullet.fontSize = 11;
	$bullet.interactionsEnabled = false;

	opt.step = 3;
	fn_setChartAttribute(obj, 'fn_amChart_riskBubbleChart', data, opt);
}

/**
 * set network amchart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100, linkWith : [ '홍길자' ], children : [ { name : '홍순', value : 40 } ] } ] ]
 * @param opt [ event : click event func ]
 * @returns
 */
function fn_amChart_forceDirected(obj, data, opt) {

	var $chart = am4core.create(obj, am4plugins_forceDirected.ForceDirectedTree);
	var $series = $chart.series.push(new am4plugins_forceDirected.ForceDirectedSeries());
	$series.dataFields.linkWith = opt.linkWith;
	$series.dataFields.name = opt.name;
	$series.dataFields.id = opt.name;
	$series.dataFields.value = opt.value;
	$series.dataFields.children = opt.children;

	$series.nodes.template.label.text = '{' + opt.name + '}';
	$series.fontSize = 11;
	$series.linkWithStrength = 0;

	var $nodeTemplate = $series.nodes.template;
	$nodeTemplate.tooltipText = '{' + opt.name + '} {' + opt.value + '}';
	$nodeTemplate.fillOpacity = 1;
	$nodeTemplate.label.hideOversized = true;
	$nodeTemplate.label.truncate = true;

	var $linkTemplate = $series.links.template;
	$linkTemplate.strokeWidth = 1;
	
	var $linkHoverState = $linkTemplate.states.create("hover");
	$linkHoverState.properties.strokeOpacity = 1;
	$linkHoverState.properties.strokeWidth = 4;

	$nodeTemplate.events.on("over", function (event) {
	    var dataItem = event.target.dataItem;
	    dataItem.childLinks.each(function (link) {
	        link.isHover = true;
	    })
	})

	$nodeTemplate.events.on("out", function (event) {
	    var dataItem = event.target.dataItem;
	    dataItem.childLinks.each(function (link) {
	        link.isHover = false;
	    })
	})
	
	if (!fn_isNull(opt.event)) {
		$nodeTemplate.events.on("hit", opt.event);
	}
	
	$series.data = data;
//	$series.minRadius = 30;

	opt.step = 3;
	fn_setChartAttribute(obj, 'fn_amChart_forceDirected', data, opt);
}

/**
 * radar amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value1 : 100, value2 : 200, ... } ] ]
 * @param opt [ categoryX : name, categoryArr : 데이터 명칭 (Array), categoryViewArr : 화면에 보여줄 데이터 명칭(Array), colors : 색상 지정(Array), stacked : 여러 데이터를 한 컬럼에 표시할 경우 true, percent : 100% 컬럼 차트 일 경우 true ]
 * @returns
 */
function fn_amChart_RadarChart(obj, data, opt) {

	var $chart = am4core.create(obj, am4charts.RadarChart);
	$chart.data = data;
	$chart.cursor = new am4charts.RadarCursor();
	$chart.legend = new am4charts.Legend();

	// set color list
	if (!fn_isNull(opt.colors)) {
		var $colorArr = new Array();
		$(opt.colors).each(function(i, e) {
			$colorArr.push(am4core.color(e));
		});
		$chart.colors.list = $colorArr;
	}
	
	/* Create axes */
	//var $cAxis = $chart.xAxes.push(new am4charts.DateAxis());
	var $cAxis = $chart.xAxes.push(new am4charts.CategoryAxis());
	$cAxis.dataFields.category = opt.categoryX;

	var $vAxis = $chart.yAxes.push(new am4charts.ValueAxis());
	$vAxis.extraMin = 0.2;
	$vAxis.extraMax = 0.2;
	$vAxis.tooltip.disabled = true;

	function fn_setSeries(field, name) {
		var $series = $chart.series.push(new am4charts.RadarSeries());
		$series.dataFields.valueY = field;
		$series.dataFields.categoryX = opt.categoryX;
		$series.strokeWidth = 3;
		$series.tooltipText = "{name}: [bold]{valueY}[/]";
		$series.name = name;
		$series.bullets.create(am4charts.CircleBullet);
		$series.dataItems.template.locations.dateX = 0.5;
	}

	var $categoryArr = opt.categoryArr, $categoryViewArr = (fn_isNull(opt.categoryViewArr) ? $categoryArr : opt.categoryViewArr);
	$($categoryArr).each(function(i, e) {
		fn_setSeries(e, $categoryViewArr[i]);
	});

	opt.step = 4;
	fn_setChartAttribute(obj, 'fn_amChart_RadarChart', data, opt)
}
/**
 * sorted bar amChart
 * @param obj - chart element id name
 * @param data - chart data [ ex. [ { name : '홍길동', value : 100 } ] ]
 * @param opt [ categoryX : name, valueY : value ]
 * @returns
 */

function fn_amChart_SortedBarChart(obj, data, opt){
	
	var chart = am4core.create(obj, am4charts.XYChart);
	chart.padding(40, 40, 40, 40);
	
	var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
	categoryAxis.renderer.grid.template.location = 0;
	categoryAxis.dataFields.category = opt.categoryX;
	categoryAxis.renderer.minGridDistance = 1;
	categoryAxis.renderer.inversed = true;
	categoryAxis.renderer.grid.template.disabled = true;

	var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
	valueAxis.min = 0;

	var series = chart.series.push(new am4charts.ColumnSeries());
	series.dataFields.categoryY = opt.categoryX;
	series.dataFields.valueX = opt.valueY;
	series.tooltipText = "{valueX.value}"
	series.columns.template.strokeOpacity = 0;
	series.columns.template.column.cornerRadiusBottomRight = 5;
	series.columns.template.column.cornerRadiusTopRight = 5;

	var labelBullet = series.bullets.push(new am4charts.LabelBullet())
	labelBullet.label.horizontalCenter = "left";
	labelBullet.label.dx = 10;
	labelBullet.label.text = "{values.valueX.workingValue.formatNumber('#.0as')}";
	labelBullet.locationX = 1;

	// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
	series.columns.template.adapter.add("fill", function(fill, target){
	  return chart.colors.getIndex(target.dataItem.index);
	});

	categoryAxis.sortBySeries = series;
	chart.data = data;
	
	opt.step = 1;
	fn_setChartAttribute(obj, 'fn_amChart_SortedBarChart', data, opt);
}


/**
 * 상관관계 분석
 * @param obj
 * @param data
 * @param opt
 * @returns
 */
function fn_setKeywordRelationChart(obj, data, opt) {
	var $correlation = (fn_isNull(data.result)) ? 0 : data.result, $source = opt.source, $target = opt.target;
	var $count = Number(Math.abs($correlation)).toFixed(4), $countArr = [ '낮음', '보통', '높음' ], $countColorArr = [ 'red', 'yellow', 'green' ];
	var $level = ($count >= 0 && $count <= 0.3) ? 0 : ($count > 0.3 && $count <= 0.6) ? 1 : 2;
	var $tag = '<div class="chart-correlation"><div class="result-object">“<span>언급량</span> : ' + $source + '“ , “<span>언급량</span> : ' + $target + '“</div>';
	$tag += '<div class="result"><span>상관계수</span><span class="count mgl20">' + Number($correlation).toFixed(4) + '</span></div>';

	$tag += '<p class="mgt20"><span class="btn btn-info btn-rounded waves-effect waves-light"><strong>상관분석</strong></span> <mark>키워드 간의 상호관계를 측정하는 분석</mark> 방법입니다.</p>';
	$tag += '<p class="mgt10"><span class="btn btn-info btn-rounded waves-effect waves-light"><strong>상관계수</strong></span> 상관계수 r는 항상 부등식 －1≤r≤1을 만족시키며,</p>';
	$tag += '<p style="padding-left:80px;">양의 상관관계가 있을 때는 r＞0, 음의 상관관계가 있을 때는 r＜0이다. 또 무상관일 때는 r＝0이 된다.</p>';
	
	$tag += '<div class="trafficlight-wrap"><div class="trafficlight">';
	$tag += '</div>';
	$('#' + obj).html($tag);

	opt.step = 6;
	fn_setChartAttribute(obj, 'fn_setKeywordRelationChart', data, opt);
}

/**
 * 상관관계 순위
 * @param obj
 * @param data
 * @param opt
 * @returns
 */
function fn_setCorRelationChart(obj, data, opt) {
	var $listCnt = opt.cnt;			
	var $tag = '<table class="table table-hover">';
	$tag += '<colgroup>';
	$tag += '<col style="width:30px"><col><col style="width:30px"><col style="width:130px"><col style="width:150px">';
	$tag += '</colgroup>';
	$tag += '<tbody>';
	
	$(data).each(function(i, e) {
		if(i < $listCnt){
			var $trCls = '', $tdCls = '', $keyword = e.keyword, $ccnt = e.correlation;
			var $ccntpercent = parseInt($ccnt * 100);
			if (!($ccnt > 0)) $ccntpercent = - $ccntpercent;
			$tag += '<tr rel="' + $keyword + '" class="correlationTr' + (i == 0 ? ' active' : '') + '" style="cursor:pointer;">';
			$tag += '<td class="rank-num-corr"><span>' + ( i + 1 ) + '</span></td>';
			$tag += '<td><strong>' + $keyword + '</strong></td>';
			$tag += '<td class="label-state center"><label class="label label-gradient-' + ($ccnt > 0 ? 'primary' : 'danger') + '">';
			$tag += '<i class="mdi mdi-' + ($ccnt > 0 ? 'plus' : 'minus') + '"></i><span class="sr-only">' + ($ccnt > 0 ? '긍정' : '부정') + '</span>';
			$tag += '</label></td>';
			$tag += '<td class="correlation-completion">';
			$tag += '<div class="progress">';
			$tag += '<div style="width: ' + $ccntpercent + '%;" role="progressbar" class="progress-bar progress-bar-striped progress-bar-animated' + ($ccnt > 0 ? ' bg-primary' : ' bg-danger') + '"></div>';
			$tag += '</div>';
			$tag += '</td>';
			$tag += '<td>';
			if ($ccntpercent >= 80) $tag += '<span class="text-primary m-l-xs"><strong>높음</strong></span>';
			else if ($ccntpercent >= 50 ) $tag += '<span class="m-l-xs"><strong>보통</strong></span>';
			else if ($ccntpercent < 50) $tag += '<span class="text-muted m-l-xs">낮음</span>';
			$tag += '</td>';
			$tag += '<td>상관 계수 : ' + $ccnt.toFixed(4) + '</td>';
			$tag += '</tr>';		
		}
	});
	$tag += '</tbody>';
	$tag += '</table>';
	
	$('#' + obj).html($tag);

	opt.step = 7;
	fn_setChartAttribute(obj, 'fn_setCorRelationChart', data, opt);
}

/**
 * 키워드 순위
 * @param obj
 * @param chart
 * @param data
 * @param opt
 * @returns
 */
function fn_setKeywordRankChart(obj, data, opt) {
	var $tag = '', $start = opt.start, $end = opt.end;
	$(data).each(function(i, e){
		var $no = i + 1;
		var $cls = (i % 2 == 0) ? ' class="gubun"' : '';
		var $scls = (i < 3) ? ' three' : (i < 5) ? ' five' : ''
		if ($no >= $start && $no <= $end) {
			$tag += '<li' + $cls + '><span class="rank-num' + $scls + '">' + $no + '</span> <span class="rankKey">' + e.name + '</span>';
			if (opt.rank)
				$tag += '<span class="rank-updown">' + Number(Math.round(e.weight)).toLocaleString() + '</span>';
			else if (opt.rising) {
				var $weight = Number(Math.round(e.weight)), $count = Math.abs($weight).toLocaleString();
				var $css = ($weight > 0) ? ' text-info' : ($weight < 0) ? ' text-danger' : '';
				var $arrow = ($weight > 0) ? ' &nbsp;<i class="fas fa-arrow-up"></i>' : ($weight < 0) ? ' &nbsp;<i class="fas fa-arrow-down"></i>' : '';
				$tag += '<span class="rank-updown' + $css + '">' + ($count == 0 ? '-' : $count) + $arrow + '</span>';
			}
			$tag += '</li>';
		}
	});
	
	if ($end >= opt.limit) $('#' + obj + 'more').hide();
	$('#' + obj).append($tag);

	opt.step = 8;
	fn_setChartAttribute(obj, 'fn_setKeywordRankChart', data, opt);
}



function fn_setKeywordRankChartFloat(obj, data, opt) {
	console.log("float");
	var $tag = '', $start = opt.start, $end = opt.end;
	$(data).each(function(i, e){
		var $no = i + 1;
		var $cls = (i % 2 == 0) ? ' class="gubun"' : '';
		var $scls = (i < 3) ? ' three' : (i < 5) ? ' five' : ''
		if ($no >= $start && $no <= $end) {
			$tag += '<li' + $cls + '><span class="rank-num' + $scls + '">' + $no + '</span> <span class="rankKey">' + e.name + '</span>';
			if (opt.rank){
				$tag += '<span class="rank-updown">' + Number(e.weight).toLocaleString() + '</span>';
			}
			else if (opt.rising) {
				var $weight = Number(Math.round(e.weight)), $count = Math.abs($weight).toLocaleString();
				var $css = ($weight > 0) ? ' text-info' : ($weight < 0) ? ' text-danger' : '';
				var $arrow = ($weight > 0) ? ' &nbsp;<i class="fas fa-arrow-up"></i>' : ($weight < 0) ? ' &nbsp;<i class="fas fa-arrow-down"></i>' : '';
				$tag += '<span class="rank-updown' + $css + '">' + ($count == 0 ? '-' : $count) + $arrow + '</span>';
				
			}
			$tag += '</li>';
		}
	});
	
	if ($end >= opt.limit) $('#' + obj + 'more').hide();
	$('#' + obj).append($tag);

	opt.step = 8;
	fn_setChartAttribute(obj, 'fn_setKeywordRankChart', data, opt);
}

/**
 * 이슈 랭킹 추이
 * @param obj
 * @param data 
 * @param opt
 * @returns
 */
//아마 배열 형태 인대 
/*
 * {
 * 		data:'2021년1월'
 * 		data:[
 * 			{word : '??', rank : 1~10까지?출력건수?, weight : 84 or 0.84},
 * 			....
 * 		]
 * },....
 * 
 * */
function fn_setRankChart(obj, data, opt) {
	var $tag = '', $category = data.category, $list = data.list, $limit = (!fn_isNull(opt.limit) ? opt.limit : 20);
	$($category).each(function(i, e) {
		$tag += '<div class="issue-rank-box">';
		$tag += '<div class="rank-box-title"><h5>' + e + '</h5></div>';
		$tag += '<div class="rank-box-content">';
		$tag += '<ol class="num-st01">';
		
		if (fn_isNull($list)) $tag += '<li class="no-data">데이터가 없습니다.</li>';
		$($list[i]).each(function(j, m) {
			if (j < $limit) {
				var $cls = (j < 3) ? ' rank-top' : (j < 5) ? ' rank-middle' : '';
				$tag += '<li>';
				$tag += '<span class="rank-num' + $cls + '">' + m.rank + '</span>';
				$tag += '<span class="rank-keyword"><span class="ellipsis">' + fn_replaceAll(m.word, '_', ' ') + '</span></span><span class="rank-updown"> ' + (m.weight).toLocaleString() + '</span>';
				$tag += '</li>';
			}
		});
		
		$tag += '</ol></div></div>';
	});
	$('#' + obj).html($tag);

	opt.step = 9;
	fn_setChartAttribute(obj, 'fn_setRankChart', data, opt);
}

/**
 * set amchart multi line data grid
 * @param data - 차트 데이타 [ name - 항목명 (string), data - 값 (array) ]
 * @param x - x 축 타이틀 [ array ]
 * @param xSubj - x 축 제목
 * @param isReport - 보고서 여부
 * @returns
 */
function fn_amChart_multiLineGridChart(data, opt) {
	var $theadTag = '', $tbodyTag = '';
	var $category = opt.categoryArr;
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thead>'), $tbody = $('<tbody></tbody>');

	$theadTag += '<tr>';
	$(data).each(function(i, e) {
		$tbodyTag += '<tr>';
		$($category).each(function(j, m) {
			if (i == 0) {
				if (j == 0) $theadTag += '<th></th>';
				$theadTag += '<th>' + m + '</th>';
			}
			
			if (j == 0) $tbodyTag += '<td class="text-center">' + eval('e.' + opt.categoryX) + '</td>';
			$tbodyTag += '<td class="text-center">' + e[m] + '</td>';
		});
		$tbodyTag += '</tr>';
		
	});
	$theadTag += '</tr>';
	
	$tbody.append($tbodyTag);
	$thead.append($theadTag);
	$('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
}
/**
 * set stacked bar data grid
 * @param data
 * @param opt
 * @returns
 */
function fn_amChart_stackedBarGridChart(data, opt) {
	var $theadTag = '', $tbodyTag = '';
	var $category = opt.categoryArr, $categoryViewArr = opt.categoryViewArr;
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thead>'), $tbody = $('<tbody></tbody>');

	$theadTag += '<tr>';
	$(data).each(function(i, e) {
		$tbodyTag += '<tr>';
		$($category).each(function(j, m) {
			if (i == 0) {
				if (j == 0) $theadTag += '<th></th>';
				$theadTag += '<th>' + $categoryViewArr[j] + '</th>';
			}
			
			if (j == 0) $tbodyTag += '<td class="text-center">' + e[opt.categoryX] + '</td>';
			$tbodyTag += '<td class="text-center">' + e[m] + '</td>';
		});
		$tbodyTag += '</tr>';
		
	});
	$theadTag += '</tr>';
	
	$tbody.append($tbodyTag);
	$thead.append($theadTag);
	$('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
}
/**
 * set wordcloud data grid
 * @param data - 차트 데이타
 * 	treemap > [ id - 부모ID (string), name - 항목명 (string), value - 값 (int), parent - 부모ID명 (string) ]
 * 	treemap axis > [ name - 항목명 (string), value - 값 (int) ]
 * @returns
 */
function fn_amChart_wordCloudGridChart(data, opt) {
	var $id = new Array(), $name = new Array(), $data1 = new Array(), $data2 = new Array(), $theadTag = '', $tbodyTag = '';
	
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thead>'), $tbody = $('<tbody></tbody>');
	
	$(data).each(function(i, e) {
		$tbodyTag += '<tr><td>' + e.word + ' ( ' + e.value + ' )</td></tr>';
	});
	
	$tbody.append($tbodyTag);
	$thead.append($theadTag);
	$('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
}
/**
 * network & bubble chart
 * @param data
 * @param opt
 * @returns
 */
function fn_amChart_forceDirectedGridChart(data, opt) {
	var $theadTag = '', $tbodyTag = '';
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thaed>'), $tbody = $('<tbody></tbody>');
	var $data = data[0];

	// network chart
	if ($data.children) {
		$theadTag += '<tr><th>' + $data.name + '</th></tr>';
		$($data.children).each(function(i, e) {
			var $weight = Number(e.value).toFixed(2);
			$tbodyTag += '<tr><td>' + e.name + ' ( ' + $weight + ' )</td></tr>';
			
			$(e.children).each(function(j, m) {
				var $cWeight = Number(m.value).toFixed(2);
				$tbodyTag += '<tr><td>' + m.name + ' ( ' + $cWeight + ' )</td></tr>';
			});
		});
	}
	// bubble chart
	else {
		$(data).each(function(i, e) {
			$tbodyTag += '<tr><td>' + e.name + ' ( ' + e.value + ' )</td></tr>';
		});
	}
	$tbody.append($tbodyTag);
	$thead.append($theadTag);
	if ($data.children) $('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
	else $('#modalTableData').html($table.append($tbody)).slimScroll({ height : '400px' });
}
/**
 * set rank data grid
 * @param data - [ category - 헤더 제목 (string), list - 데이타set (array) ]
 * @param opt - [ limit - 리스트수 (int) ]
 * 		ex) 업무별 이슈 TOP 3 [ limit : 3 ]
 * 		ex) 이슈 랭킹 추이 [ limit : 10 ]
 * @param isReport - 보고서 여부
 * @returns
 */
function fn_setRankDataGridChart(data, opt) {
	var $category = data.category, $list = data.list;
	var $title = new Array(), $data = new Array();
	var $theadTag = '', $tbodyTag = '';
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thaed>'), $tbody = $('<tbody></tbody>');
	var $optLimit = fn_isNull(opt.limit) ? 3 : opt.limit;
	
	$theadTag = '<tr>';
	$($category).each(function(i, e) {
		if (i == 0) $theadTag += '<th></th>';
		$theadTag += '<th>' + e + '</th>';
	});
	$theadTag += '</tr>';
	
	for (var x = 0; x < $optLimit; x ++) {
		$tbodyTag = '<tr>';
		$($list).each(function(i, e) {
			if (i == 0) $tbodyTag += '<td class="text-center">' + (x + 1) + '</td>';
			if (fn_isNull(e)) {
				$tbodyTag += '<td></td>';
			} else {
				if (!fn_isNull(e[x])) $tbodyTag += '<td>' + e[x].word + ' (' + e[x].weight + ')</td>';
				else $tbodyTag += '<td></td>';
			}
		});
		$tbodyTag += '</tr>';
		$tbody.append($tbodyTag);
	}
	
	$thead.append($theadTag);
	$('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
}

/**
 * set amchart pie data grid
 * @param data - 차트 데이타 [ category - 항목명 (string), value - 값 (string) ]
 * @param category - dataName
 * @param value - dataValue
 * @param 
 * @returns
 */
function fn_amChart_PieGridChart(data, opt) {
	var $theadTag = '', $tbodyTag = '';
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thead>'), $tbody = $('<tbody></tbody>');

	$theadTag += '<tr>';
	$theadTag += '<th>항목</th>';
	$theadTag += '<th>값</th>';
	$theadTag += '</tr>';
	$(data).each(function(i, e) {
		$tbodyTag += '<tr>';
		$tbodyTag += '<td class="text-center">' + eval('e.' + opt.category) + '</td>';
		$tbodyTag += '<td class="text-center">' + eval('e.' + opt.value) + '</td>';
		$tbodyTag += '</tr>';
	});
	
	$tbody.append($tbodyTag);
	$thead.append($theadTag);
	$('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
}

/**
 * set amchart sortedBar data grid
 * @param data - 차트 데이타 [ category - 항목명 (string), value - 값 (string) ]
 * @param category - dataName
 * @param value - dataValue
 * @param 
 * @returns
 */
function fn_amChart_SortedBarGridChart(data, opt) {
	var $theadTag = '', $tbodyTag = '';
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thead>'), $tbody = $('<tbody></tbody>');

	$theadTag += '<tr>';
	$theadTag += '<th>항목</th>';
	$theadTag += '<th>값</th>';
	$theadTag += '</tr>';
	$(data).each(function(i, e) {
		$tbodyTag += '<tr>';
		$tbodyTag += '<td class="text-center">' + eval('e.' + opt.categoryX) + '</td>';
		$tbodyTag += '<td class="text-center">' + eval('e.' + opt.valueY) + '</td>';
		$tbodyTag += '</tr>';
	});
	
	$tbody.append($tbodyTag);
	$thead.append($theadTag);
	$('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
}

/**
 * set rank data grid
 * @param data - [ date - 날짜 (string), word - 키워드 (string), weight - 상승률 ]
 * @param opt
 * @param 
 * @returns
 */
function fn_setRisingKeywordRankGridChart(data, opt){
	
	var $theadTag = '', $tbodyTag = '';
	var $table = $('<table class="tbListA table-hover mb-0 mgt5"></table>');
	var $thead = $('<thead></thead>'), $tbody = $('<tbody></tbody>');

	$theadTag += '<tr>';
	$theadTag += '<th>날짜</th>';
	$theadTag += '<th>키워드</th>';
	$theadTag += '<th>상승률</th>';
	$theadTag += '</tr>';
	$(data).each(function(i, e) {
		$tbodyTag += '<tr>';
		$tbodyTag += '<td class="text-center">' + e.date + '</td>';
		$tbodyTag += '<td class="text-center">' + e.word + '</td>';
		$tbodyTag += '<td class="text-center">' + e.weight + ' %</td>';
		$tbodyTag += '</tr>';
	});
	
	$tbody.append($tbodyTag);
	$thead.append($theadTag);
	$('#modalTableData').html($table.append($thead).append($tbody)).slimScroll({ height : '400px' });
}

$(document).ready(function() {
	/* amcharts theme setting */
//	am4core.useTheme(am4themes_material);
//	am4core.useTheme(am4themes_animated);
});