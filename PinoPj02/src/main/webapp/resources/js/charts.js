//ajax호출함수
function fn_apiAjax(options) {
	var $url = options.url;
	var $type = (fn_isNull(options.type)) ? 'GET' : options.type;
	var $success = (!fn_isNull(options.success)) ? options.success : function(data) {
		if (options.debug == 1) console.log(data);
	};
	
	$.ajax({
		async : true,
		url : $url,
		type : $type,
		dataType : 'json',
		data : options.data, 
		success : $success
	}).fail(function(request, status, error) {	
		console.log(status + " error > " + error);
	});
}

function fn_isNull(varObj) {
	if ($.trim(varObj) == '' || varObj == null || varObj == undefined)
		return true;
	else
		return false;
}

function fn_setParam(data) {
	var $param = data
	
	return $.param($param);
}


//지도차트
function fn_setChart(result){
	var data = result.returnObject[0];
 	for(var i = 0; i<markerLatLng.length; i++){
 		//전국시 차트
 		if(markerLatLng[i].ename != 'summary'){
 	 		for(var j = 0; j < data.mapList.length; j++){
 	 			if(data.mapList[j].name == markerLatLng[i].name){
 	 				var $value = new Array();
 	 				
 	 				var tScoreMax = ['최대', Number(data.mapList[j].tScoreMax), 1];
 	 				$value.push(tScoreMax);
 	 				var tScoreAvg = ['평균', Number(data.mapList[j].tScoreAvg), 1];
 	 				$value.push(tScoreAvg);
 	 				var tScoreMin = ['최소', Number(data.mapList[j].tScoreMin), 1];
 	 				$value.push(tScoreMin);
 	 		 		Highcharts.chart(markerLatLng[i].ename, {

 	 	 			  chart: {
 	 	 			   type: 'variwide'
 	 	 			   ,backgroundColor: 'transparent'
 	 	 			   ,spacing: [0,0,0,0]
 	 	 			  },

 	 	 			  title: {
 	 	 			    text: ''
 	 	 			  },
 	 	 			  credits:{
 	 	 				enabled: false
 	 	 			  },
 	 	 			  exporting: { 
 	 	 				enabled: false 
 	 	 			  },
 	 	 			  plotOptions:{
 	 					series:{
 	 		 			  	colors : [ '#2f7ed8', '#0d233a', '#FF0000'],
 	 		 			  	//stroke-width : 0으로 주는 함수
 	 						borderWidth : 0
 	 					},
 	 					bar : {
 	 						borderWidth: 0
 	 					}
 	 				  },
 	 	 			  symbol : { 
 	 	 			    enabled: false
 	 	 			  },
 	 	 			  accessibility: {
 	 	 			    announceNewData: {
 	 	 			      enabled: false
 	 	 			    }
 	 	 			  },
 	 	 			  subtitle: {
 	 	 			    text: ''
 	 	 			  },
 	 	 			  tooltip: {
 	 	 			      enabled: false
 	 	 			    },
 	 	 			  xAxis: {
 	 	 			    type: 'category',
 	 	 			    crosshair: false,
 	 	 			   minPadding: 0,
 	 	 			  maxPadding: 0,
 	 	 			  tickWidth: 0,
 	 	 			  lineWidth: 0,
 	 	 			    visible:false,
 	 	 			   maxPadding:0,
 	 	 			     labels:{
 	 	 			      enabled: false
 	 	 			    }
 	 	 			  },
 	 	 			  
 	 	 			  yAxis: {
 	 	 				crosshair: false,
 	 	 			    padding : 0,
 	 	 			    margin : 0,
 	 	 			   visible:false,
 	 	 			    pointPadding: 0,
 	 	 			    borderWidth: 0,
 	 	 			   maxPadding:0,
 	 	 			    title: {
 	 	 			      text: ''
 	 	 			    },
 	 	 			    labels:{
 	 	 			      enabled: false
 	 	 			    }
 	 	 			  },

 	 	 			  caption: {
 	 	 			    text: ''
 	 	 			  },

 	 	 			  legend: {
 	 	 			    enabled: false
 	 	 			  },

 	 	 			  series: [{
 	 	 			    name: 'Labor Costs',
 	 	 			    data: $value,
 	 	 			    dataLabels: {
 	 	 			      enabled: false
 	 	 			    },
 	 	 			    tooltip: {
 	 	 			      enabled: false
 	 	 			    },
 	 	 			    colorByPoint: true
 	 	 			  }]

 	 	 			}); //차트엔드
 	 			}
 	 		}
 	 		
 		}else{
 			var $value = new Array();
 			
 			var tScoreMax = [data.sumList[0].name, Number(data.sumList[0].tScore), 1];
			$value.push(tScoreMax);
			var tScoreAvg = [data.sumList[1].name, Number(data.sumList[1].tScore), 1];
			$value.push(tScoreAvg);
			var tScoreMin = [data.sumList[2].name, Number(data.sumList[2].tScore), 1];
			$value.push(tScoreMin);
 			
	 		Highcharts.chart(markerLatLng[i].ename, {
	 			  chart: {
	 			    type: 'variwide',
	 			   //차트뒤에 div를 감춤
	 			   backgroundColor: 'transparent'
	 			   //div안에 차트 padding 먹이는것
	 			   ,spacing: [0,0,0,0]
	 			  },
				  //타이틀 없애기
	 			  title: {
	 			    text: ''
	 			  },
	 			  //크래딧 없애기
	 			  credits:{
	 				    enabled: false
	 			  },
	 			  //우측상단 메뉴버튼 감추기
	 			  exporting: { 
	 				  enabled: false 
	 			  },
	 			  plotOptions:{
						series:{
							//막대기 컬러 지정
			 			  	colors : [ '#2f7ed8', '#0d233a', '#FF0000'],
			 			  	//stroke-width : 0으로 주는 함수
							borderWidth : 0
						}
				  },
				  //
	 			  symbol : { 
	 			    enabled: false
	 			  },
	 			  //
// 	 			  accessibility: {
// 	 			    announceNewData: {
// 	 			      enabled: false
// 	 			    }
// 	 			  },
	 			  //서브타이틀감추기
	 			  subtitle: {
	 			    text: ''
	 			  },
	 			  //마우스 오버시 툴팁 감추기
	 			  tooltip: {
	 			      enabled: false
	 			    },
	 			  xAxis: {
		 			  type: 'category',
		 			  crosshair: false,
		 			  minPadding: 0,
		 			  maxPadding: 0,
		 			  tickWidth: 0,
		 			  lineWidth: 0,
//		 			  visible:false,
		 			  maxPadding:0
// 		 			  labels:{
// 		 			  	enabled: false
// 		 			  }
	 			  },
	 			  
	 			  yAxis: {
					crosshair: false,
	 			    padding : 0,
	 			    margin : 0,
	 			   visible:false,
	 			    pointPadding: 0,
	 			    borderWidth: 0,
	 			   maxPadding:0,
	 			    title: {
	 			      text: ''
	 			    },
	 			    labels:{
	 			      enabled: false
	 			    }
	 			  },
	 			  caption: {
	 			    text: ''
	 			  },

	 			  legend: {
	 			    enabled: false
	 			  },

	 			  series: [{
	 			    name: 'Labor Costs',
	 			    data: $value,
	 				shapes: [{
	 					strokeWidth: 0
	 				}],
	 			    dataLabels: {
	 			      enabled: false
	 			    },
	 			    tooltip: {
	 			      enabled: false
	 			    },
	 			    colorByPoint: true
	 			  }]

	 			});
 		}
 	}
}

//radrList 방사형 차트 데이터
//ntnaList 전국평균 데이터
function fn_setPolarChart(result){

	var data = result.returnObject[0];

	var $categories = new Array();
	var $radrListData = new Array();
	var $ntnaListData = new Array();
	
	for(var i = 0; i<data.ntnaList.length; i++){
		$categories.push(data.ntnaList[i].name);
		$ntnaListData.push(Number(data.ntnaList[i].tScore));
		
		for(var j = 0; j<data.radrList.length; j++){
			if(data.ntnaList[i].name == data.radrList[j].name) $radrListData.push(Number(data.radrList[j].tScore));
		}
	}
	
	Highcharts.chart('polarChartArea', {
		//차트를 polar형식으로 변환
	    chart: {
	        polar: true
	    },
		//타이틀감추기
	    title: {
	        text: ''
	    },
	    //그래프전체 사이즈
	    pane: {
	        size: '90%'
	    },
	    //x축설정
	    xAxis: {
	        categories: $categories,
	         tickmarkPlacement: 'on',
	         lineWidth: 0
	        },
	    //Y축설정
	    yAxis: {
	    	gridLineInterpolation: 'polygon',
	        lineWidth: 0,
	        min: 0,
	        labels:{
		 		enabled: false
		 	}
	    },
	    //마우스호버 이벤트 없애기 
	    plotOptions: {
			series: {
				enableMouseTracking: false
			}
		},
	    //호버시 툴팁감춤
	    tooltip: {
	         enabled: false
	    },
	    //하단카테고리감추기  
	    legend: {
		 	enabled: false
		},
		//크래딧 없애기
		credits:{
		  enabled: false
		},
		//우측상단 메뉴버튼 감추기
		exporting: { 
		  enabled: false 
		},
		series: [
		//방사형 차트 데이터
		{
			type:"line",
			opacity : 1,
			index : 1000,
			data: $radrListData
		}, 
		//전국평균 데이터
		{
			lineWidth:0,
			type:"polygon",
			opacity:0.6,
			color:"pink",
			index : 1,
			data: $ntnaListData
		}]
	});
}

function fn_setTreemapChart(result, str){

	var $color = ["red", "orange", "gray", "yellow", "green", "blue", "black", "navy", "purple"];
	var data = result.returnObject[0].right;
	var $div = 'treemapChartAreaRight';
	
	if(str == 'left') {
		data = result.returnObject[0].left;	
		$div = 'treemapChartAreaLeft';
	};
	
	var $treemapChartAreaData = new Array();
	
	for(var i = 0; i<data[0].tScoreList.length; i++){
		var $value = {name : data[0].tScoreList[i].label, value:data[0].tScoreList[i].tScore, color:$color[i]}
		$treemapChartAreaData.push($value);
	}
	
	console.log('$treemapChartAreaData', $treemapChartAreaData);
	
	Highcharts.chart('treemapChartArea', {
		//크래딧 없애기 오른쪽하단 하이차트 글씨
		credits:{
			enabled: false
		},
		//우측상단 메뉴버튼 감추기
		exporting: { 
			enabled: false 
		},
		plotOptions:{
			series:{
				borderWidth : 3
			}
		},
		title: {
			text: ''
		},
		series: [{
		    type: 'treemap',
		    layoutAlgorithm: 'squarified',
		    data: [{
				name: '도서관',
				value: 6 ,
				color : "red"
		    }, {
		      	name: '박물관',
		     	 value: 6 ,
		     	 color : "orange"
		    }, {
		   	   name: '미술관',
		   	   value: 4,
		   	   color : "gray"
		    }, {
		   	   name: '공연행사',
		   	   value: 3,
		   	   color : "yellow"
		    }, {
		  	    name: '시장',
		  	    value: 2,
		 	    color : "green"
		    }, {
		  	    name: '휴양림',
		    	value: 2,
		      	color : "blue"
		    }, {
		      	name: '낚시터',
		      	value: 1,
		      	color : "black"
		    }, {
		      	name: '관광지',
		      	value: 1,
		      	color : "navy"
		    }, {
		      	name: '문화축제',
		      	value: 1,
		      	color : "purple"
		    }]
		}]
	});
}