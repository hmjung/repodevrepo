var $_vChartVarArr = new Array(), $_vCurrentChartHtml = '<p class="text-pink"><strong>현재 시각화</strong></p>';
function fn_setVisualModalHtml(obj, _obj, cnt) {
	var $html = '';

	$_vChartVarArr = new Array();
	for (var i = 0; i < cnt; i ++) {
		$html += '<div class="col-xl-6">';
		$html += '<div id="' + _obj + ( i + 1 ) + '" data-chart-fn="" data-chart-data="" data-chart-opt="" style="height:300px;"></div>';
		$html += '<div class="text-center mgt10">';
		$html += '<button type="button" data-ref="' + obj + '" data-no="' + (i + 1) + '" class="btn4chgVis btn btn-dark waves-effect waves-light" data-dismiss="modal">변경</button>';
		$html += '</div>';
		$html += '</div>';
	}
	$('#_visualModalDiv').html($html);
}
// 1차원 차트 시각화 셋팅
function fn_getVisualChartView1(obj) {
	var $fnArr = ['fn_amChart_lineChart', 'fn_amChart_lineChart', 'fn_amChart_barChart', 'fn_amChart_SortedBarChart'];
	var _obj = '_vChart', cnt = 1, _pieData = new Array(), _data = new Array();
	var $chart = $('#' + obj);
	var fn = $chart.data('chart-fn');
	var data = JSON.parse($chart.data('chart-data'));
	var opt = JSON.parse($chart.data('chart-opt'));
	var wave = (opt.wave ? true : false);
	
	// set html tag
	fn_setVisualModalHtml(obj, _obj, $fnArr.length);
	
	$($fnArr).each(function(i, e) {
		if (fn == e) {
			if (i == 0 && !wave || i == 1 && wave || i == 2) {
				$('#' + _obj + (i + 1)).addClass('chartSelect').next().html($_vCurrentChartHtml);
			}
		}
		if (i == 1) opt.wave = true; else delete opt.wave;
		
		eval(e + '(\'' + _obj + (i + 1) + '\', data , opt)');
	});
}

// 2차원 차트 시각화 셋팅
function fn_getVisualChartView2(obj) {
	//var $fnArr = ['fn_amChart_multiLineChart', 'fn_amChart_multiLineChart', 'fn_amChart_stackedBarChart'];
	var $fnArr = ['fn_amChart_multiLineChart', 'fn_amChart_multiLineChart', 'fn_amChart_stackedBarChart', 'fn_amChart_stackedBarChart', 'fn_amChart_stackedBarChart', 'fn_amChart_stackedAreaChart', 'fn_amChart_stackedAreaChart'];
	var _obj = '_vChart', cnt = 1, _pieData = new Array(), _data = new Array();
	var $chart = $('#' + obj);
	var fn = $chart.data('chart-fn');
	var data = JSON.parse($chart.data('chart-data'));
	var opt = JSON.parse($chart.data('chart-opt'));
	var wave = (opt.wave ? true : false), stacked = (opt.stacked ? true : false), percent = (opt.percent ? true : false);
	
	// set html tag
	fn_setVisualModalHtml(obj, _obj, $fnArr.length);
	
	$($fnArr).each(function(i, e) {
		if (fn == e) {
			if ((i == 0 || i == 5) && !wave || (i == 1 || i == 6) && wave || i == 2 && !stacked || (i == 3 && stacked && !percent)|| i == 4 && percent) {
				$('#' + _obj + (i + 1)).addClass('chartSelect').next().html($_vCurrentChartHtml);
			}
		}
		
		if (i == 1 || i == 6) opt.wave = true; else delete opt.wave;
		if (i == 3 || i == 4) opt.stacked = true; else delete opt.stacked;
		if (i == 4) opt.percent = true; else delete opt.percent;
		
		eval(e + '(\'' + _obj + (i + 1) + '\', data , opt)');
	});
}

// 3차원 차트 시각화 셋팅
function fn_getVisualChartView3(obj) {
var $fnArr = ['fn_amChart_wordCloud', 'fn_amChart_treemap'];
var _obj = '_vChart', cnt = 1, _pieData = new Array(), _data = new Array();
var $chart = $('#' + obj);
var fn = $chart.data('chart-fn');
var data = JSON.parse($chart.data('chart-data'));
var opt = JSON.parse($chart.data('chart-opt'));
var wave = (opt.wave ? true : false), stacked = (opt.stacked ? true : false), percent = (opt.percent ? true : false);

// set html tag
fn_setVisualModalHtml(obj, _obj, $fnArr.length);

$($fnArr).each(function(i, e) {
	if (fn == e) {
		$('#' + _obj + (i + 1)).addClass('chartSelect').next().html($_vCurrentChartHtml);
	}
	
	eval(e + '(\'' + _obj + (i + 1) + '\', data , opt)');
});
}

//chart ( svg ) to convert canvas
function fn_setSvgToCanvas(target) {
	var $svg = $('#' + target).find('svg');
	$svg.each(function() {

		var canvas = document.createElement("canvas");
		canvas.className = "tempCanvas";

		var xml = (new XMLSerializer()).serializeToString(this);
		xml = xml.replace(/xmlns=\"http:\/\/www\.w3\/.org\/2000\/svg\"/, '');

		canvg(canvas, xml);
		$(canvas).insertAfter(this);
		$(this).attr('class', 'tempHide').hide();
	});
	setTimeout(function() {
		$('#' + target).find('.tempCanvas').remove();
		$('#' + target).find('.tempHide').show().removeClass('tempHide');
	}, 800);
}