/**
 * null check
 * 
 * @param varObj
 * @returns {Boolean}
 */
function fn_isNull(varObj) {
	if ($.trim(varObj) == '' || varObj == null || varObj == undefined)
		return true;
	else
		return false;
}

function fn_isMatch(source, target) {
	var bool = false;
	if (typeof source === 'object') {
		$(source).each(function(i, e) {
			if (target == e) bool = true
		});
	}
	else {
		if (source == target) bool = true;
	}
	return bool;
}
/**
 * input[type=text] 입력여부 확인
 * @param varFrm
 * @param varObj
 * @returns {Boolean}
 */
function fn_checkFill(varFrm, varObj) {
	return fn_checkFillProc(varFrm, varObj, _comInputTextReqMsg);
}

function fn_checkFillProc(varFrm, varObj, varMsg) {
	var $obj = $('#' + varObj);
	if(fn_isNull($obj.val())) {
		var label = varFrm.find( 'label[for=' + varObj + ']' );
		alert(label.text().replace(':', '') + varMsg);
		$obj.focus();
		return false;
	}
	return true;
}

/**
 * input[type=select] 선택여부 확인
 * @param varFrm
 * @param varObj
 * @returns {Boolean}
 */
function fn_checkSelect(varFrm, varObj) {
	var $obj = $('#' + varObj);
	if(fn_isNull($obj.val())) {
		var label = varFrm.find( 'label[for=' + varObj + ']' );
		alert(label.text().replace(':', '') + _comRadioMsg);
		$obj.focus();
		return false;
	}
	return true;
}

/**
 * 삭제 체크박스 체크여부 확인
 * @param varObj
 * @returns {Boolean}
 */
function fn_checkCboxChecked(varObj) {
	return fn_checkCboxCheckedProc(varObj, _comCboxCheckDelMsg);
}

function fn_checkCboxCheckedProc(varObj, varMsg) {
	if($('input[name=' + varObj + ']:checked').val() == undefined) {
		alert(varMsg);
		return false;
	}
	
	return true;
}

/**
 * 체크박스 체크여부 확인 [ 메시지 알림 및 return true or false ]
 * @param varFrm
 * @param varObj
 * @returns {Boolean}
 */
function fn_isCboxChecked(varFrm, varObj) {
	var $obj = $('#' + varObj);
	if($('input[name=' + varObj + ']:checked').val() == undefined) {
		var label = varFrm.find( 'label[for=' + varObj + ']' );
		alert(label.text().replace(':', '') + _comCBoxMsg);
		$obj.focus();
		return false;
	}
	return true;
}

/**
 * 체크박스 체크여부 확인 - 결과값만 리턴 [ return true or false ]
 * @param varFrm
 * @param varObj
 * @returns {Boolean}
 */
function fn_isCboxCheckedNoMsg(varFrm, varObj) {
	var $obj = $('#' + varObj);
	if($('input[name=' + varObj + ']:checked').val() == undefined) {
		return false;
	}
	return true;
}

/**
 * 체크박스 전체선택 / 전체해제
 */
function fn_checkAllCheckbox(varObj) {
	var varObjRel = varObj.attr('rel');
	$('input[name=' + varObjRel + ']').prop('checked', varObj.prop('checked'));
}

/**
 * 라디오 버튼 선택여부 확인
 * @param varObj
 * @returns {Boolean}
 */
function fn_isCheckedRadio(varFrm, varObj) {
	return fn_isCheckedRadioProc(varFrm, varObj, _comRadioMsg);
}

/**
 * 라디오 버튼 선택여부 확인
 * @param varFrm
 * @param varObj
 * @param varMsg
 * @returns {Boolean}
 */
function fn_isCheckedRadioProc(varFrm, varObj, varMsg) {
	if (fn_isNull($('input[name=' + varObj + ']:checked').val())) {
		var label = varFrm.find( 'label[for=' + varObj + ']' );
		alert(label.text().replace(':', '') + varMsg);
		return false;
	}
	
	return true;
}
/**
 * replace all text
 * @param originStr
 * @param searchStr
 * @param replaceStr
 * @returns
 */
function fn_replaceAll(originStr, searchStr, replaceStr) {
  return originStr.split(searchStr).join(replaceStr);
}

/**
 * convert html to code
 * @param obj
 * @returns
 */
function fn_convertHtmlToCode(obj) {
	obj = fn_replaceAll(obj, '\n', '');
	obj = fn_replaceAll(obj, '"', '&#34;');
	obj = fn_replaceAll(obj, '\'', '&#39;');
	return obj;
}

/**
 * convert code to html
 * @param obj
 * @returns
 */
function fn_convertCodeToHtml(obj) {
	obj = fn_replaceAll(obj, '&#39;', '\'');
	obj = fn_replaceAll(obj, '&#34;', '"');
	return obj;
}

/**
 * convert file size
 * @param fileSize
 * @returns
 */
function fn_convertFileSize(fileSize) {
	var $unitArr = [ 'Bytes', 'KB', 'MB', 'GB' ];
	var $unitIndex = 0
	while (fileSize > 900) {
		fileSize /= 1024;
		$unitIndex ++;
	}
	return (Math.round(fileSize * 100) / 100) + ' ' + $unitArr[$unitIndex];
}

/**
 * check file info - file size, file exts
 * @param options
 * @returns
 */
function fn_chkFileInfo(options) {
	var $fileName = options.fileName, $fileSize = options.fileSize;
	var $chkExts = options.exts, $chkSize = options.size, $limit = options.limit;
	var $regExp = new RegExp('\\.(' + $chkExts + ')$');
	if ($regExp.test($fileName)) {
		alert('첨부파일은 다음 확장자만 가능합니다.\n\n' + fn_replaceAll($chkExts, '|', ', '));
		return false;
	}
	
	if ($fileSize > $chkSize) {
		alert('첨부파일은 최대 ' + $limit + '까지 첨부 가능합니다.');
		return false;
	}
	return true;
}

/**
 * only numeric text allow
 * @param varObj
 * @param addKeyCodeArr [ 허용 가능한 문자열 ( array ) ]
 */
function fn_setNumericText(varObj, addKeyCodeArr) {
		
	var $obj = $('#' + varObj);
	
	// Allow: backspace, delete, tab, ESC, enter and .
	var $keyCodeArr = [8, 46, 9, 27, 13, 110, 190];
	
	// 사용자 추가 예외 처리 문자가 존재할 경우 해당 데이터 추가 셋팅
	if (!fn_isNull(addKeyCodeArr)) {
		var addKeyCodeArrLen = addKeyCodeArr.length;
		for (var i = 0; i < addKeyCodeArrLen; i ++) {
			var $temp = parseInt(addKeyCodeArr[i], 10);
			$keyCodeArr.push($temp);
		}
	}
	
	$obj.css('ime-mode', 'disabled');
	$(document).on('keydown', '#' + varObj, function (e) {
		if ($.inArray(e.keyCode, $keyCodeArr) !== -1 ||
				// Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
				// Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					// let it happen, don't do anything
					return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
	if (!fn_isNull($obj.attr('maxlength'))) {
		$(document).on('keyup', '#' + varObj, function() {
			if($obj.attr('maxlength') == $obj.val().length){
				$obj.next().focus();
			}
		});
	}
}

/**
 * add char - to left location
 * @param obj
 * @param len
 * @param padStr
 * @returns
 */
function fn_lpad(obj, len, padStr) {
  obj = obj + '';
  return obj.length >= len ? obj : new Array(len - obj.length + 1).join(padStr) + obj;
}

/**
 * add char - to right location
 * @param obj
 * @param len
 * @param padStr
 * @returns
 */
function fn_rpad(obj, len, padStr) {
  obj = obj + '';
  return obj.length >= len ? obj : obj + new Array(len - obj.length + 1).join(padStr);
}

/**
 * jquery ajax function
 * @param options
 * 					url - link url
 * 					data - parameter
 *          dataType - get data type [ json, odata ... ]
 *          type - method type [ GET, POST ]
 *          success - success function
 *          debug - debug
 * @returns
 */
function fn_ajax(options) {
	var $token = $("meta[name='_csrf']").attr("content");
	var $header = $("meta[name='_csrf_header']").attr("content");
	var $async = (fn_isNull(options.async)) ? false : true;
	var $type = (fn_isNull(options.type)) ? 'GET' : options.type;
	var $dataType = (fn_isNull(options.dataType)) ? 'script' : options.dataType;
	var $success = (!fn_isNull(options.success)) ? options.success : function(response) {
				if (options.debug == 1) console.log(response);
				//eval(response);
			};

	$.ajax({
		type : $type,
		url : options.url,
		async : $async,
		dataType : $dataType,
		data : options.data,
		beforeSend : function(xhr) {
			xhr.setRequestHeader('ajax', 'true');
			xhr.setRequestHeader($header, $token);
		},
		success : $success,
		error : function(e) {
			console.log(e);
		}
	});
}
/*
function fn_apiAjax(options) {
	var $token = $("meta[name='_csrf']").attr("content");
	var $header = $("meta[name='_csrf_header']").attr("content");
	var $url = options.url;
	var $type = (fn_isNull(options.type)) ? 'GET' : options.type;
	var $success = (!fn_isNull(options.success)) ? options.success : function(data) {
		if (options.debug == 1) console.log(data);
	};
	
	$.ajax({
		async : true,
		url : $url,
		type : $type,
		dataType : 'json',
		contentType: 'application/json',
		data : options.data,
		beforeSend : function(xhr) {
			xhr.setRequestHeader($header, $token);
		},
		success : $success
	}).fail(function(request, status, error) {	
		console.log(status + " error > " + error);
	});
}
*/

function fn_apiAjax(options) {
	//var $token = $("meta[name='_csrf']").attr("content");
	//var $header = $("meta[name='_csrf_header']").attr("content");
	var $url = options.url;
	var $type = (fn_isNull(options.type)) ? 'GET' : options.type;
	var $success = (!fn_isNull(options.success)) ? options.success : function(data) {
		if (options.debug == 1) console.log(data);
	};
	
	$.ajax({
		async : true,
		url : $url,
		type : $type,
		dataType : 'json',
		data : options.data, 
		success : $success
	}).fail(function(request, status, error) {	
		console.log(status + " error > " + error);
	});
}

function fn_jsonAjax(options) {
	var $token = $("meta[name='_csrf']").attr("content");
	var $header = $("meta[name='_csrf_header']").attr("content");
	var $url = options.url;
	var $type = (fn_isNull(options.type)) ? 'GET' : options.type;
	var $success = (!fn_isNull(options.success)) ? options.success : function(data) {
		if (options.debug == 1) console.log(data);
	};
	
	$.ajax({
		async : true,
		url : $url,
		type : $type,
		dataType : 'json',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		data : options.data,
		beforeSend : function(xhr) {
			xhr.setRequestHeader($header, $token);
		},
		success : $success
	}).fail(function(request, status, error) {	
		console.log(status + " error > " + error);
	});
}
/**
 * jquery ajax function with nimbleLoader
 * @param options
 * 					url - link url
 * 					data - parameter
 *          dataType - get data type [ json, odata ... ]
 *          type - method type [ GET, POST ]
 *          success - success function
 *          debug - debug
 * @returns
 */
function fn_nimbleAjax(options) {

	var $type = (fn_isNull(options.type)) ? 'GET' : options.type;
	var $dataType = (fn_isNull(options.dataType)) ? 'script' : options.dataType;
	var $success = (!fn_isNull(options.success)) ? options.success : function(response) {
				if (options.debug == 1) console.log(response);
				//eval(response);
			};

	$.ajax({
		type : $type,
		url : options.url,
		async : false,
		dataType : $dataType,
		data : options.data,
		beforeSend : function(request) {
			request.setRequestHeader('Ajax', 'true');
			$('.sCont').nimbleLoader('show', {
				position : 'fixed',
				loaderClass : 'loading_bar_body',
				hasBackground : true,
				zIndex : 999999,
				backgroundColor : '#34383e',
				backgroundOpacity : 0.7
			});
		},
		success : $success,
		complete : function() {
			$('.sCont').nimbleLoader('hide');
		},
		error : function(e) {
			console.log(e);
		}
	});
}

/**
 * 입력 폼 validator
 * @param frm
 * @param opt
 * 		id : 체크 엘리멘트 아이디명 [ array ]
 * 		type : [ null or 1 : , 2 : 이메일, 7 : 비밀번호 ]
 * 		min : 체크 엘리멘트 최소값 [ array ]
 * 		max : 체크 엘리멘트 최대값 [ array ]
 * 		equal : 비교 대상 엘리먼트 id [ array ]
 * 		req : 해당 엘리먼트가 체크되어있을 경우 데이터 체크 [ array ]
 * 		id, type 등의 array 타입은 값의 길이가 같아야 함.
 */
function fn_setValidator(frm, opt) {
	var $idArr = opt.id, $typeArr = opt.type
			, $minArr = opt.min, $maxArr = opt.max
			, $equalArr = opt.equal, $reqArr = opt.req;
	var $rules = [], $messages = [];
	var $obj, $objType, $objMin, $objMax, $objPwd, $objEqualTo, $objEmail, $objReq, $required;

	// 폼 초기화
	try { frm.validate().resetForm(); } catch (e) {}
	
	$.each($idArr, function(index, value) {
		$objType = (fn_isNull($typeArr)) ? 1 : $typeArr[index];
		$objPwd = ($objType == 7) ? true : false;
		$objEmail = ($objType == 2) ? true : false;
		
		$obj = frm.find('#' + value);
		$objMin = (fn_isNull($minArr)) ? 0 : $minArr[index];
		$objMax = (fn_isNull($maxArr) || $maxArr[index] == 0) ? false : $maxArr[index];
		$objEqualTo = (fn_isNull($equalArr)) ? false : $equalArr[index];
		$objReq = (fn_isNull($reqArr)) ? false : $reqArr[index];
		$required = (!$objEqualTo) ? true : (!$objReq) ? true : $objReq;

		$obj.rules('add'
		, {
				required : $required
				, pwdChk : $objPwd
				, equalTo : $objEqualTo
				, email : $objEmail
				, minlength : $objMin
				, maxlength : $objMax
				, messages : {
					required : fn_validatorCheckFill(frm, value)
				}
			}
		);
	});
}

function fn_validatorCheckFill(varFrm, varObj, varMsg) {
	var label = varFrm.find( 'label[for=' + varObj + ']' ).text();
	return fn_isNull(varMsg) ? label + _comInputTextReqMsg : label + varMsg;
}

/**
 * naver editor setting
 */
var oEditors = [];
function fn_setEditor(ctx, frm, obj) {

	nhn.husky.EZCreator.createInIFrame({
	    oAppRef: oEditors,
	    elPlaceHolder: $('#' + frm).find('#' + obj).attr('id'),
	    sSkinURI: ctx + "/naver/SmartEditor2Skin.html",
	    fCreator: "createSEditor2"
	});
}

/**
 * tinymce editor [ https://www.tinymce.com ]
 * @param lang - site language
 * @param obj - element id
 */
function fn_setTinyEditor(lang, obj) {
	var $headers = [], $fonts = [];
	for (var i = 1; i < 7; i ++) { $headers.push({ title : 'Header ' + i, format : 'h' + i }); }
	for (var i = 0; i < 17; i ++) {
		if (i % 2 == 0) $fonts.push({ title : ( i + 8 ) + 'pt', inline : 'span', styles : { 'font-size' : ( i + 8 ) + 'px' } });
	}
	
	tinymce.init({
		selector : '#' + obj,
		theme : 'modern',
		language : lang,
		menubar : false,
		entity_encoding : 'raw',
		valid_elements : '*[*]', 
		valid_children : '*[*]',
		extended_valid_elements : 'pre[*],script[*],style[*]',
    style_formats : [ { title : "Headers", items : $headers }, { title : "Font Sizes", items : $fonts } ],
		plugins : [ 'advlist autolink lists link charmap hr anchor pagebreak nonbreaking save table contextmenu directionality template paste textcolor colorpicker textpattern' ],
    toolbar : 'styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor hilitecolor | link custom_image | table custom_close',
    table_default_styles : { width : '100%' },
    forced_root_block : 'div',
    keep_styles : false,
    setup : function(editor) {
    		editor.on('PostProcess', function(edt) {
    			edt.content = edt.content.replace(/(<div><\/div>)/gi, '<br>');
    		}),
	    	editor.addButton('custom_image', {
	  			title : 'Insert image',
				icon : 'image',
				onclick : function() {
					//window.open(ctx + '/tinymce/photo_uploader/photo_uploader.html?obj=' + obj + '','tinymcePop','width=400,height=350');
					window.open('/photoUploader.do?cmnx=1&obj=' + obj + '','tinymcePop','width=400,height=350');
				}
	    	}),
	    	editor.addButton('custom_close', {
	    		title : 'CLOSE',
	    		text : '닫기',
	    		classes : 'closeBtn',
	    		onclick : function() {
	    			editor.remove();
	    		}
	    	});
    }
	});
}
/**
 * 바이트 문자 입력가능 문자수 체크
 * @param id : tag id 
 * @param title : tag title
 * @param max : 최대 입력가능 수 (byte)
 * @returns {Boolean}
 */
function fn_checkMaxLength(id, title, max){
     var obj = $("#"+id);
     if(max == null) {
    	 max = obj.attr("maxLength") != null ? obj.attr("maxLength") : 1000;
     }
     
     if(Number(byteCheck(obj)) > Number(maxLength)) {
         alert(title + "이(가) 입력가능문자수를 초과하였습니다.\n(영문, 숫자, 일반 특수문자 : " + max + " / 한글, 한자, 기타 특수문자 : " + parseInt(max/2, 10) + ").");
         obj.focus();
         return false;
     } else {
         return true;
    }
}
 
/**
 * 바이트수 반환  
 * 
 * @param el : tag jquery object
 * @returns {Number}
 */
function byteCheck(el){
    var codeByte = 0;
    for (var idx = 0; idx < el.val().length; idx++) {
        var oneChar = escape(el.val().charAt(idx));
        if ( oneChar.length == 1 ) {
            codeByte ++;
        } else if (oneChar.indexOf("%u") != -1) {
            codeByte += 2;
        } else if (oneChar.indexOf("%") != -1) {
            codeByte ++;
        }
    }
    return codeByte;
}
/**
 * set paging with bootstrap [ https://esimakin.github.io/twbs-pagination/ ]
 * @param options
 * jquery paging 함수
 * 일반적인 페이지 새로고침 페이징일 경우 아래처럼 기본값만으로 사용
 * 	[ ex. {obj : $pagingId, url : $url, idx : '${cmnx}', total : 전체페이지수, visible : 페이지에 보여질 리스트수} ]
 * ajax
 * 최초 로드시 onPageClick 옵션을 무조건 호출.
 * $isLoad = true 일 경우 최초 페이지 로드시 해당 옵션 사용안함.
 * ajax 사용할시 최초 페이징 함수 호출 후 $isLoad = false; 셋팅 해줘야 함.
 */
var $isLoad = true;
function fn_setPaging(options) {
	var $obj = options.obj;
	var $idx = options.idx;
	var $startPage = (fn_isNull(options.start)) ? 1 : options.start;
	var $totalPage = (fn_isNull(options.total)) ? 10 : options.total;
	var $visiblePage = (fn_isNull(options.visible)) ? 10 : options.visible;
	var $params = (fn_isNull(options.param)) ? '' : options.param;
	var $page = (fn_isNull(options.pageName)) ? 'page' : options.pageName;
	var $url = (fn_isNull(options.url)) ? '' : options.url + '&' + $page + '={{page}}' + $params;
	var $action = (fn_isNull(options.action)) ? '' : options.action + $params;
	
	$('#' + $obj).addClass('pagination-sm alignC');
	$('#' + $obj).twbsPagination({
		//startPage: new Number($startPage),
	  totalPages: $totalPage,
	  visiblePages: $visiblePage ,
	  href: $url,
	  onPageClick: function (event, page) {
	  	var $ajaxAction = $page == 'page' ? $action + '&page=' + page : $action + '&' + $page + '=' + page; 
	  	if (!$isLoad) $.get($ajaxAction, function(response) { eval(response); });
	  }
	});
}

var $isApiLoad = true;
function fn_setAPIPaging(options) {
	var $obj = options.obj;
	var $idx = options.idx;
	var $totalPage = (fn_isNull(options.total)) ? 10 : options.total;
	var $visiblePage = (fn_isNull(options.visible)) ? 10 : options.visible;
	var $params = (fn_isNull(options.param)) ? '' : options.param;
	var $page = (fn_isNull(options.pageName)) ? 'page' : options.pageName;
	var $url = (fn_isNull(options.url)) ? '' : fn_replaceAll(options.url, '_page_', '{{page}}');
	var $fn = options.fn;
	
	$('#' + $obj).addClass('pagination-sm alignC');
	$('#' + $obj).twbsPagination({
	  totalPages: $totalPage,
	  visiblePages: $visiblePage ,
	  //href: $url,
	  onPageClick: function (event, page) {
	  	if (!$isApiLoad) {
	  		eval($fn + '(' + page + ');');
	  		$isApiLoad = true;
	  	}
	  }
	});
}

/**
 * [ http://xdsoft.net/jqplugins/datetimepicker/ ]
 * set jqeury datetimepicker
 * @param opt
 * 	obj - 항목 아이디명
 * 	step - 시간 단위 [ 30 > 09:30, 10:00, 10:30 ... ]
 * 	time - 시간선택 사용여부
 * 	date - 날짜선택 사용여부
 * 	minDate - 선택가능 최저 날짜 [ 0 : 오늘, 2010.01.01 : 이후 선택 가능 ]
 * 	maxDate - 선택가능 최대 날짜 [ 0 : 오늘, 2020.01.01 : 까지 선택 가능 ]
 * 	minTime - 선택가능 최저 시간 [ 0 : 지금, 09:00 : 09시 이후 선택 가능 ]
 * 	maxTime - 선택가능 최대 시간 [ 0 : 지금, 18:00 : 18시 이전 선택 가능 ]
 *  period - 기간 설정 [ 1 : 시작, 2 : 끝 ]
 *  ref - 기간 설정 비교 대상 아이디명
 */
function fn_setDateTimePicker(options) {
	var $allowTimes = [];
	
	for (var i = 0; i < 24; i ++) {
		var $hh = i < 10 ? '0' + i : i;
		
		$allowTimes.push($hh + ':00');
		$allowTimes.push($hh + ':30');
	}
	
	var $obj = options.obj, $mask
			, $format = (fn_isNull(options.format)) ? 'Y-m-d' : options.format
			, $step = (fn_isNull(options.step)) ? 60 : options.step
			, $time = (fn_isNull(options.time)) ? true : options.time
			, $date = (fn_isNull(options.date)) ? true : options.date
			, $minDate = (fn_isNull(options.minDate)) ? false : options.minDate
			, $maxDate = (fn_isNull(options.maxDate)) ? false : options.maxDate
			, $minTime = (fn_isNull(options.minTime)) ? false : options.minTime
			, $maxTime = (fn_isNull(options.maxTime)) ? false : options.maxTime
			, $period = (fn_isNull(options.period)) ? false : options.period
			, $ref = (fn_isNull(options.ref)) ? false : options.ref;
	
	if ($format == 'Y-m-d') $mask = '9999-19-39';
	else if ($format == 'Y-m') $mask = '9999-19';
	else $mask = '29:59';
	
	$('#' + $obj).datetimepicker({
		format : $format
		, formatDate : $format
		//, mask : $mask
		, allowTimes : $allowTimes
		, step : $step
		, timepicker : $time
		, datepicker : $date
		, minDate : $minDate
		, maxDate : $maxDate
		, minTime : $minTime
		, maxTime : $maxTime
	});

	// period == 1 -> 시작일 셋팅으로 최대값 설정
	if ($period == 1) $('#' + $obj).datetimepicker('setOptions', { onShow : function (ct) { this.setOptions({ maxDate : jQuery('#' + $ref).val() ? jQuery('#' + $ref).val() : false }); } });
	// period == 2 -> 종료일 셋팅으로 최소값 설정
	else if ($period == 2) $('#' + $obj).datetimepicker('setOptions', { onShow : function (ct) { this.setOptions({ minDate : jQuery('#' + $ref).val() ? jQuery('#' + $ref).val() : false }); } });
	
	//$.datetimepicker.setLocale('ko');
	$('#' + $obj).on('keydown', function() {return false;});
}

function fn_setDateTimePickerShowOption ($period, $ref) {
	// period == 0 -> 시작일 셋팅으로 최대값 설정
	if ($period == 0) return function (ct) { maxDate : $('#' + $ref).val() ? $('#' + $ref).val() : false };
	// period == 1 -> 종료일 셋팅으로 최소값 설정
	else if ($period == 1) return function (ct) { minDate : $('#' + $ref).val() ? $('#' + $ref).val() : false };
	else return false;
}

/**
 * 달력 셋팅 [ https://fullcalendar.io ]
 * @param obj
 */
var isCalCheckbox = false;
function fn_setCalendar(obj, defaultDate, mng) {
	$('#' + obj).fullCalendar({
		header: {
			left: '', //'prev,next,nextYear today',
			center: '', //'title',
			right: '', //'month,basicWeek,basicDay'
		},
		defaultDate: defaultDate,
		navLinks: false, // can click day/week names to navigate views
		editable: false,
		eventLimit: false, // allow "more" link when too many events
		displayEventTime : false,
		eventClick: function(calEvent, jsEvent, view) {
			if (!isCalCheckbox) {
				var $url = calEvent.url;
				
				if (!fn_isNull($url)) {
					fn_ajax( { url : $url } );
					$('#calendar-info-popup').modal();
				}
				
				isCalCheckbox = false;
				return false;
			}
			else 
				isCalCheckbox = false;
		},
		eventRender: function (event, element) {
			var $cbox = mng ? '<input type="checkbox" name="listCboxs" id="listCboxs' + event.id + '" value="' + event.id + '" /> ' : '';
			var $delBtn = '<span class="btnTypeO">삭제</span>';
			element.find('span.fc-title').html($cbox + element.find('span.fc-title').text());
			element.find('#listCboxs' + event.id).click(function() { isCalCheckbox = true; });
		},
	});
}

/**
 * 다음 주소 api [ http://postcode.map.daum.net/guide ]
 * @param zipcode - 우편번호 ID
 * @param addr1 - 기본주소 ID
 * @param addr2 - 상세주소 ID
 */
//본 예제에서는 도로명 주소 표기 방식에 대한 법령에 따라, 내려오는 데이터를 조합하여 올바른 주소를 구성하는 방법을 설명합니다.
function fn_setPost(zipcode, addr1, addr2, sido, gugun) {
  new daum.Postcode({
    oncomplete: function(data) {
        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

        // 각 주소의 노출 규칙에 따라 주소를 조합한다.
        // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
        var fullAddr = ''; // 최종 주소 변수
        var extraAddr = ''; // 조합형 주소 변수
        var sidoAddr = data.sido; // 시도명칭
        var gugunAddr = data.sigungu; // 구군명칭

        // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
        if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
            fullAddr = data.roadAddress;

        } else { // 사용자가 지번 주소를 선택했을 경우(J)
            fullAddr = data.jibunAddress;
        }

        // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
        if(data.userSelectedType === 'R'){
            //법정동명이 있을 경우 추가한다.
            if(data.bname !== ''){
                extraAddr += data.bname;
            }
            // 건물명이 있을 경우 추가한다.
            if(data.buildingName !== ''){
                extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }
            // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
            fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
        }

        // 우편번호와 주소 정보를 해당 필드에 넣는다.
        document.getElementById(zipcode).value = data.zonecode; //5자리 새우편번호 사용
        document.getElementById(addr1).value = fullAddr;
        if (!fn_isNull(sido)) document.getElementById(sido).value = sidoAddr;
        if (!fn_isNull(gugun)) document.getElementById(gugun).value = gugunAddr;

        // 커서를 상세주소 필드로 이동한다.
        document.getElementById(addr2).focus();
        
        $('#' + zipcode).prop('readonly', true);
        $('#' + addr1).prop('readonly', true);
    }
}).open();
}

//base64 encode
function fn_setB64encode(varFrm, source) {

	if (varFrm.find('input[name=b64elms]').length == 0) {
		varFrm.append($('<input/>', {
			name : 'b64elms'
			, class : 'sr-only'
		}));
	}
	$(source).each ( function (i) {
		var $temp;
		var $obj = $('#' + source[i]);
		var $objVal = $obj.val();
		$obj.val('').prop('disabled', true);
		
		try { $temp = b64EncodeUnicode($objVal); } catch (e) { $temp = Base64.encode($objVal); }
		varFrm.find('input[name=_' + source[i] + '_]').remove();
		varFrm.append($('<input/>', {
			name : '_' + source[i] + '_'
			, value : $temp
			, class : 'sr-only'
		}));

		// 인코딩 엘리멘트 항목 추가
		var $b64elm = varFrm.find('input[name=b64elms]');
		if (fn_isNull($b64elm.val())) $b64elm.val(source[i]);
		else $b64elm.val($b64elm.val() + '|' + source[i]);
	});
}
/**
 * open top notification
 * @param opt [ 선택 옵션 ]
 *  -> title - 제목
 *  -> msg - 알림창 내용
 * 	-> type - success, info, warning, error
 * @returns
 */
function fn_openNoti(opt) {
	toastr.options = {
	    positionClass: 'toast-top-center',
	    timeOut : 1000
	};
	toastr[opt.type](opt.msg, opt.title);
}

/**
 * open sweet alert
 * @param opt [ 선택 옵션 ]
 * 	-> title - 제목
 * 	-> msg - 알림창 내용
 * 	-> type - success, info, warning, error
 * 	-> okMsg - 확인 버튼 텍스트
 * 	-> noMsg - 취소 버튼 텍스트
 * 	-> success - 확인 버튼 클릭 이벤트 함수
 * @returns
 */
function fn_openSweetAlert(opt) {
  swal({
      title: opt.title,
      text: opt.msg,
      type: opt.type,
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: fn_isNull(opt.okMsg) ? '확인' : opt.okMsg,
      cancelButtonText: fn_isNull(opt.noMsg) ? '취소' : opt.noMsg,
  }, opt.success);
}
//입력한 Date 객체 날짜 반환 (YYYYMMDD)
function DateForm(d, sp) { // d: Date 객체
	var yy = d.getFullYear();
	var mm = d.getMonth() + 1;
	var dd = d.getDate();
	
	if (!sp) sp = "";
	if (mm < 10)
		mm = '0' + mm;
	if (dd < 10)
		dd = '0' + dd;
	
	return yy + sp + mm + sp + dd;
}
//문자열 날짜를 Date 객체로 변환
function DStr2Date(dstr) { // dstr: 날짜 형식의 문자열 ex. 20170101 or 2017-01-01
	var yy, mm, dd;
	
	dstr = dstr.replace(/[^0-9]+/gi, "");
	
	if (dstr.length<8)
		return null;
	
	yy = dstr.substr(0, 4);
	mm = dstr.substr(4, 2).replace(/^[0]+/gi, "");
	dd = dstr.substr(6, 2).replace(/^[0]+/gi, "");
	
	return new Date(parseInt(yy), parseInt(mm)-1, parseInt(dd));
}
//날짜 차이 (days)
function DateDiff(d1, d2) { // d1, d2: 날짜 형식의 문자열 ex. 20170101 or 2017-01-01
	var dt1, dt2;
	var yy, mm, dd;
	
	d1 = d1.replace(/[^0-9]+/gi, "");
	d2 = d2.replace(/[^0-9]+/gi, "");
	
	if (d1.length<8 || d2.length<8)
		return null;

	dt1 = DStr2Date(d1);
	dt2 = DStr2Date(d2);
	
	var dt = dt2.getTime()- dt1.getTime();
	
	return Math.floor(dt/(1000*60*60*24));
}

function MonthDiff(d1, d2, p) { // d1, d2: 날짜 형식의 문자열 ex. 20170101 or 2017-01-01
	var dt1, dt2;
	var yy, mm, dd;
	
	d1 = d1.replace(/[^0-9]+/gi, "");
	d2 = d2.replace(/[^0-9]+/gi, "");
	
	if (d1.length<8 || d2.length<8)
		return null;

	dt1 = DStr2Date(d1);
	dt2 = DStr2Date(d2);
	
	var dt = dt2.getTime()- dt1.getTime();
	
	return Math.floor(dt/((1000*60*60*24) * 30 * p));
}
//입력한 일 수 이전 날짜 반환
function BeforeDate(days, d) { // days: 일 수 (전), d: Date 객체
	var t_d = d;
	
	if (t_d == null)
		t_d = new Date();
	
	return DateForm(new Date( t_d.getTime() - (days)*1000*60*60*24 ), "-");
}
//입력한 달 수 이전 날짜 반환
function BeforeMonth(months, d) { // month: 개월 수 (전), d: Date 객체
	var t_d = d;
	
	if (t_d == null)
		t_d = new Date();
	
	return DateForm(new Date( t_d.getFullYear(), t_d.getMonth() - months, t_d.getDate()), "-");
}
//입력한 날짜에 해당일수 전(-)/후(+) 날짜 반환
function AddDayDate(dstr, days) { // dstr:YYYYMMDD ex)20170101, days: (+/-)일 수 (전/후)
	dstr = dstr.replace(/[^0-9]+/gi, "");
	
	if (dstr.length<8)
		return null;
	
	var dt = DStr2Date(dstr);
	
	return DateForm(new Date(dt.getTime() + days*1000*60*60*24));
}
//입력한 날짜에 해당 개월 전(-)/후(+) 날짜 반환
function AddMonthDate(dstr, months) { // dstr:YYYYMMDD ex)20170101, days: (+/-)일 수 (전/후)
	dstr = dstr.replace(/[^0-9]+/gi, "");
	
	var dt = new Date();
	if (dstr.length==6) {
		dt = DStr2Date(dstr+"01");
	} else if (dstr.length==8) {
		dt = DStr2Date(dstr);
	} else {
		return null;
	}
	
	return DateForm(new Date( dt.getFullYear(), dt.getMonth() + months, dt.getDate() )).substr(0, 6);
}
function DateFormView(d) { // d: Date 객체
	var yy = d.getFullYear();
	var mm = d.getMonth() + 1;
	var dd = d.getDate();
		
	if (mm < 10)
		mm = '0' + mm;
	if (dd < 10)
		dd = '0' + dd;
	
	return yy +'년 '+ mm +'월 '+ dd+'일';
}
function DateTimeFormView(d) { // d: string
	var yy = d.substring(0, 4);
	var mm = d.substring(4, 6);
	var dd = d.substring(6, 8);
	var hh = d.substring(8, 10);
	var mi = d.substring(10, 12);
	var ss = d.substring(12, 14);
		
	if (mm < 10)
		mm = '0' + mm;
	if (dd < 10)
		dd = '0' + dd;
	
	return yy +'-'+ mm +'-'+ dd+' ' + hh + ':' + mi + ':' + ss
}
function fn_setTimeFormat(t) {
	if (t.length > 4 && t.indexOf(':') > -1) return t;
	return t.substr(0, 2) + ':' + t.substr(2);
}

function fn_setDatepicker(obj, opt){ // obj : html tag id or class, opt : datepicker option
	$(obj).datepicker(opt);
}
