$(function($){
	/* gnb */
	var depth2Wrap = $(".depth2Wrap");
	$("#gnb > li > span > a").mouseenter(function(){
		$(this).parents("li").addClass("on").siblings().removeClass("on");
		depth2Wrap.hide();
		$(this).parent().next(".depth2Wrap").show();
	});

	$("#gnb > li").mouseleave(function(){
		$(this).removeClass("on");
		depth2Wrap.hide();
	});

	var max = $(".depth2Wrap").length + 1;
	for(var i = 0; i < max; i++){
		var depth2sub = $(".depth2Wrap"+".sub0"+i+" .depth2 > li:lt(5)");
		depth2sub.addClass("mgt0");
	}


	/* 전체메뉴 */
	$(".btnAllMenu").click(function(){
		$(".allMenu").slideToggle(800, 'easeOutExpo');
		return false;
	});

	$(".allMenu .btnClose").click(function(){
		$(".allMenu").slideUp(800, 'easeOutExpo');
		return false;
	});

/* network */
	$(document).on('click', '.btnNetwork', function(){
		if($(".btnNetwork span").text() == '열기'){
			$(".btnNetwork span").text('닫기');
			$(this).removeClass("open");
			$("#network").slideDown(1000, 'easeOutExpo');
			var scrollHeight = $(document).height();
			$('html, body').animate({scrollTop:scrollHeight}, 1000, 'easeOutExpo');
			return false;
		}else{
			$(".btnNetwork span").text('열기');
			$(this).addClass("open");
			$("#network").slideUp(1000, 'easeOutExpo');
			return false;
		}
	});


	/* top 버튼 */
	$("#footer .btnTop").on('click', function(){
		$('html, body').animate({scrollTop:0}, 300);
		return false;
	});

	/* footer mark */
	$('.markList').jCarouselLite({
		btnNext: ".btnBnUp",
		btnPrev: ".btnBnDown",
		visible: 1,
		speed:400,
		vertical: true
	});

	/* tab */
	$(".tab li a").on('click', function(e){
		e.preventDefault();
		var href= $(this).attr("href");
		if ($(this).parents("li").hasClass("on") == false) {
			$(href).show().siblings(".tabCont").hide();
			$(this).parents("li").addClass("on").siblings("li").removeClass("on");
			$(this).find("img").each(on).parents().siblings("li").find("img").each(off);
		}
		return false;
	});

	/* main */
	//Visual
	$.fn.cycle.defaults.timeout = 5000;
	$(".visualBn .visual").cycle({
		fx:"cover",
		speed:"1000",
		timeout:"3000",
		pager:".visualBtn .bnPagin"
	});

	$('.visualBn .visualBtn .btnPause').toggle(
		function(){
			$(this).find('img').attr('src','../../images/main/btn_play.png');
			$(".visualBn .visual").cycle("pause");
			$(this).find('img').attr('alt','재생');
		},
		function(){
			$(this).find('img').attr('src','../../images/main/btn_pause.png');
			$(".visualBn .visual").cycle("resume");
			$(this).find('img').attr('alt','정지');
		}
	);

	//popup zone
	$.fn.cycle.defaults.timeout = 5000;
	$(".popupzone .columnImg").cycle({
		fx:"scrollLeft",
		speed:"400",
		timeout:"5000",
		pager:".columnBtn .bnPagin"
	});

	$('.columnBtn .btnPause').toggle(
		function(){
			$(this).find('img').attr('src','../../images/main/btn_play.png');
			$(".popupzone .columnImg").cycle("pause");
			$(this).find('img').attr('alt','재생');
		},
		function(){
			$(this).find('img').attr('src','../../images/main/btn_pause.png');
			$(".popupzone .columnImg").cycle("resume");
			$(this).find('img').attr('alt','정지');
		}
	);

	// 배너존 
	$('.Playlist').jCarouselLite({
		btnNext: $(".btnBnLeft"),
		btnPrev: $(".btnBnRight"),
		btnStop: $(".btnBnPause"),
		visible: 5,
		speed: 1500,
		auto: 1,
		scroll: 1,
		circular: true,
		width:1000
		}
	);


});