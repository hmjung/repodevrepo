function fnMoveMenu(is){
	var url = "/"+$(is).attr("menu");
	var rel = $(is).attr("rel");
	
	if(rel == "2207" || rel == "2208") {
		fnMovePop(rel);
	} else {
		var menuNm = $(is).attr("title");
		document.frm.menu_id.value = rel;
		document.frm.menu_nm.value = menuNm;
		document.frm.action = url;
	   	document.frm.submit();
	}
}

function fnMovePop(rel) {
	var vsWidth=900;
	var vsHeight=600;
	var viPosTop  =  (screen.height -vsHeight)/2;   
	var viPosLeft =  (screen.width - vsWidth)/2;
	var openOption = "top="+viPosTop+",left="+viPosLeft+",width="+vsWidth+",height="+vsHeight+",status='', titlebar=no, scrollbars=yes,menubar=no,toolbar=no,location=no,resizable=yes";
	
	var url = "";
	if(rel == "2207" ) {
		url = "http://100.1.28.150:20808/";
	} else {
		url = "http://100.1.28.162:23808/";
	}
	
	window.open(url,'',openOption);
}
