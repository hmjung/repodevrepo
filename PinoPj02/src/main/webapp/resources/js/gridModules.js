
/**
 * set data grid [ line, bar, ... ]
 * @param data - 차트 데이타 [ name - 항목명 (string), data - 값 (array) ]
 * @param x - x 축 타이틀 [ array ]
 * @param xSubj - x 축 제목
 * @param isReport - 보고서 여부
 * @returns
 */
function fn_setDataGridChart1(data, opt, widget) {

	var $categoryX = opt.categoryX, $valueY = opt.valueY;
	var $table = $('<table class="table m-0 table-colored-bordered table-bordered-dark table-hover"></table>');
	var $thead = $('<thead class="center"></thead>'), $tbody = $('<tbody class="center"></tbody>');
	var $theadTag = '', $tbodyTag = '';
	
	$theadTag += '<tr><th>구분</th><th>데이터</th></tr>';
	$(data).each(function(i, e) {
		$tbodyTag += '<tr>';
		$tbodyTag += '<td>' + e[$categoryX] + '</td>';
		$tbodyTag += '<td>' + e[$valueY] + '</td>';
		$tbodyTag += '</tr>';
	});
	$theadTag += '</tr>'
	
	$thead.append($theadTag);
	$tbody.append($tbodyTag);
	if (!widget) $('#modalTableData').html($thead).append($tbody);
	else return $table.append($thead).append($tbody);
}

/**
 * set data grid [ line, bar, ... ]
 * @param data - 차트 데이타 [ name - 항목명 (string), data - 값 (array) ]
 * @param x - x 축 타이틀 [ array ]
 * @param xSubj - x 축 제목
 * @param isReport - 보고서 여부
 * @returns
 */
function fn_setDataGridChart2(data, opt, widget) {

	var $categoryX = opt.categoryX, $categoryArr = opt.categoryArr, $categoryViewArr = opt.categoryViewArr;
	var $table = $('<table class="table m-0 table-colored-bordered table-bordered-dark table-hover"></table>');
	var $thead = $('<thead class="center"></thead>'), $tbody = $('<tbody class="center"></tbody>');
	var $theadTag = '', $tbodyTag = '';
	
	$theadTag += '<tr>';
	$(data).each(function(i, e) {
		
		$tbodyTag += '<tr>';
		$tbodyTag += '<td>' + e[$categoryX] + '</td>';
		$($categoryArr).each(function(j, m) {
			if (i == 0) {
				if (j == 0) $theadTag += '<th>날짜</th>';
				$theadTag += '<th>' + $categoryViewArr[j] + '</th>';
			}
			$tbodyTag += '<td>' + eval('e[m]') + '</td>';
		});
		$tbodyTag += '</tr>';
	});
	$theadTag += '</tr>'
	
	$thead.append($theadTag);
	$tbody.append($tbodyTag);
	if (!widget) $('#modalTableData').html($thead).append($tbody);
	else return $table.append($thead).append($tbody);
}

/**
 * set data grid [ treemap, wordcloud ]
 * @param data - 차트 데이타 [ name - 항목명 (string), children : [ name ; name, value : value ] ]
 * @param opt
 * @param widget
 * @returns
 */
function fn_setDataGridChart3(data, opt, widget) {

	var $category = new Array();
	var $table = $('<table class="table m-0 table-colored-bordered table-bordered-dark table-hover"></table>');
	var $thead = $('<thead class="center"></thead>'), $tbody = $('<tbody class="center"></tbody>');
	var $theadTag = '', $tbodyTag = '';
	
	$theadTag += '<tr>';

	$(data).each(function(i, e) {
		$category.push(e[opt.name]);
		$theadTag += '<th>' + e[opt.name] + '</th>';
	});
	$theadTag += '</tr>';
	
	$tbodyTag += '<tr>';

	$($category).each(function(j, m) {
		var $_data = data[j];
		var $child = $_data[opt.child];
		$($child).each(function(k, l) {
			$tbodyTag += '<td>' + l[opt.name] + '(' + l[opt.value] + ')</td>';
		});
	});

	$tbodyTag += '</tr>';
	
	$thead.append($theadTag);
	$tbody.append($tbodyTag);
	if (!widget) $('#modalTableData').html($thead).append($tbody);
	else return $table.append($thead).append($tbody);
}

function fn_setDataGridChart7(data, opt, widget) {
	var $category = new Array();
	var $table = $('<table class="table m-0 table-colored-bordered table-bordered-dark table-hover"></table>');
	var $thead = $('<thead class="center"></thead>'), $tbody = $('<tbody class="center"></tbody>');
	var $theadTag = '', $tbodyTag = '';
	
	$theadTag += '<tr>';
	$theadTag += '<th>구분</th>';
	$theadTag += '<th>데이터</th>';
	$theadTag += '</tr>';
	
	$(data).each(function(i, e) {
		if (i < opt.cnt) {
			$tbodyTag += '<tr>';
			$tbodyTag += '<td>' + e.keyword + '</td>';
			$tbodyTag += '<td>' + e.correlation + '</td>';
			$tbodyTag += '</tr>';	
		}
	});

	$thead.append($theadTag);
	$tbody.append($tbodyTag);
	if (!widget) $('#modalTableData').html($thead).append($tbody);
	else return $table.append($thead).append($tbody);
}

function fn_setDataGridChart8(data, opt, widget) {

	var $category = new Array();
	var $table = $('<table class="table m-0 table-colored-bordered table-bordered-dark table-hover"></table>');
	var $thead = $('<thead class="center"></thead>'), $tbody = $('<tbody class="center"></tbody>');
	var $theadTag = '', $tbodyTag = '';
	
	$theadTag += '<tr>';
	$theadTag += '<th>구분</th>';
	$theadTag += '<th>데이터</th>';
	$theadTag += '</tr>';
	
	$(data).each(function(i, e) {
		if (i < opt.end) {
			$tbodyTag += '<tr>';
			$tbodyTag += '<td>' + e.term + '</td>';
			$tbodyTag += '<td>' + e.weight + '</td>';
			$tbodyTag += '</tr>';	
		}
	});

	$thead.append($theadTag);
	$tbody.append($tbodyTag);
	if (!widget) $('#modalTableData').html($thead).append($tbody);
	else return $table.append($thead).append($tbody);
}

function fn_setDataGridChart9(data, opt, widget) {

	var $category = new Array();
	var $table = $('<table class="table m-0 table-colored-bordered table-bordered-dark table-hover"></table>');
	var $thead = $('<thead class="center"></thead>'), $tbody = $('<tbody class="center"></tbody>');
	var $theadTag = '', $tbodyTag = '';
	
	$theadTag += '<tr>';

	$(data.category).each(function(i, e) {
		$category.push(e);
		$theadTag += '<th>' + e + '</th>';
	});
	$theadTag += '</tr>';
	

	for (var i = 0; i < opt.limit; i ++) {
		var $_data = data.list;

		$tbodyTag += '<tr>';
		$($category).each(function(j, m) {
			$tbodyTag += '<td>' + $_data[j][i].term + '(' + $_data[j][i].weight + ')</td>';
		});
		$tbodyTag += '</tr>';
	}
	
	$thead.append($theadTag);
	$tbody.append($tbodyTag);
	if (!widget) $('#modalTableData').html($thead).append($tbody);
	else return $table.append($thead).append($tbody);
}

function fn_RankChartDateFormView(d, p) { // d: 날짜
		
	var yy = d.substr(0,4), mm = d.substr(4,2), dd = d.substr(6,2), day = '';

	if (p == 'Y') {
		if(yy != "0000") day += yy + '년';
	} else if (p == 'YI') {
		if(yy != "0000") day += yy;
	} else if (p == 'M') {
		if (yy != "0000") day += yy + '년';
		if (mm != "00") day += ' ' + mm + '월';
	} else if (p == 'W') {

		var nowDate = new Date(yy, mm - 1, dd);
		var preDate = new Date(yy, mm, 0).getDay();
		var lastDate = nowDate.getDay();
		var weekSeq = Math.ceil( (parseInt(dd) + (preDate + 1)) / 7);

		day += nowDate.getFullYear() + '년';
		day += ' ' + (nowDate.getMonth() + 1) + '월';
		day += ' (' + weekSeq + '주)';
		
	} else if (p == 'D') {
		if (yy != "0000") day += yy + '년';
		if (mm != "00") day += ' ' + mm + '월';
		if (dd != "00") day += ' ' + dd + '일';
	} else
		day = yy + '-' + mm + '-' + dd;
	
	return day;
}