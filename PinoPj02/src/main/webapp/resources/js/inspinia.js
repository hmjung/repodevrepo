/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.7.1
 *
 */


$(document).ready(function () {


    // Add body-small class if window less than 768px
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }

    // MetsiMenu
    $('#side-menu').metisMenu();
	$('#report-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link').on('click', function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.children('.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link').on('click', function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Fullscreen ibox function
    $('.fullscreen-link').on('click', function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        $('body').toggleClass('fullscreen-ibox-mode');
        button.toggleClass('fa-expand').toggleClass('fa-compress');
        ibox.toggleClass('fullscreen');
        setTimeout(function () {
            $(window).trigger('resize');
        }, 100);
    });

    // Close menu in canvas mode
    $('.close-canvas-menu').on('click', function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    });

    // Run menu of canvas
    $('body.canvas-menu .sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
    });

    // Open close right sidebar
    $('.right-sidebar-toggle').on('click', function () {
        $('#right-sidebar').toggleClass('sidebar-open');
    });

    // Initialize slimscroll for right sidebar
    $('.sidebar-container').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
    });

    // Open close small chat
    $('.open-small-chat').on('click', function () {
        $(this).children().toggleClass('fa-comments').toggleClass('fa-remove');
        $('.small-chat-box').toggleClass('active');
    });

    // Initialize slimscroll for small chat
    $('.small-chat-box .content').slimScroll({
        height: '234px',
        railOpacity: 0.4
    });

    // Small todo handler
    $('.check-link').on('click', function () {
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });


    // Minimalize menu
    $('.navbar-minimalize').on('click', function (event) {
        event.preventDefault();
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();

    });

    // Tooltips demo
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });


    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebar-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeigh = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if (navbarHeigh > wrapperHeigh) {
            $('#page-wrapper').css("min-height", navbarHeigh + "px");
        }

        if (navbarHeigh < wrapperHeigh) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            if (navbarHeigh > wrapperHeigh) {
                $('#page-wrapper').css("min-height", navbarHeigh - 60 + "px");
            } else {
                $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
            }
        }

    }

    fix_height();

    // Fixed Sidebar
    $(window).bind("load", function () {
        if ($("body").hasClass('fixed-sidebar')) {
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }
    });

    // Move right sidebar top after scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#right-sidebar').addClass('sidebar-top');
        } else {
            $('#right-sidebar').removeClass('sidebar-top');
        }
    });

    $(window).bind("load resize scroll", function () {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    });

    $("[data-toggle=popover]")
        .popover();

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    })
});


// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
});


// Local Storage functions
// Set proper body class and plugins based on user configuration
$(document).ready(function () {
	$('#position1').show();
   /* if (localStorageSupport()) {

        var collapse = localStorage.getItem("collapse_menu");
        var fixedsidebar = localStorage.getItem("fixedsidebar");
        var fixednavbar = localStorage.getItem("fixednavbar");
        var fixedfooter = localStorage.getItem("fixedfooter");

        var body = $('body');

        if (fixedsidebar == 'on') {
            body.addClass('fixed-sidebar');
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }

        if (collapse == 'on') {
            if (body.hasClass('fixed-sidebar')) {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar');
                }
            } else {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar');
                }

            }
        }

        if (fixednavbar == 'on') {
            $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
            body.addClass('fixed-nav');
        }

        if (boxedlayout == 'on') {
            body.addClass('boxed-layout');
        }

        if (fixedfooter == 'on') {
            $(".footer").addClass('fixed');
        }
    }
	*/
	
	
	$('.i-checks').iCheck({
		checkboxClass: 'icheckbox_blue-skin',
		radioClass: 'iradio_blue-skin',
	});
	
	$('.i-checks-d').iCheck({
		checkboxClass: 'icheckbox_blue-skin2',
		radioClass: 'iradio_blue-skin2',
	});
			
			
	 

	/* div 스크롤*/
	$('.filter-content .scroll_content, .widget-cate-box').slimscroll({
		height: '200px',
		alwaysVisible: 'false'
	})
	$('.favo-rank .rank-box-content').slimscroll({
		height: '400px',
		distance: '10px',
		alwaysVisible: 'false'
	})
	
	$('.data-view-table .scroll_content, .preview-img-box').slimscroll({
		height:'300px',
		alwaysVisible: 'true'
	})	
	
	$('.area-pn-rank .rank-box ol, .area-keyword-rank .rank-box ol').slimscroll({
		height:'350px',
		alwaysVisible: 'true'
	})	
	
	$('#modal-keyword .modal-body').slimscroll({
		height:'800px',
		alwaysVisible: 'true'
	})
	// 보고서 회람자 목록
	$('.reply-list').slimscroll({
		height:'200px',
		alwaysVisible: 'true'
	})
	
	
	// 원문 상세보기
	$('#vertical-timeline .scroll_content').slimscroll({
		height:'350px',
		distance: '0px',
		alwaysVisible: 'true'
	})
	// 시각화 변경
	$('#modal-chart-design .modal-body').slimscroll({
		height:'680px',
		alwaysVisible: 'true'
	})			
	//보고서 위젯 컨텐츠 영역
	
	$('.widget-list .scroll_content').slimscroll({
		height:'560px',
		distance: '0px',
		alwaysVisible: 'true'
	})
	
	$('.signal-rank .rank-box-content').slimscroll({
		height:'100px',
		distance: '0px',
		alwaysVisible: 'true'
	})
	
	// 키워드분석추적-관심 키워드 리스트
	$('.my-keyword-list').slimscroll({
		height:'220px',
		distance: '0px',
		alwaysVisible: 'true'
	})
	
	
	// 키워드분석추적 댓글 목록
	$('#accordion-comment .analysis-list').slimscroll({
		height:'220px',
		distance: '0px',
		alwaysVisible: 'true'
	})
	
	// 왼쪽 메시지 목록
	$('.dropdown-messages > ul').slimscroll({
		height:'240px',
		distance: '0px',
		alwaysVisible: 'true'
	})
	
	// 회람 및 승인 현황
	$('#modal-circulation-list .analysis-list').slimscroll({
		height:'480px',
		distance: '0px',
		alwaysVisible: 'false'
	})
	
	
	
	// 민원보고서 회람자 영역 show/hide
	/*$("#min-report").on('click', function () {
		alert ("--3-3-3-");
		if($("input:checkbox[name='min-report']").is(":checked")  == true ){
			alert (" aaa ");
			$('.reply-area').show();
		}else {
			alert (" nnnn ");
			$('.reply-area').hide();
		}

	 });*/
	
	//스크롤시 검색창 상단 고정
	var snav = $('#sbox');
	$(window).scroll(function () {
		var sboxSize = $(".wrapper-content").innerWidth();
		var setSize;
		if($(this).scrollTop() > 136) {
		setSize = sboxSize ;
		snav.width(sboxSize-41);
			$('.sbox-scroll').addClass("f-nav");
			$(".btn-search-close").show();
		} else {
			$('.sbox-scroll').css("width","100%");
			$('.sbox-scroll').removeClass("f-nav");
			$(".btn-search-close").hide();
			$(".btn-search-fixed").hide();
			$("#sbox.search-box").show();
		}
	});
	$(window).bind("resize", function(){
		var sboxSize = $(".wrapper-content").innerWidth();
		$('.sbox-scroll').width(sboxSize-20);
	})
	
	$('.btn-search-close').click(function (){
		$("#sbox.search-box").hide();
		$(".btn-search-fixed").show();
		
	})
	$('.btn-search-fixed').click(function (){
		$(this).toggle();
		$(".f-nav.search-box").slideToggle(100);
	})
	
	/* toastr 접속시 뜨는 형태	
	setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');

            }, 1300);	
	*/
	
	$('#showsimple').click(function (){
		// Display a success toast, with a title
		
		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "progressBar": false,
		  "preventDuplicates": false,
		  "positionClass": "toast-top-center",
		  "onclick": null,
		  "showDuration": "400",
		  "hideDuration": "1000",
		  "timeOut": "70000000",
		  "extendedTimeOut": "1000000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut",
		};
		toastr.success('분석이 완료되었습니다.', '');
	});
	
	// 적용 필터
	$('.tagsinput').tagsinput({
		tagClass: 'label label-primary' 
    });
	
	//주요 이슈 후보 
	$('.tagsinput-date, .tagsinput-email').tagsinput({
		tagClass: 'label label-sec' 
    });
	
	//주요 이슈 후보 
	$('.tagsinput-keyword').tagsinput({
		tagClass: 'label label-danger' 
    });
	

	$('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });

	
	
	/* 필터 클릭시 
	 $('.filter-box  .filter-content ol  li').on('click', function (event) {
       $(this).addClass("active");

	 });*/
	// 유사도 분석 활설화
	$('#radio-similarity1').on('click', function () {
		$('#input-similarity1').focus().removeAttr("disabled")
		$('#input-similarity2').attr("disabled","disabled")
	});
	$('#radio-similarity2').on('click', function () {
		$('#input-similarity2').focus().removeAttr("disabled")
		$('#input-similarity1').attr("disabled","disabled")
	});
	 
	// 키워드 등록(modal) 알람 등록 여부 입력 폼 활성화
	$("#al-sms, #al-email").on('click', function () {
		if($("input:checkbox[id='al-email']").is(":checked")  == true ||$("input:checkbox[id='al-sms']").is(":checked")  == true ){
			$('#ex_doc').show();
		}else {
			$('#ex_doc').hide();
		}
		/*if($("input:checkbox[id='filtering']").is(":checked") == true){
			var val = $("#filterVal").val();
			setCookie(urlVal,val, 1);
		}*/

		//$('#al-sms-box').toggle();
	 });
	/*$('#al-email').on('click', function () {
		$('#al-email-box').toggle();
	 });*/
	
	/* 공통 검색 상세보기 컨트롤 */
	$('.default-search-box .btn-detail-toggle').on('click', function () {
		 $('.detail-search-box').slideToggle(200);
		 $('.default-search-box .ibox-title').slideToggle(200);;
		 $('.default-search-box .btn-detail-toggle span').toggle();
		 });
	 
	 $('.close-detail').on('click', function () {
		 $('.detail-search-box').slideToggle(200);
		 $('.default-search-box .ibox-title').slideToggle(200);;
	 });
	 
	 /* 키워드 분석 추적 상세보기 컨트롤 */
	 $('.keyword-search-box .btn-detail-toggle, .keyword-detail-box .btn-detail-toggle').on('click', function () {
		 $('.keyword-detail-box').slideToggle(200);
		 $('.keyword-search-box .key-input-box').toggleClass('key-input-box-full');
		 $('.key-button-box').toggle();
		 });
	 //키워드 분석 추적 상세검색 닫기 버튼 */
	 $('.keyword-detail-box .close-detail').on('click', function () {
		 $('.keyword-detail-box').slideToggle(200);
		 $('.keyword-search-box .key-input-box').removeClass('key-input-box-full');
		 $('.key-button-box').toggle();
		 });
	 
	 
	 // 검색어 추이 분석 식 선택
	 $('#btn-total').on('click', function () {
		if($(this).hasClass('btn-primary')){
			$(this).removeClass('btn-primary');
			$(this).addClass('btn-white');
		}else {
			$(this).removeClass('btn-white');
			$(this).addClass('btn-primary');
		}
		$('#btn-one-value').removeClass('btn-primary');
		$('#btn-one-value').addClass('btn-white');
		//$(this).next('.btn-group').removeClass('open')	
	 });
	 
	 
	 $('#btn-one-value').on('click', function () {
			if($(this).hasClass('btn-primary')){
				$(this).removeClass('btn-primary');
				$(this).addClass('btn-white');
			}else {
				$(this).removeClass('btn-white');
				$(this).addClass('btn-primary');
			}
			$('#btn-total').removeClass('btn-primary');
			$('#btn-total').addClass('btn-white');
		 });
		 
	 
	 // 키워드 분석 패널 더보기 
	 $('.add-next').on('click', function () {
			$(this).next('div').slideToggle(200);
			$(this).hide();
			$(".rank-slider").slick('refresh');
		 });
	 
	
	 $('.btn-dropdown').on('click', function () {
		$(this).next('.dropdown').slideToggle(200);
		$(this).toggleClass('open');
	 });
	 

	 $('.dropdown .close').on('click', function () {
		$(this).parent('.dropdown').toggle();
		$(this).closest('.dropdown').parent().find('.btn-dropdown').removeClass('open');	
		//console.log($(this).closest('.dropdown').parent().find('.btn-dropdown'));
	});
	 
	 $('.btn-messages').on('click', function () {
		$(this).next('.dropdown').toggle();
		$(this).toggleClass('open');
	 });
	 
	 
	 // 관심키워드 목록 적용 버튼
	 $('.btn-app-favo-keyword').on('click', function () {
		 $(this).parent('.dropdown').slideToggle(100);
		 $(this).parent().siblings('.btn-dropdown').removeClass('open');
		 });
		 
	 
	 
	
	 
	/* 상세분석 필터 드롭다운  */
	 $('.btn-dropdown').on('click', function () {
		$(this).parent('li.dropdown-filter').toggleClass('open'); 
		$(this).parent().siblings().children('.dropdown').hide();
		$(this).closest('.filter-box-wrap').siblings().find('.dropdown').hide();
		$(this).closest('.filter-box-wrap').siblings().find('li.dropdown-filter').removeClass('open');

		$(this).parent().siblings('li.dropdown-filter').removeClass('open');
	 });
	 
	 $('.filter-content li > a').on('click', function () {
		$(this).closest('.filter-box-wrap').siblings().find('.dropdown').hide();
	});
	 
	
	 
	 $('.dropdown .close').on('click', function () {
		 $(this).closest('.dropdown').parents().find('li.dropdown-filter').removeClass('open');
	});
	 
	 $('.filter-box .filter-content ol > li').on('click', function () {
		 $(this).siblings().children('.dropdown').hide();
		 $(this).siblings('li.dropdown-filter').removeClass('open');
		 $(this).closest('.filter-box-wrap').siblings().find('li.dropdown-filter').removeClass('open');
	});
	
	
	 
	 
	$('#modal-report .report-nav-wrap .nav.report-nav  > li.li-widget').on('click', function () {
		if($(this).hasClass('active')){
			$('.widget-nav-button').show();
		}else{
			$('.widget-nav-button').hide();
		}
	 });
	 
	 
	// Tooltips demo
    $('.title').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

	
	 /*$('.report-wrap .report-container .content-box > div').on('click', function () {
		$(this).find('.col-option').show();
	 });
	 
	 $('.report-wrap .report-container .content-box > div').on('blur', function () {
		$(this).find('.col-option').hide();
	 });
	 */

	
	
	 
	/* $('.dropdown-toggle').click(function () {
		 dropDownFixPosition($('button'),$('.dropdown-menu'));
	 });
	 function dropDownFixPosition(button, dropdown) {
			 var dropDownTop = button.offset().top+button.outerHeight();
			 dropdown.css('top', dropDownTop + "px");
			 dropdown.css('left', button.offset().left + "px");
		 }

		
		
		
	 	$('.dropdown').on('show.bs.dropdown', function () {
		$('body').append($('.dropdown').css({
				position: 'absolute',
				left: $('.dropdown').offset().left,
				top: $('.dropdown').offset().top
		}).detach())
	});	;*/
	
	/* 지역본부 지도 */
	
	$('.btn-position1, .btn-position2, .btn-position3, .btn-position4, .btn-position5, .btn-position6, .btn-position7, .btn-position8, .btn-position9, .btn-position10, .btn-position11, .btn-position12, .btn-position13, .btn-position14, .btn-position15').hover(
			function (e) {
			$("#position1").hide();
			$("#"+$(this).attr("class").replace("btn-","")).show();
			//$(".map-layer-info").show();
		}, function(e){
			$("#"+$(this).attr("class").replace("btn-","")).hide();
		}
	);

	
	/* 적용필터 줄이기 */
	$('.filter-collapse-link').on('click', function () {
        var ibox = $('.ibox-filter .ibox-content .row').toggleClass('ibox-filter-collapse');
        var button = $(this).find('i');
        var content = ibox.children('.ibox-content .row');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 150);
    });
	
	// 
	$('.msg-confirm').click(function(){
            swal({
                title: "분석 저장",
                text: "현재 분석 조건을 저장하시겠습니까?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#038eff",
                confirmButtonText: "저장",
				cancelButtonText: "취소",
                closeOnConfirm: false
            }, function () {
                swal("저장!", "저장이 완료되었습니다.", "success");
            });

        });
	
	$('.msg-report-modify').click(function(){
        swal({
            title: "분석 저장",
            text: "현재 분석 조건을 저장하시겠습니까?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#038eff",
            confirmButtonText: "저장",
			cancelButtonText: "취소",
            closeOnConfirm: false
        }, function () {
            swal("저장!", "저장이 완료되었습니다.", "success");
        });

    });

	
	/*
	// 체크박스 전체선택  및 전체해제
	$('#chk-all').click(function () {
		if($('#chk-all').is(':chked')) {
			$('.chk').prop('checked',true);
		}
		else {
			$('.chk').prop('checked',false);
		}
	 });
	 // 한개 체크박스 해제시 전채선택 체크박스 해지
	$('.chk').click(function () {
		if($("input[name='chk']:checked").lenght==3) {
			$('#chk-all').prop('checked',true);
		}
		else {
			$('#chk-all').prop('checked',false);
		}
	 });*/
	 //$(".hide-panels").hide();
	 $('.rank-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        centerMode: false,
        responsive: [
	           {
	
	                breakpoint: 1600,
	                settings: {
	                    slidesToShow: 4,
	                    slidesToScroll: 3,
	                    infinite: true,
	                    dots: false
                	}
	
	            }

            ]	
	 	});
	 
	 //slick();
	
});


// check if browser support HTML5 local storage
function localStorageSupport() {
    return (('localStorage' in window) && window['localStorage'] !== null)
}


function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 200);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 100);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

// Dragable panels
function WinMove() {
    var element = "[class*=col]";
    var handle = ".ibox-title";
    var connect = "[class*=col]";
    $(element).sortable(
        {
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8
        })
        .disableSelection();
}



